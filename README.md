官方站点：[https://www.hemaphp.com](https://www.hemaphp.com)

使用教程：[https://www.kancloud.cn/he_ma/hema_wechat/2304422](https://www.kancloud.cn/he_ma/hema_wechat/2304422)

# 项目介绍

完全开源的微信小程序SaaS平台，TP6框架开发，对接微信开放平台，支持微信小程序和公众号的扫码授权对接，在线DIY小程序生成源代码并一键发布小程序源码。可无限扩展小程序模板，可实现小程序的快速免审核注册（免300元审核费），可批量发布小程序模板，同步升级版本等功能。基础版本提供餐饮小程序模板，可实现堂食点餐，外卖点餐与配送。适合餐饮小吃行业，水果生鲜门店，服装护肤行业，零食百货，超市等。支持多用户，多应用，多门店（三多模式）。

# 为什么选择SaaS版

1.免300元审核费，快速注册不同主体微信小程序。

2.集中管理N个商户（小程序）。一键升级与发布小程序模板。

3.商户在线生成小程序，一键发布与升级，无需接触源代码。

4.提供丰富的小程序DIY页面组件，千户千页，不会出现重复的。

5.提供多套小程序模板和商户管理端小程序。

6.无限扩展小程序模板数量（各行业）。

7.商业运营，营销活动全面。

8.全面对接各大同城配送跑腿平台。

9.支持多门店，实现店铺连锁经营。

10.支持绑定公众号，实现智能消息推送，和订单等各类提醒。

# 项目演示

演示站点：[https://demo.hemaphp.com](https://demo.hemaphp.com)

1.超管端入口：[https://demo.hemaphp.com/admin](https://demo.hemaphp.com/admin)

2.代理入口：[https://demo.hemaphp.com/agent](https://demo.hemaphp.com/agent)

3.商户入口：[https://demo.hemaphp.com](https://demo.hemaphp.com)

4.店长入口：[https://demo.hemaphp.com/store/foodclerk.passport/login](https://demo.hemaphp.com/store/foodclerk.passport/login)

5.PC收银端：[https://demo.hemaphp.com/h5/cash](https://demo.hemaphp.com/h5/cash)

**以上体验账号密码均为：test**

# 前端演示中心
![输入图片说明](https://images.gitee.com/uploads/images/2021/0816/125031_04a05be5_1882845.jpeg "1.jpeg")

商家助手小程序登录所需账号密码，请使用默认初始的即可

# 文件目录

1.PHP文件夹：为服务端文件，请按照根目录上传至您的服务器。

2.Wechat文件夹：为小程序代码文件。

# 商业版咨询

 **QQ:19966591** 

 **微信** 

![输入图片说明](https://www.hemaphp.com/assets/img/wechat.png)

