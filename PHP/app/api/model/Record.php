<?php
namespace app\api\model;

use app\common\model\Record as RecordModel;
use think\facade\Cache;

/**
 * 交易记录模型
 */
class Record extends RecordModel
{
    /**
     * 获取充值订单详情 - 微信充值回调
     */
    public function payDetail(string $order_no)
    {
        return Cache::get($order_no);
    }

    /**
     * 更新充值付款状态 - 微信充值回调
     */
    public function updatePayStatus(string $transaction_id, array $data)
    {
        //更新用户余额信息
		$user = User::getUser(['user_id' => $data['user_id']]);
		$user->money = ['inc', $data['money']];//增加充值金额
		$user->save();
		$data['transaction_id'] = $transaction_id;
		$this->save($data);
		return true;
    }
}
