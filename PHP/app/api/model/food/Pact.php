<?php
namespace app\api\model\food;

use app\common\model\food\Pact as PactModel;

/**
 * 预约模型
 */
class Pact extends PactModel
{
	/**
     * 添加
     */
    public function add(array $data,$user_id)
    {
        $data['row_no'] = row_no(self::$applet_id,$user_id);
		$data['applet_user_id'] = $user_id;
		$data['formwork_order_id'] = self::$applet_id;
		return $this->save($data);
    }

    /**
     * 订单详情
     */
    public static function getUserSort($shop_id,$user_id)
    {
        return self::get([
            'shop_id' => $shop_id,
            'user_id' => $user_id,
            'status' => 10,
            'pact_type' => 10
        ], ['shop']);
    }

}
