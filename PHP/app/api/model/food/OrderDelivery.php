<?php
namespace app\api\model\food;

use app\common\model\food\OrderDelivery as OrderDeliveryModel;

/**
 * 订单配送模型
 */
class OrderDelivery extends OrderDeliveryModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];
}
