<?php
namespace app\api\model\food;

use app\common\model\food\OrderGoods as OrderGoodsModel;

/**
 * 订单商品模型
 */
class OrderGoods extends OrderGoodsModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [
    ];

}
