<?php
namespace app\api\model\food;

use app\common\model\food\Category as CategoryModel;

class Category extends CategoryModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];

}
