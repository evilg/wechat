<?php
namespace app\api\model\food;

use app\common\model\food\OrderAddress as OrderAddressModel;

/**
 * 订单收货地址模型
 */
class OrderAddress extends OrderAddressModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];

}
