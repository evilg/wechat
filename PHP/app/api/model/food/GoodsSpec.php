<?php
namespace app\api\model\food;

use app\common\model\food\GoodsSpec as GoodsSpecModel;

class GoodsSpec extends GoodsSpecModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];

}
