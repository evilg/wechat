<?php
namespace app\api\model\food;

use app\common\model\food\Comment as CommentModel;
use think\facade\Db;

class Comment extends CommentModel
{
    /**
     * 添加
     */
    public function add(array $data)
    {
    	// 开启事务
        Db::startTrans();
        try {
    		Order::where(['order_id' => $data['order_id']])->update(['is_cmt' => 1]);
			$this->save($data);
		 	Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }
}
