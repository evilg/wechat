<?php
namespace app\api\model\food;

use app\common\model\Record as RecordModel;
use think\facade\Cache;

/**
 * 交易记录模型
 */
class Record extends RecordModel
{
	/**
     * 新增
    */ 
    public function add($order, $action)
    {
    	$data = [
            'order_no' => $order['order_no'],
            'money' => $order['pay_price'],
            'user_id' => $order['user_id'],
            'shop_id' => $order['shop_id'],
            'applet_id' => $order['applet_id'],
            'action' => $action,
            'remark' => '订单支付'
        ];
        return $this->save($data);
    }
    
    /**
     * 获取充值订单详情 - 微信充值回调
     */
    public function payDetail(string $order_no)
    {
        return Cache::get($order_no);
    }

    /**
     * 更新充值付款状态 - 微信充值回调
     */
    public function updatePayStatus(string $transaction_id, array $data)
    {
        $gift_money = 0;//赠送金额
        $coupon = [];//赠送优惠券
        if(isset($data['recharge_plan_id']) AND $data['recharge_plan_id'] > 0){
            if($plan = RechargePlan::detail($data['recharge_plan_id'])){
                if($plan['gift_type']['value'] == 10){
                    //优惠券
                    for($n=0;$n<$plan['gift_money'];$n++){
                        //计算优惠券到期时间
                        switch ($plan['coupon']['valid_time']['value']) {
                            case '10':
                                $expiretime = strtotime(date('Y-m-d').'23:59:59');
                                break;
                            case '20':
                                $expiretime = strtotime("+1 week");
                                break;
                            case '30':
                                $expiretime = strtotime("+1 month");
                                break;
                            case '40':
                                $expiretime = strtotime("+1 year");
                                break;
                            default:
                                $expiretime = time();
                                break;
                        }
                        $coupon[] = [
                            'coupon_id' => $plan['coupon']['coupon_id'],
                            'type' => $plan['coupon']['type']['value'],
                            'rule' => $plan['coupon']['rule']['value'],
                            'expiretime' => $expiretime,
                            'shop_id' => $data['shop_id'],
                            'user_id' => $data['user_id'],
                            'applet_id' => $data['applet_id']
                        ];
                    }
                }else{
                    $gift_money = $plan['gift_money']; //赠送现金余额
                }
            }
        }
        //更新用户余额信息
		$user = User::getUser(['user_id' => $data['user_id']]);
		$user->money = ['inc', $data['money']+$gift_money];//增加充值金额
		$user->save();
        //是否赠送优惠券
        if(sizeof($coupon) > 0){
            $model = new CouponUser;
            $model->saveAll($coupon);
        }
		$data['transaction_id'] = $transaction_id;
		$this->save($data);
		return true;
    }
}
