<?php
namespace app\api\model\food;

use app\common\model\food\Shop as ShopModel;

class Shop extends ShopModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [];
}
