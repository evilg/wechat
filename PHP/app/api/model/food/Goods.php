<?php
namespace app\api\model\food;

use app\common\model\food\Goods as GoodsModel;

class Goods extends GoodsModel
{
    /**
     * 隐藏字段
     */
    protected $hidden = [
    ];
	
	/**
     * 按照分类获取上架的所有商品
     */
	public function getGoodsList($shop_id, $category, $user,$table_id)
	{
		//获取当前购物车商品
		$cart = new Cart($user['user_id'],$shop_id,self::$applet_id,$table_id);
		$cart_data = $cart->getCart();
		$cartlist = $cart->getList($user);
		$list=array();
		for($i=0;$i<sizeof($category);$i++){
			$detail = $this->with(['spec', 'image.file', 'spec_rel.spec'])
						->where([
							'goods_status' => 10, 
							'category_id' => $category[$i]['category_id'],
							'shop_id' => $shop_id
						])
						->order(['goods_sort', 'goods_id' => 'desc'])
						->select();
			$goods = $detail;
			// 规格信息
			for($n=0;$n<sizeof($goods);$n++){
				$goods[$n]['total_num'] = 0;//购物车数量
				if($goods[$n]['spec_type'] == 20){
					$goods[$n]['specData'] = $this->getManySpecData($goods[$n]['spec_rel'], $goods[$n]['spec']);
					for($s=0;$s<sizeof($goods[$n]['spec']);$s++){
						$goods[$n]['spec'][$s]['sell_num'] = 0;
						if(isset($cart_data[$goods[$n]['goods_id'].'_'.$goods[$n]['spec'][$s]['spec_sku_id']])){
							$goods[$n]['total_num'] = $goods[$n]['total_num'] + $cart_data[$goods[$n]['goods_id'].'_'.$goods[$n]['spec'][$s]['spec_sku_id']]['goods_num'];	
							$goods[$n]['spec'][$s]['sell_num'] = $cart_data[$goods[$n]['goods_id'].'_'.$goods[$n]['spec'][$s]['spec_sku_id']]['goods_num'];
						}
					}
				}else{
					if(isset($cart_data[$goods[$n]['goods_id'].'_'])){
						$goods[$n]['total_num'] = $cart_data[$goods[$n]['goods_id'].'_']['goods_num'];	
					}
					$goods[$n]['specData'] = null;
				}
			}
			$category[$i]['goods'] = $goods;
		}
		//获取推荐商品
		$goods = $this->with(['image.file', 'spec', 'spec.image', 'spec_rel.spec'])->where(['is_recommend' => 1, 'shop_id' => $shop_id])->select();
		if(sizeof($goods)){
		    for($n=0;$n<sizeof($goods);$n++){
				$goods[$n]['total_num'] = 0;//购物车数量
				if($goods[$n]['spec_type'] == 20){
					$goods[$n]['specData'] = $this->getManySpecData($goods[$n]['spec_rel'], $goods[$n]['spec']);
					for($s=0;$s<sizeof($goods[$n]['spec']);$s++){
						$goods[$n]['spec'][$s]['sell_num'] = 0;
						if(isset($cart_data[$goods[$n]['goods_id'].'_'.$goods[$n]['spec'][$s]['spec_sku_id']])){
							$goods[$n]['total_num'] = $goods[$n]['total_num'] + $cart_data[$goods[$n]['goods_id'].'_'.$goods[$n]['spec'][$s]['spec_sku_id']]['goods_num'];	
							$goods[$n]['spec'][$s]['sell_num'] = $cart_data[$goods[$n]['goods_id'].'_'.$goods[$n]['spec'][$s]['spec_sku_id']]['goods_num'];
						}
					}
				}else{
					if(isset($cart_data[$goods[$n]['goods_id'].'_'])){
						$goods[$n]['total_num'] = $cart_data[$goods[$n]['goods_id'].'_']['goods_num'];	
					}
					$goods[$n]['specData'] = null;
				}
			}
		    array_unshift($category,[
		        'category_id' => 10000,
		        'name' => '商家推荐',
		        'goods' => $goods
		    ]);
		}
		return compact('category','cartlist');
	}

    /**
     * 商品详情：HTML实体转换回普通字符
     */
    public function getContentAttr($value)
    {
       return htmlspecialchars_decode($value);
    }

    /**
     * 根据商品id集获取商品列表 (购物车列表用)
     */
    public function getListByIds($goodsIds) {
        return $this->with(['category','image.file', 'spec','spec.image','spec_rel.spec'])
            ->where('goods_id', 'in', $goodsIds)->select();
    }

}
