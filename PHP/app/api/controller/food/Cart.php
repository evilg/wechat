<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Cart as CartModel;

/**
 * 购物车管理
 */
class Cart extends Controller
{
    private $user;
    private $model;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();
        $table_id = $this->request->param('table_id');
        $this->model = new CartModel($this->user['user_id'],$this->shop_id,$this->applet_id,$table_id);
    }

    /**
     * 购物车列表
     */
    public function lists($order_mode = 0)
    {
        return $this->renderSuccess($this->model->getList($this->user),$order_mode);
    }
	
	/**
     * 一键清空购物车
     */
    public function clearAll()
    {
		$this->model->clearAll();
        return $this->renderMsg('清除成功');
    }

    /**
     * 加入购物车
     */
    public function add()
    {
        if (!$this->model->add($this->request->post())) {
            return $this->renderError($this->model->getError() ?: '加入购物车失败');
        }
        $total_num = $this->model->getTotalNum();
        return $this->renderSuccess(['cart_total_num' => $total_num], '加入购物车成功');
    }

    /**
     * 减少购物车商品数量
     */
    public function sub()
    {
        $this->model->sub($this->request->post());
        return $this->renderSuccess();
    }

    /**
     * 删除购物车中指定商品
     */
    public function delete()
    {
        $this->model->delete($this->request->post());
        return $this->renderSuccess();
    }

}
