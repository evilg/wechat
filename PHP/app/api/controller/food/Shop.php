<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Shop as ShopModel;

/**
 * 商家门店控制器
 */
class Shop extends Controller
{
    /**
     * 门店列表
     */
    public function lists()
    {
		$model = new ShopModel;
        $list = $model->getList(false,10,$this->location);
        return $this->renderSuccess(compact('list'));
    }
	
	/**
     * 门店列表
     */
    public function detail()
    {
		if($detail = ShopModel::detail($this->shop_id,$this->location)){
			return $this->renderSuccess(compact('detail'));
		}
		return $this->renderError('门店不存在');
    }
}
