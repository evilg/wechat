<?php
namespace app\api\controller\food;

use hema\wechat\Pay as WxPay;
use app\api\model\food\Order as OrderModel;
use app\api\model\food\Record as RecordModel;


/**
 * 支付成功异步通知接口
 */
class Notify
{
    /**
     * 订单支付
     */
    public function order()
    {
		$WxPay = new WxPay([]);
        $WxPay->notify(new OrderModel);
    }

    /**
     * 会员充值
     */
    public function recharge()
    {
        $WxPay = new WxPay([]);
        $WxPay->notify(new RecordModel,'add');
    }

	/**
     * 订单退款
     */
    public function orderRefund()
    {
        $WxPay = new WxPay([]);
        $WxPay->notifyRefund(new OrderModel);
    }
}
