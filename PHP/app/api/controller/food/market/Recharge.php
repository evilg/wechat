<?php
namespace app\api\controller\food\market;

use app\api\controller\food\Controller;
use app\api\model\Setting as SettingModel;
use hema\wechat\Pay as WxPay;
use think\facade\Cache;

/**
 * 用户充值管理
 */
class Recharge extends Controller
{
	private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    } 
	
    /**
     * 充值支付
     */
    public function pay($money,$recharge_plan_id = 0)
    {
        $order_no = order_no();
        $order = [
            'order_no' => $order_no,
            'money' => $money,
            'recharge_plan_id' => $recharge_plan_id,
            'remark' => '用户充值',
            'user_id' => $this->user['user_id'],
            'shop_id' => $this->shop_id,
            'applet_id' => $this->applet_id
        ];
        Cache::set($order_no, $order,7200);
        $wx = new WxPay(SettingModel::getItem('wxpay'));
        $wxParams = $wx->unifiedorder('JSAPI',$order_no,$money,'api/food.notify/recharge',$this->user['open_id'],'用户充值');
        return $this->renderSuccess($wxParams);
    }
}
