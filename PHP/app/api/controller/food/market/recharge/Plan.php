<?php
namespace app\api\controller\food\market\recharge;

use app\api\controller\food\Controller;
use app\api\model\food\RechargePlan as RechargePlanModel;
use app\api\model\food\Setting;

/**
 * 用户充值套餐控制器
 */
class Plan extends Controller
{
    /**
     * 列表
     */
    public function lists()
    {
		$setting =  Setting::getItem('recharge');
        $model = new RechargePlanModel;
        $plan = $model->getList();
        return $this->renderSuccess(compact('plan','setting'));
    }

}
