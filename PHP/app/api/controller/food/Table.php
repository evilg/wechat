<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Table as TableModel;

/**
 * 餐桌/包间控制器
 */
class Table extends Controller
{
    /**
     * 详情
     */
    public function detail($table_id)
    {
		if ($detail = TableModel::get($table_id,['shop'])) {
            return $this->renderSuccess(compact('detail'));
        }
        return $this->renderError('餐桌/包间不存在');
    }
	
	/**
     * 列表
     */
    public function lists($dataType = 0)
    {
		$model = new TableModel;
        $list = $model->getList($this->shop_id,$dataType);
        return $this->renderSuccess(compact('list'));
    }

}
