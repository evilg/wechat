<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\Page;
use app\api\model\food\Goods as GoodsModel;
use app\api\model\food\Shop as ShopModel;

/**
 * 首页控制器
 */
class Index extends Controller
{
    /**
     * 首页diy数据
     */
    public function page()
    {
        // 页面元素
        $wxappPage = Page::getHome();
        $items = $wxappPage['page_data']['array']['items'];
		for($n=0;$n<sizeof($items);$n++){
			//公告组
			if($items[$n]['type']=='notice'){
				$title = [];
				$url = [];
				for ($i=0; $i<sizeof($items[$n]['data']); $i++) {
					$title[] = $items[$n]['data'][$i]['title'];
					$url[] = $items[$n]['data'][$i]['url'];
				}
				$items[$n]['data'] = [
					'title' => $title,
					'url' => $url
				];
			}
			//线下门店
			/*
			if($items[$n]['type']=='shop' AND $items[$n]['params']['source']=='auto'){
				$list = ShopModel::getAll($items[$n]['params']['auto']['showNum']);
				for($m=0;$m<sizeof($list);$m++){
					$list[$m]['logo_image']=$list[$m]['front']['url'];
					$list[$m]['shop_name']=$list[$m]['name'];
				}
				$items[$n]['data'] = $list;
			}*/
			//商品组
			if($items[$n]['type']=='goods' AND $items[$n]['params']['source']=='auto'){
				$limit = $items[$n]['params']['auto']['showNum'];
				$sortType = $items[$n]['params']['auto']['goodsSort'];
				$list = GoodsModel::getAll($this->shop_id,$sortType,$limit);
				$news = [];
				for($m=0;$m<sizeof($list);$m++){
					$news[$m] = [
						'goods_id' => $list[$m]['goods_id'],
						'goods_name' => $list[$m]['goods_name'],
						'image' => $list[$m]['image'][0]['url'],
						'goods_price' => $list[$m]['spec'][0]['goods_price'],
						'line_price' => $list[$m]['spec'][0]['line_price'],
						'selling_point' => $list[$m]['selling_point'],
						'goods_sales' => $list[$m]['goods_sales']
					];
				}
				$items[$n]['data'] = $news;
			}
		}
        return $this->renderSuccess(compact('items'));
    }

}
