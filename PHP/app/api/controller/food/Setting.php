<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Setting as SettingModel;
use app\api\model\food\Shop as ShopModel;

/**
 * 商户配置
 */
class Setting extends Controller
{
    /**
     * 订阅消息配置
	 * $order_mode 订单类型
     */
    public function tpl($order_mode)
    {
        $tpl = [];
		if($values = SettingModel::getItem('wxapptpl',$this->applet_id)){
    		//堂食
    		if($order_mode == 10){
    			$shop = ShopModel::get($this->shop_id);
    			if($shop['tang_mode'] == 1){
    				//扫码
    				array_push($tpl,$values['receive']);//商家接单
    				array_push($tpl,$values['finish']);//订单完成
    			}else{
    				//排号
    				array_push($tpl,$values['receive']);//商家接单
    				array_push($tpl,$values['take']);//取餐提醒
    				array_push($tpl,$values['finish']);//订单完成
    			}
    		}
    		//外卖
    		if($order_mode == 20){
    			array_push($tpl,$values['receive']);//商家接单
    			array_push($tpl,$values['horseman']);//骑手取餐
    			array_push($tpl,$values['delivery']);//订单配送
    		}
    		//自取
    		if($order_mode == 30){
    			array_push($tpl,$values['receive']);//商家接单
    			array_push($tpl,$values['take']);//取餐提醒
    			array_push($tpl,$values['finish']);//订单完成
    		}
    		//退款状态提醒
    		if($order_mode == 'refund'){
    			array_push($tpl,$values['refund']);//订单完成
    		}
		}
		return $this->renderSuccess(compact('tpl'));
    }
    
}
