<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\food\CouponUser as CouponUserModel;

/**
 * 预约排号管理
 */
class Coupon extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 优惠券列表
     */
    public function lists()
    {
        $model = new CouponUserModel;
        $list = $model->getList((int)$this->user['user_id']);
        if($list->toArray()){
            $list = array_repeat($list->toArray(),'coupon_id'); //去除重复的优惠券
        }
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 优惠券首页
     */
    public function all()
    {
        $model = new CouponUserModel;
        $list = [
            0 => $model->getList((int)$this->user['user_id'],0),
            1 => $model->getList((int)$this->user['user_id'],10),
            2 => $model->getList((int)$this->user['user_id'],20),
            3 => $model->getList((int)$this->user['user_id'],30),
            4 => $model->getList((int)$this->user['user_id'],40)
        ];
        $couponCount = CouponUserModel::getUserCount((int)$this->user['user_id']);
        $tabs = [
            ['name' => '全部','count' => $couponCount['all']],
            ['name' => '现金券','count' => $couponCount['10']],
            ['name' => '折扣券','count' => $couponCount['20']],
            ['name' => '赠送券','count' => $couponCount['30']],
            ['name' => '减免券','count' => $couponCount['40']]
        ];
        return $this->renderSuccess(compact('list','tabs'));
    }
}
