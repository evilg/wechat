<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\food\Comment as CommentModel;

/**
 * 订单评论
 */
class Comment extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 我的列表
     */
    public function lists()
    {
        $model = new CommentModel;
        $list = $model->getList((int)$this->user['user_id']);
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 添加评论
     */
    public function add()
    {
        $model = new CommentModel;
		$data = $this->request->post();
		$data['user_id'] = $this->user['user_id'];
        $data['applet_id'] = $this->applet_id;
		if($model->add($data)){
			return $this->renderMsg('提交成功');
		}
		$error = $model->getError() ?: '提交失败';
		return $this->renderError($error);
    }
}
