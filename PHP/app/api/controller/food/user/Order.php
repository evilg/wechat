<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\food\Order as OrderModel;
use app\api\model\food\Goods as GoodsModel;

/**
 * 用户订单管理
 */
class Order extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 我的所有类型订单列表
     */
    public function all()
    {
        $model = new OrderModel;
        $list = [
            0 => $model->getList('all',0,$this->user['user_id']),
            1 => $model->getList('payment',0,$this->user['user_id']),
            2 => $model->getList('comment',0,$this->user['user_id']),
            3 => $model->getList('refund10',0,$this->user['user_id'])
        ];
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 我的订单列表
     */
    public function lists(string $dataType)
    {
        $model = new OrderModel;
        $list = $model->getList($dataType,0,$this->user['user_id']);
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 订单详情信息
     */
    public function detail($order_id)
    {
        $order = OrderModel::getUserOrderDetail($order_id, $this->user['user_id']);
        return $this->renderSuccess(compact('order'));
    }

    /**
     * 取消订单
     */
    public function cancel($order_id)
    {
        $model = OrderModel::getUserOrderDetail($order_id, $this->user['user_id']);
        if ($model->userCancel()) {
            return $this->renderMsg('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 确认收货
     */
    public function receipt($order_id)
    {
        $model = OrderModel::getUserOrderDetail($order_id, $this->user['user_id']);
        if ($model->receipt()) {
            return $this->renderMsg('操作成功');
        }
		$error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 立即支付
	 * $pay_mode 支付模式 0微信，1余额
     */
    public function pay($order_id, $pay_mode = 0)
    {
        // 订单详情
        $order = OrderModel::getUserOrderDetail($order_id, $this->user['user_id']);
        if($data = $order->userPay($pay_mode)){
            return $this->renderSuccess($data);
        }
        $error = $order->getError() ?: '支付失败';
        return $this->renderError($error);
    }

}
