<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\food\Order as OrderModel;
use app\api\model\food\CouponUser as CouponUserModel;

/**
 * 个人中心主页
 */
class Index extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 获取当前用户信息
     */
	public function detail()
    {
        // 当前用户信息
        $userInfo = $this->user;
        //优惠券数量
        $couponCount = CouponUserModel::getUserCount($userInfo['user_id']);
        // 订单总数
        $model = new OrderModel;
        $orderCount = [
            'payment' => $model->getOrderClassCount('payment',$this->shop_id,$userInfo['user_id']),
            'comment' => $model->getOrderClassCount('comment',$this->shop_id,$userInfo['user_id']),
            'refund10' => $model->getOrderClassCount('refund10',$this->shop_id,$userInfo['user_id'])
        ];
        return $this->renderSuccess(compact('userInfo', 'orderCount','couponCount'));
    }
	
}
