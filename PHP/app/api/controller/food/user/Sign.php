<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\SignLog as SignLogModel;

/**
 * 签到管理
 */
class Sign extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 是否签到
     */
    public function isSign()
    {
        $model = new SignLogModel;
        $isSign = $model->getIsSign((int)$this->user['user_id']);
        return $this->renderSuccess(compact('isSign'));
    }

    /**
     * 签到
     */
    public function sign()
    {
        $model = new SignLogModel;
        if($model->add(['user_id' => $this->user['user_id']])){
            return $this->renderMsg('签到成功');
        }
        return $this->renderError('签到失败');
    }
}
