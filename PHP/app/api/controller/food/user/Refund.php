<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\food\Order as OrderModel;
use think\facade\Cache;
use hema\Helper;

/**
 * 用户订单管理
 */
class Refund extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }
	
	/**
     * 订单详情
     */
    public function detail($order_id)
    {
		$model = new OrderModel;
		$order = $model->getUserOrderDetail($order_id, $this->user['user_id']);
		Cache::set('refund_' . $order_id . '_' . $this->user['user_id'] . '_' . $this->shop_id, $order->toArray(), 86400);//存贮24小时
		return $this->renderSuccess(compact('order'));
    }
	
	/**
     * 订单退款
     */
    public function refund()
    {
    	$data = $this->request->post();
		$model = OrderModel::get($data['order_id']);
		$model->userRefund($data);
		return $this->renderMsg('申请成功');
    }
	
	/**
     * 修改订单
	 * $mode 参数  dec=减少 inc=增加
	 * $index 产品在数组中的编号
     */
    public function edit($order_id,$mode,$index)
    {
		$refund = Cache::get('refund_' . $order_id . '_' . $this->user['user_id'] . '_' . $this->shop_id);
		if(!$refund){
			return $this->renderError('订单不存在');
		}
		if(!isset($refund['refund_pack'])){
			$refund['refund_pack'] = 0;//退打包费
		}
		if(!isset($refund['refund_express'])){
			$refund['refund_express'] = 0;//退配送费
		}
		if(!isset($refund['refund_ware'])){
			$refund['refund_ware'] = 0;//餐具调料费
		}

		//增加操作
		if($mode == 'inc'){
			$refund['goods'][$index]['refund_num'] = $refund['goods'][$index]['refund_num']+1;
			$refund['goods'][$index]['refund_price'] = $refund['goods'][$index]['refund_price']+$refund['goods'][$index]['goods_price'];
			$refund['refund_price'] = $refund['refund_price'] + $refund['goods'][$index]['goods_price'];
			//打包费退减
			if($refund['order_mode']['value'] != 10){
				$refund['refund_pack'] = $refund['refund_pack'] + $refund['goods'][$index]['goods']['pack_price'];
				$refund['refund_price'] = $refund['refund_price'] + $refund['goods'][$index]['goods']['pack_price'];
			}

		}else{
			$refund['goods'][$index]['refund_num'] = $refund['goods'][$index]['refund_num']-1;
			$refund['goods'][$index]['refund_price'] = $refund['goods'][$index]['refund_price']-$refund['goods'][$index]['goods_price'];
			$refund['refund_price'] = $refund['refund_price'] - $refund['goods'][$index]['goods_price'];
			//打包费退减
			if($refund['order_mode']['value'] != 10){
				$refund['refund_pack'] = $refund['refund_pack'] - $refund['goods'][$index]['goods']['pack_price'];
				$refund['refund_price'] = $refund['refund_price'] - $refund['goods'][$index]['goods']['pack_price'];
			}
			$refund['refund_price'] = $refund['refund_price'] - $refund['refund_express']-$refund['refund_ware'];
		}

		if($refund['refund_price']+$refund['express_price']+$refund['ware_price']>=$refund['pay_price']){
			$refund['refund_price']=$refund['pay_price'];
			$refund['refund_express']=$refund['express_price'];//退配送费
			$refund['refund_ware']=$refund['ware_price'];//餐具调料费
		}
		$refund['refund_price'] = helper::number2($refund['refund_price']);
		Cache::set('refund_' . $order_id . '_' . $this->user['user_id'] . '_' . $this->shop_id,$refund,86400);
		return $this->renderSuccess($refund['refund_price']);
	}
}
