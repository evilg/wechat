<?php
namespace app\api\controller\food\user;

use app\api\controller\food\Controller;
use app\api\model\food\Pact as PactModel;

/**
 * 预约排号管理
 */
class Pactsort extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 获取用户排队信息
     */
    public function detail()
    {
        $model = new PactModel;
        $detail['sort'] = $model->getUserSort($this->shop_id,$this->user['id']);
        $detail['count'] = PactModel::getNowCount(10,$this->shop_id);
        return $this->renderSuccess(compact('detail'));
    }

    /**
     * 取消排队
     */
    public function cancel($pact_id)
    {
        $model = PactModel::get($pact_id);
        if($model->save(['status' => 40])){
            return $this->renderMsg('操作成功');
        }
        return $this->renderError('操作失败');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new PactModel;
        if($model->add($this->request->post(),$this->user['id'])){
            return $this->renderMsg('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
}
