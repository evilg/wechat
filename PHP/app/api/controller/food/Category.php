<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Category as CategoryModel;

/**
 * 商品分类控制器
 */
class Category extends Controller
{
    /**
     * 全部分类
     */
    public function index()
    {
        $list = array_values(CategoryModel::getCacheTree($this->shop_id));
        return $this->renderSuccess(compact('list'));
    }
}
