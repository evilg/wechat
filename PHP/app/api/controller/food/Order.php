<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Order as OrderModel;
use app\api\model\food\Cart as CartModel;
use app\api\model\food\Goods as GoodsModel;
use app\api\model\food\Shop as ShopModel;
use app\api\model\food\Flavor as FlavorModel;
use app\api\model\food\Table as TableModel;
use app\api\model\food\Device;
use hema\Helper;

/**
 * 订单控制器
 */
class Order extends Controller
{
    private $user;

    /**
     * 构造方法
     */
    public function initialize()
    {
        parent::initialize();
        $this->user = $this->getUserDetail();   // 用户信息
    }

    /**
     * 订单确认-购物车结算
	 * $order_mode 点餐模式 10店内，20外卖，30打包
	 * $pay_mode 支付模式 0微信，1余额，2后付款
	 * 店内就餐参数 $people=就餐人数,$ware_price=餐具调料费,$order_pay_price=0
	 * $message=买家留言
	 * $flavor=口味选择
	 * $arrive_time = 到店时间
     */
    public function cart($order_mode,$pay_mode = 0,$people=0,$table_id='',string $message = '', string $flavor = '', $arrive_time = 0, $coupon_user_id=0)
    {
		$model =  new FlavorModel;
		$flavor_list = $model->getList($this->shop_id);//获取口味选项
        // 商品结算信息
        $model = new OrderModel;
        $order = $model->getCart($this->user,$order_mode,$this->shop_id,$coupon_user_id,$table_id);
		$order['flavor_list'] = $flavor_list;
        $shop = ShopModel::get($this->shop_id);
        //堂食
        if($order_mode==10){
            $order['tang_mode']=$shop['tang_mode'];
            //扫码
            if($order['tang_mode']==1 AND $table_id > 0){
            	$table = TableModel::get($table_id);
				//如果餐桌状态空闲中才进行验证，否则为加单
				if($table['status']['value'] == 10){
				    $order['is_order'] = 1;//点单
			        $order['table_name'] = $table['table_name'] . '（点单）';
    				//餐桌容纳人数
    				if($table['people'] < $people){
    					return $this->renderError('该（包间/餐桌）只能容纳'.$table['people'].'人');
    				}
    				//最低消费
    				if($table['consume'] > $order['order_pay_price']){
    					return $this->renderError('该（包间/餐桌）未达到最低消费￥'.$table['consume'].'元');
    				}
    				//计算所需餐具调料费
    				if($table['ware_price']>0){
    					$order['ware_price'] = Helper::number2($table['ware_price'] * $people);
    					$order['order_pay_price'] = Helper::number2($order['order_pay_price']+$order['ware_price']);
    				}
				}else{
				    $order['is_order'] = 0;//加单
			        $order['table_name'] = $table['table_name'] . '（加单）';
				}
            }else{
                $order['is_order'] = 1;//点单
			    $order['table_name'] = '';
            }
        }
        if (!$this->request->isPost()) {
            return $this->renderSuccess($order);
        }
        //堂食
        if($order_mode==10 AND $order['tang_mode']==1){
    		//扫码
    		$order['table_id'] = $table_id;
			$order['people'] = $people;
        }else{
        	//排号
        	$order['row_no'] = row_no($this->applet_id,$this->shop_id);
        }
        //自取订单计算到店时间
		if($order_mode==30){
			$order['arrive_time'] = time() + $arrive_time * 60;
		}
		$order['message'] = $message;
		$order['flavor'] = $flavor;
		$order['order_mode'] = $order_mode;
		$order['user_id'] = $this->user['user_id'];
		$order['shop_id'] = $this->shop_id;
		$order['applet_id'] = $this->applet_id;
        // 创建订单
        if (!$model->add($order,$coupon_user_id)) {
			$error = $model->getError() ?: '订单创建失败';
			return $this->renderError($error);  
        }
		// 清空购物车
		$Card = new CartModel($this->user['user_id'],$this->shop_id,$this->applet_id,$table_id);
		$Card->clearAll();
		$order = OrderModel::detail($model['order_id']);
		if($data = $order->userPay($pay_mode)){
            return $this->renderSuccess($data);
        }
        $error = $order->getError() ?: '支付失败';
        return $this->renderError($error);
    }

    /**
     * 呼叫服务员
     */
    public function spk($order_id,$mode)
    {
        $model = OrderModel::detail($order_id);
        Device::push($this->shop_id,$mode,$model['table']['row_no']);
        return $this->renderMsg('操作成功');
    }
}
