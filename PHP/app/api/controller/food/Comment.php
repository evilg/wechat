<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Comment as CommentModel;

/**
 * 订单评论
 */
class Comment extends Controller
{
    /**
     * 门店列表
     */
    public function lists()
    {
        $model = new CommentModel;
        $list = $model->getList(0,$this->shop_id,1);
        return $this->renderSuccess(compact('list'));
    }
	
	/**
     * 门店评分
     */
    public function score()
    {
        $model = new CommentModel;
        $score = $model->score($this->shop_id);
        return $this->renderSuccess(compact('score'));
    }

}
