<?php
namespace app\api\controller\food;

use app\api\controller\food\Controller;
use app\api\model\food\Goods as GoodsModel;
use app\api\model\food\Category as CategoryModel;

/**
 * 商品控制器
 */
class Goods extends Controller
{
    /**
     * 商品列表 - 点餐版接口
     */
    public function lists($table_id = '')
    {
        $user = $this->getUserDetail();
        $model = new GoodsModel;
        $category = array_values(CategoryModel::getCacheTree($this->shop_id));
        $result = $model->getGoodsList($this->shop_id,$category,$user,$table_id);
        $category = $result['category'];
        $order_total_num = $result['cartlist']['order_total_num'];
        $order_total_price = $result['cartlist']['order_total_price'];
        $min_price = $result['cartlist']['min_price'];
        $goodslist = $result['cartlist']['goods_list'];
        return $this->renderSuccess(compact('category','order_total_num','order_total_price','min_price','goodslist'));
    }

    /**
     * 获取商品详情
     */
    public function detail($goods_id)
    {
        // 商品详情
        $detail = GoodsModel::detail($goods_id);
        if (!$detail || $detail['goods_status']['value'] != 10) {
            return $this->renderError('很抱歉，商品信息不存在或已下架');
        }
        // 规格信息
        $specData = $detail['spec_type'] == 20 ? $detail->getManySpecData($detail['spec_rel'], $detail['spec']) : null;
        //$user = $this->getUserDetail();
        // 购物车商品总数量
        //$cart_total_num = (new CartModel($user['user_id'],$this->shop_id))->getTotalNum();
        return $this->renderSuccess(compact('detail', /*'cart_total_num',*/ 'specData'));
    }

}
