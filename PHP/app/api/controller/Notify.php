<?php
namespace app\api\controller;

use hema\wechat\Pay as WxPay;
use app\api\model\Setting;
use app\api\model\Record as RecordModel;


/**
 * 支付成功异步通知接口
 */
class Notify
{
    /**
     * 站点扫码支付成功异步通知
     */
    public function native()
    {
        $values = Setting::getItem('webpay',0);
        $WxPay = new WxPay([]);
        $WxPay->notify(new RecordModel,'add',$values['wx']['api_key']);
    }
    
    /**
     * 微信小程序充值成功异步通知
     */
    public function wxapp()
    {
        $values = Setting::getItem('webpay',0);
        $WxPay = new WxPay([]);
        $WxPay->notify(new RecordModel,'add',$values['wx']['api_key']);
    }
}
