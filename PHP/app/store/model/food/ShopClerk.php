<?php
namespace app\store\model\food;

use app\common\model\food\ShopClerk as ShopClerkModel;
use app\store\model\Applet;
use think\facade\Session;

/**
 * 店员模型
 */
class ShopClerk extends ShopClerkModel
{
    /**
     * 店长登录登录
     */
    public function login($data)
    {
        if(!captcha_check($data['captcha'])){
            $this->error = '验证码错误';
            return false;
        }
        if(!$clerk = $this->where(['mobile' => $data['user_name'],'status' => 20])->find()){
            $this->error = '账号不存在';
            return false;
        }
        if($clerk['pwd'] != hema_hash($data['password'])){
            $this->error = '密码错误';
            return false;
        }
        $applet = Applet::get($clerk['applet_id']);
		$user = User::getUser(['user_id' => $applet['user_id']]);
		$user['user_name'] = $data['user_name'];
        Session::set('hema_store_foodclerk',[
        	'user' => $user,
        	'applet' => $applet,
        	'shop_id' => $clerk['shop_id'],
        	'is_login' => true,
        ]);
        return true;
    }
    
    /**
     * 修改密码
     */
    public function renew(array $data)
    {    
        if(strlen($data['password']) < 6){
            $this->error = '密码长度不足6位';
            return false;
        }
        if ($data['password'] != $data['password_confirm']) {
            $this->error = '两次密码输入不一致';
            return false;
        }
        $data['pwd'] = hema_hash($data['password']);
        return $this->save($data) !== false;
    }
}
