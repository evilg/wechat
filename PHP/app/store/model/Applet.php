<?php
namespace app\store\model;

use app\common\model\Applet as AppletModel;

/**
 * 微信小程序模型
 */
class Applet extends AppletModel
{
    /**
     * 小程序登录 - 模板端
     */
    public function login($data)
    {
        if(!captcha_check($data['captcha'])){
            $this->error = '验证码错误';
            return false;
        }
        if(!$applet = Applet::get($data['user_name'])){
            $this->error = '小程序不存在';
            return false;
        }
        if(empty($applet['password'])){
            $this->error = '通过用户中心一键登录，初始化密码后在登录！';
            return false;
        }else{
            if($applet['password'] != hema_hash($data['password'])){
                $this->error = '密码错误';
                return false;
            }
        }
		$user = User::getUser(['user_id' => $applet['user_id']]);
        Session::set('hema_store_' . $applet['app_type'],[
        	'user' => $user,
        	'applet' => $applet,
        	'is_login' => true,
        ]);
        return '/store/' . $applet['app_type'] . '.index/index';
    }
}
