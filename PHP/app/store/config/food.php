<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'food.index/index',
    ],
    'applet' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'food.applet.page/index',
        'submenu' => [
			[
				'name' => '页面管理',
				'index' => 'food.applet.page/index',
			],
			[
				'name' => '常用链接',
				'index' => 'food.applet.page/links',
			],
			[
				'name' => '帮助文档',
				'index' => 'food.applet.help/index',
			],
        ],
    ],
    'shop' => [
        'name' => '门店管理',
        'icon' => 'icondianpu',
        'index' => 'food.shop/index',
		'submenu' => [
		    [
		        'name' => '门店列表',
		        'index' => 'food.shop/index',
				'urls' => [
				    'food.shop/index',
				    'food.shop/add',
				    'food.shop/edit',
				    'food.shop/delete'
				]
		    ],
		    [
		        'name' => '店员管理',
		        'index' => 'food.shop.clerk/index',
				'urls' => [
				    'food.shop.clerk/index',
				    'food.shop.clerk/add',
				    'food.shop.clerk/edit',
				    'food.shop.clerk/delete'
				]
		    ],
			[
		        'name' => '餐桌管理',
		        'index' => 'food.shop.table/index',
				'urls' => [
				    'food.shop.table/index',
				    'food.shop.table/add',
				    'food.shop.table/edit',
				    'food.shop.table/delete'
				]
		    ],
			[
		        'name' => '口味选项',
		        'index' => 'food.shop.flavor/index',
				'urls' => [
				    'food.shop.flavor/index',
				    'food.shop.flavor/add',
				    'food.shop.flavor/edit',
				    'food.shop.flavor/delete'
				]
		    ],
		    [
		        'name' => '智能设备',
		        'index' => 'food.shop.device/index',
				'urls' => [
				    'food.shop.device/index',
				    'food.shop.device/add',
				    'food.shop.device/edit',
				    'food.shop.device/delete',
				    'food.shop.device/status'
				]
		    ],
		]
    ],
    'goods' => [
        'name' => '商品管理',
        'icon' => 'iconshangpinguanli',
        'index' => 'food.goods/opt',
        'submenu' => [
            [
                'name' => '商品列表',
                'index' => 'food.goods/opt',
                'uris' => [
					'food.goods/opt',
					'food.goods/copys',
                    'food.goods/index',
                    'food.goods/add',
                    'food.goods/edit',
					'food.goods/delete'
                ],
            ],
            [
                'name' => '商品分类',
                'index' => 'food.goods.category/opt',
                'uris' => [
					'food.goods.category/opt',
                    'food.goods.category/index',
                    'food.goods.category/add',
                    'food.goods.category/edit',
					'food.goods.category/delete'
                ],
            ]
        ],
    ],
    'order' => [
        'name' => '订单管理',
        'icon' => 'icondingdanguanli',
        'index' => 'food.order/all_list',
        'submenu' => [
            [
                'name' => '全部订单',
                'index' => 'food.order/all_list',
				'uris' => [
					'food.order/all_list'
				],
            ],
            [
				'name' => '待收款',
				'index' => 'food.order/collection_list',
				'uris' => [
					'food.order/collection_list'
				],
			],
            [
				'name' => '待接单',
				'index' => 'food.order/shop_list',
				'uris' => [
					'food.order/shop_list'
				],
			],
			[
				'name' => '待发货',
				'index' => 'food.order/delivery_list',
				'uris' => [
					'food.order/delivery_list'
				],
			],
			[
				'name' => '待收货',
				'index' => 'food.order/receipt_list',
				'uris' => [
					'food.order/receipt_list'
				],
			],
			
			[
				'name' => '已完成',
				'index' => 'food.order/complete_list',
				'uris' => [
					'food.order/complete_list'
				],

			],
			[
				'name' => '已取消',
				'index' => 'food.order/cancel_list',
				'uris' => [
					'food.order/cancel_list'
				],
			],
			[
                'name' => '退款订单',
                'active' => false,
                'submenu' => [
                    [
						'name' => '全部订单',
						'index' => 'food.order/refund_list',
						'uris' => [
							'food.order/refund_list'
						],
					],
					[
						'name' => '待退款',
						'index' => 'food.order/refund10_list',
						'uris' => [
							'food.order/refund10_list'
						],
					],
					[
						'name' => '已退款',
						'index' => 'food.order/refund20_list',
						'uris' => [
							'food.order/refund20_list'
						],
					],
                ]
            ],
			[
                'name' => '预约订单',
                'active' => false,
                'submenu' => [
                    [
						'name' => '排号等座',
						'index' => 'food.order.pact/sorts',
						'uris' => [
							'food.order.pact/sorts'
						],
					],
					[
						'name' => '预约订桌',
						'index' => 'food.order.pact/table',
						'uris' => [
							'food.order.pact/table'
						],
					],
                ]
            ],
			[
                'name' => '订单评价',
                'index' => 'food.order.comment/index',
				'uris' => [
					'food.order.comment/index',
					'food.order.comment/edit',
				],
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'food.user/index',
        'submenu' => [
            [
                'name' => '用户列表',
                'index' => 'food.user/index',
            ],
			[
                'name' => '等级管理',
                'index' => 'food.user.grade/index',
				'uris' => [
					'food.user.grade/index',
					'food.user.grade/add',
					'food.user.grade/edit',
					'food.user.grade/delete',
				],
            ],
			[
                'name' => '交易记录',
                'index' => 'food.user.pay/index',
				'uris' => [
					'food.user.pay/index'
				],
            ],
            [
                'name' => '收货地址',
                'index' => 'food.user.addres/index',
				'uris' => [
					'food.user.addres/index'
				],
            ],
            [
                'name' => '购物车',
                'index' => 'food.user.cart/index',
				'uris' => [
					'food.user.cart/index'
				],
            ],
        ]
    ],
	'market' => [
        'name' => '营销管理',
        'icon' => 'iconyingxiao',
        'index' => 'food.market.coupon/index',
        'submenu' => [
			[
                'name' => '优惠券',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '优惠券管理',
                        'index' => 'food.market.coupon/index',
                        'uris' => [
                            'food.market.coupon/index',
                            'food.market.coupon/add',
                            'food.market.coupon/edit',
                            'food.market.coupon/delete'
                        ],
                    ],
                    [
                        'name' => '发券管理',
                        'index' => 'food.market.coupon.log/index',
                        'uris' => [
                            'food.market.coupon.log/index',
                            'food.market.coupon.log/add',
                            'food.market.coupon.log/delete'
                        ],
                    ],
                ]
            ],
			[
                'name' => '用户充值',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '充值套餐',
                        'index' => 'food.market.recharge.plan/index',
						'uris' => [
							'food.market.recharge.plan/index',
							'food.market.recharge.plan/add',
							'food.market.recharge.plan/edit',
							'food.market.recharge.plan/delete',
						],
                    ],
					[
                        'name' => '充值设置',
                        'index' => 'food.market.recharge/setting',
                    ],
                ]
            ],
		],
    ],
	'statistics' =>[
		'name' => '数据统计',
        'icon' => 'icontongji',
        'index' => 'food.statistics/opt',
		'urls' => [
			'food.statistics/opt',
			'food.statistics/index'
		]
	],
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'food.setting/other',
        'submenu' => [
            [
                'name' => '基本设置',
                'index' => 'food.setting/other',
            ],
            [
                'name' => '交易设置',
                'index' => 'food.setting/trade',
            ],
            [
                'name' => '配送设置',
                'index' => 'food.setting/delivery',
            ],
            [
                'name' => '预约设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '预约排队',
                        'index' => 'food.setting/pactsort'
                    ],
                    [
                        'name' => '预约订桌',
                        'index' => 'food.setting/pacttable'
                    ],
                ]
            ],
            [
                'name' => '消息设置',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '微信小程序',
                        'index' => 'food.setting/wxapptpl'
                    ],
                    [
                        'name' => '微信公众号',
                        'index' => 'food.setting/wechattpl'
                    ],
                ]
            ],
            [
                'name' => '清理缓存',
                'index' => 'food.setting.cache/clear',
            ],
            [
                'name' => '登录密码',
                'index' => 'food.setting/renew',
            ],
        ],
    ],
];
