<?php
namespace app\store\controller;

use app\store\model\Setting;
use think\facade\View;
use think\response\Json;

/**
 * 控制器基类
 */
class Controller extends \app\BaseController
{
    /**
     * 后台初始化
     */
    public function initialize()
    { 
        $this->layout(); // 全局layout
    }


    /**
     * 全局layout模板输出
     */
    private function layout()
    {
        // 输出到view
        View::assign([
            'web' => Setting::getItem('web',0),    //站点设置
        ]);
    }

    
    /**
     * 返回封装后的 API 数据到客户端
     */
    protected function renderJson($code = 1, string $msg = '', string $url = '', array $data = [])
    {
        return json(compact('code', 'msg', 'url', 'data'));
    }

    /**
     * 返回操作成功json
     */
    protected function renderSuccess(string $msg = 'success', string $url = '', array $data = [])
    {
        return $this->renderJson(1, $msg, $url, $data);
    }

    /**
     * 返回操作失败json
     */
    protected function renderError(string $msg = 'error', string $url = '', array $data = [])
    {
        return $this->renderJson(0, $msg, $url, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key = null)
    {
        return $this->request->post(empty($key) ? '' : "{$key}/a");
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postForm($key = 'form')
    {
        return $this->postData($key);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function getData($key = null)
    {
        return $this->request->get(is_null($key) ? '' : $key);
    }
}
