<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\food\Order as OrderModel;
use app\store\model\food\Comment as CommentModel;
use app\store\model\food\Record as RecordModel;
use app\store\model\food\Goods as GoodsModel;
use app\store\model\food\User as UserModel;
use app\store\model\food\Shop as ShopModel;
use app\store\model\food\Pact as PactModel;
use think\facade\View;


/**
 * 商户后台首页
 */
class Index extends Controller
{
	
    public function index($shop_id = 0)
    {
        if($this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
		$model = new ShopModel;
	    $shop = $model->getList();
		$count = array();
		$count['order'] = OrderModel::getCount($shop_id);	//订单和收入统计
		$count['comment'] = CommentModel::getCount($shop_id);	//评价统计
		$count['record'] = RecordModel::getCount($shop_id);	//充值统计
		$count['user'] = UserModel::getCount();	//用户统计
		$count['goods'] = GoodsModel::getCount($shop_id);	//产品统计
		$count['pact'] = PactModel::getCount($shop_id);	//预约统计
		return View::fetch('index', compact('count','shop','shop_id'));
    }
}
