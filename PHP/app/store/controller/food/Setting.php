<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\food\Setting as SettingModel;
use app\store\model\Applet as AppletModel;
use think\facade\View;

/**
 * 系统设置
 */
class Setting extends Controller
{	
	/**
     * 功能设置
     */
    public function mode()
    {
        return $this->updateEvent('mode');
    }

    /**
     * 交易设置
     */
    public function trade()
    {
        return $this->updateEvent('trade');
    }
	
	/**
     * 配送设置
     */
    public function delivery()
    {
        return $this->updateEvent('delivery');
    }

    /**
     * 其它设置
     */
    public function other()
    {
        return $this->updateEvent('other');
    }

    /**
     * 微信小程序订阅消息设置
     */
    public function wxapptpl()
    {
        return $this->updateEvent('wxapptpl');
    }

    /**
     * 微信公众号模板消息设置
     */
    public function wechattpl()
    {
        return $this->updateEvent('wechattpl');
    }

    /**
     * 预约订桌设置
     */
    public function pacttable()
    {
        return $this->updateEvent('pacttable');
    }

    /**
     * 预约排队设置
     */
    public function pactsort()
    {
        return $this->updateEvent('pactsort');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
			$model = SettingModel::getItem($key);
            return View::fetch($key, compact('model'));
        }
        if($this->user['applet']['status']['value']==0 AND $key=='wxapptpl'){
            return $this->renderError('请先绑定您的小程序');
        }
        $model = new SettingModel;
        if($model->edit($key, $this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }
    
    /**
     * 更新密码
     */
    public function renew()
    {
        $model = AppletModel::get($this->applet_id);
        if (!$this->request->isAjax()) {
            return View::fetch('renew', compact('model'));            
        }
        if ($model->renew($this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError($model->getError() ?: '更新失败');
    }
}
