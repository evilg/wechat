<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\food\Category;
use app\store\model\food\Goods as GoodsModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 商品管理控制器
 */
class Goods extends Controller
{
	/**
	 * 门店选择
	 */
	public function opt()
	{
		if($this->shop_mode == 10){
			return redirect(url('food.goods/index',['shop_id' => $this->shop_id]));
		}
        $model = new ShopModel;
        $shoplist = $model->getList(false);
        return View::fetch('opt', compact('shoplist'));
	}
	
    /**
     * 商品列表
     */
    public function index($shop_id,$category_id = 0, string $search = '')
    {
		$shop = ShopModel::get($shop_id);
		$shop_name = $shop['shop_name'];
        // 分类
        $category = Category::getCacheTree($shop_id);
        $model = new GoodsModel;
        $list = $model->getList($shop_id,0,$category_id,$search);
        return View::fetch('index', compact('list','category','shop_id','category_id','shop_name','search'));
    }

    /**
     * 添加
     */
    public function add($shop_id)
    {
        $this->shop_id = $shop_id;
        if (!$this->request->isAjax()) {
            // 分类
            $category = Category::getCacheTree($shop_id);
            return View::fetch('add', compact('category','shop_id'));
        }
        $model = new GoodsModel;
        if ($model->add($this->postData('data'),$shop_id)) {
            return $this->renderSuccess('添加成功', url('food.goods/index',['shop_id' => $shop_id]));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除商品
     */
    public function delete($id)
    {
        $model = new GoodsModel;
        if ($model->remove($id)) {
           return $this->renderSuccess('删除成功'); 
        }
		$error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }
	
	
	/**
     * 上架/下架
     */
	public function status($id)
	{
		$model = GoodsModel::get($id);
		if($model->status()){
			return $this->renderSuccess('操作成功');
		}
		$error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
	}

    /**
     * 编辑
     */
    public function edit($id, $shop_id)
    {
        $this->shop_id = $shop_id;
        // 菜品详情
        $model = GoodsModel::detail($id);
        if (!$this->request->isAjax()) {
            // 菜品分类
            $category = Category::getCacheTree($shop_id);
            // 多规格信息
            $specData = 'null';
            if ($model['spec_type'] == 20)
                $specData = json_encode($model->getManySpecData($model['spec_rel'], $model['spec']));
            return View::fetch('edit', compact('model', 'category', 'specData','shop_id'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('food.goods/index',['shop_id' => $shop_id]));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 一键复制
     */
    public function copys($id, $shop_id)
    {
        $this->shop_id = $shop_id;
        // 菜品详情
        $model = GoodsModel::detail($id);
        if (!$this->request->isAjax()) {
			$shop = $this->category($shop_id);
			$shoplist = $shop['shoplist'];//门店列表
			$category = $shop['category'];//门店商品分类
            // 多规格信息
            $specData = 'null';
            if ($model['spec_type'] == 20)
                $specData = json_encode($model->getManySpecData($model['spec_rel'], $model['spec']));
            return View::fetch('copys', compact('model', 'category', 'shoplist', 'specData','shop_id'));
        }
		$model = new GoodsModel;
		$data = $this->postData('data');
        // 更新记录
        if ($model->add($data,$data['shop_id'])) {
            return $this->renderSuccess('复制成功', url('food.goods/index',['shop_id' => $shop_id]));
        }
        $error = $model->getError() ?: '复制失败';
        return $this->renderError($error);
    }

	
	/**
     * 获取门店列表和分类列表
     */
	private function category($shop_id)
	{
		$model = new ShopModel;
        $list = $model->getList(false);
		$category = []; //商品分类
		$shoplist = []; //门店列表
		for($n=0;$n<sizeof($list);$n++){
			if($list[$n]['shop_id'] != $shop_id){
				array_push($shoplist,$list[$n]);
				$category[$list[$n]['shop_id']] = Category::getCacheTree($list[$n]['shop_id']);
			}
		}
		return compact('category','shoplist');
	}
	
}
