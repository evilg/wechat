<?php
namespace app\store\controller\food\setting;

use app\store\controller\food\Controller;
use think\facade\Cache as Driver;
use think\facade\View;

/**
 * 清理缓存
 */
class Cache extends Controller
{
    /**
     * 清理缓存
     */
    public function clear($isForce = false)
    {
        if ($this->request->isAjax()) {
            $data = $this->postData('data');
            $this->rmCache($data['keys'], isset($data['isForce']) ? !!$data['isForce'] : false);
            return $this->renderSuccess('操作成功');
        }
        return View::fetch('clear', [
            'cacheList' => $this->getCacheKeys(),
            'isForce' => !!$isForce ?: config('app_debug'),
        ]);
    }

    /**
     * 删除缓存
     */
    private function rmCache($keys, $isForce = false)
    {
        if ($isForce === true) {
            Driver::clear();
        } else {
            $cacheList = $this->getCacheKeys();
            foreach (array_intersect(array_keys($cacheList), $keys) as $key) {
                Driver::delete($cacheList[$key]['key']);
            }
        }
    }

    /**
     * 获取缓存索引数据
     */
    private function getCacheKeys()
    {
        $applet_id = $this->applet_id;
        return [
            'setting' => [
                'key' => 'food_setting_' . $applet_id,
                'name' => '站点设置'
            ],
            'category' => [
                'key' => 'food_category_' . $applet_id,
                'name' => '商品分类'
            ],
            'applet' => [
                'key' => 'food_applet_' . $applet_id,
                'name' => '小程序设置'
            ],
        ];
    }

}
