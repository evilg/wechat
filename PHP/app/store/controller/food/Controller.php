<?php
namespace app\store\controller\food;

use app\store\model\Setting as SettingModel;
use app\store\model\food\Shop;
use think\facade\View;
use think\facade\Session;

/**
 * 控制器基类
 */
class Controller extends \app\BaseController
{
    protected $user;        //用户登录信息
	protected $user_id = '';//商户id
	protected $applet_id = '';//app_id
	protected $agent_id = '';//代理id
	protected $shop_id; //门店id
	protected $shop_mode;//门店模式 10=单门店 20=多门店
    protected $app_type = 'food';    //应用类型
    protected $controller = '';//当前控制器名称
    protected $action = '';//当前方法名称
    protected $routeUri = '';//当前路由uri
    protected $group = '';//当前路由：分组名称

    /* 登录验证白名单 */
    protected $allowAllAction = [];

    /**
     * 后台初始化
     */
    public function initialize()
    { 
        $this->getUserInfo();// 获取登录信息
        $this->getRouteInfo();// 当前路由信息
        $this->checkLogin();// 验证登录状态
        $this->layout(); // 全局layout
    }

    /**
     * 初始化应用数据
     */
    protected function getUserInfo()
    {
        $this->user = Session::get('hema_store_' . $this->app_type);// 商家登录信息
    }

    /**
     * 解析当前路由参数 （分组名称、控制器名称、方法名）
     */
    protected function getRouteInfo()
    {
        // 控制器名称
        $this->controller = uncamelize($this->request->controller());
        $controller = str_replace($this->app_type . '.','',$this->controller); //取消掉 'food.'
        // 方法名称
        $this->action = $this->request->action();
        // 控制器分组 (用于定义所属模块)
        $group = strstr($controller, '.', true);//返回‘.’开始往后的字符
        $this->group = $group !== false ? $group : $controller;
        // 当前uri
        $this->routeUri = "{$this->controller}/$this->action";
    }

    /**
     * 验证登录状态
     */
    private function checkLogin()
    {
        // 验证当前请求是否在白名单
        if (in_array($this->routeUri, $this->allowAllAction)) {
            return true;
        }
        // 验证登录状态
        if (empty($this->user)
            || (int)$this->user['is_login'] !== 1
            || !isset($this->user['applet'])
            || empty($this->user['applet'])
        ) {
            return redirect(url('passport/login'))->send();
        }
        $this->agent_id = $this->user['applet']['agent_id'];//代理id
        $this->user_id = $this->user['user']['user_id'];//商户id
        $this->shop_mode = $this->user['applet']['shop_mode']['value'];//门店模式
        $this->applet_id = $this->user['applet']['applet_id'];//应用ID
        //如果是单门店
        if($this->shop_mode == 10){
            $shop = Shop::getShop();
            $this->shop_id = $shop['shop_id'];
        }
        return true;
    }

    /**
     * 全局layout模板输出
     */
    private function layout()
    {
        $info = get_addons_info($this->app_type);
        // 输出到view
        View::assign([
            'base_url' => base_url(),            // 当前站点域名
            'web' => SettingModel::getItem('web',0),    //站点设置
            'store_url' => '/store/' . $this->app_type . '.',       // 后台模块url
            'group' => $this->group,
            'menus' => $this->menus(),          // 后台菜单
            'user' => $this->user,            // 登录信息
            'app_type' => $this->app_type,      //小程序类型
            'applet_id' => $this->applet_id,      //小程序ID
            'logout' => 'applet', //退出登录方式 applet=小程序退出 logout=其他管理端退出
            'app_name' => '智慧餐厅',      //小程序名称
            'version' => $info['version']
        ]);
    }

    /**
     * 后台菜单配置
     */
    private function menus()
    {
		foreach ($data = Config($this->app_type) as $group => $first) {
            $data[$group]['active'] = $group === $this->group;

			//$data[0]['active']
            // 遍历：二级菜单
            if (isset($first['submenu'])) {
                foreach ($first['submenu'] as $secondKey => $second) {
                    // 二级菜单所有uri
                    $secondUris = [];
                    if (isset($second['submenu'])) {
                        // 遍历：三级菜单
                        foreach ($second['submenu'] as $thirdKey => $third) {
                            $thirdUris = [];
                            if (isset($third['uris'])) {
                                $secondUris = array_merge($secondUris, $third['uris']);
                                $thirdUris = array_merge($thirdUris, $third['uris']);
                            } else {
                                $secondUris[] = $third['index'];
                                $thirdUris[] = $third['index'];
                            }
                            $data[$group]['submenu'][$secondKey]['submenu'][$thirdKey]['active'] = in_array($this->routeUri, $thirdUris);
							$data[$group]['submenu'][$secondKey]['active'] = in_array($this->routeUri, $secondUris);
                        }
                    } else {
                        if (isset($second['uris']))
                            $secondUris = array_merge($secondUris, $second['uris']);
                        else
                            $secondUris[] = $second['index'];
                    }
                    // 二级菜单：active
                    !isset($data[$group]['submenu'][$secondKey]['active'])
                    && $data[$group]['submenu'][$secondKey]['active'] = in_array($this->routeUri, $secondUris);	
                }
            }
        }
        return $data;
    }

    /**
     * 返回封装后的 API 数据到客户端
     */
    protected function renderJson($code = 1, string $msg = '', string $url = '', array $data = [])
    {
        return json(compact('code', 'msg', 'url', 'data'));
    }

    /**
     * 返回操作成功json
     */
    protected function renderSuccess(string $msg = 'success', string $url = '', array $data = [])
    {
        return $this->renderJson(1, $msg, $url, $data);
    }

    /**
     * 返回操作失败json
     */
    protected function renderError(string $msg = 'error', string $url = '', array $data = [])
    {
        return $this->renderJson(0, $msg, $url, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key = null)
    {
        return $this->request->post(empty($key) ? '' : "{$key}/a");
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postForm($key = 'form')
    {
        return $this->postData($key);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function getData($key = null)
    {
        return $this->request->get(is_null($key) ? '' : $key);
    }
}
