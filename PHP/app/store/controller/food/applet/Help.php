<?php
namespace app\store\controller\food\applet;

use app\store\controller\food\Controller;
use app\store\model\food\Help as HelpModel;
use think\facade\View;

/**
 * 小程序帮助中心
 */
class Help extends Controller
{
    /**
     * 帮助中心列表
     */
    public function index()
    {
        $model = new HelpModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new HelpModel;
            if ($model->add($this->postData('data'))) {
                return $this->renderSuccess('添加成功', url('food.applet.help/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('food.applet.help/index'));
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = HelpModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('food.applet.help/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        // 帮助详情
        $model = HelpModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

}
