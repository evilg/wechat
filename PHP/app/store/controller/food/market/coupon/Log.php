<?php
namespace app\store\controller\food\market\coupon;

use app\store\controller\food\Controller;
use app\store\model\food\CouponLog as CouponLogModel;
use app\store\model\food\Coupon as CouponModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 发券记录控制器
 */
class Log extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new CouponLogModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isGet()) {
            $model = new CouponModel;
            $list = $model->getList();
            $model = new ShopModel;
            $shoplist = $model->getList(false);
            return $this->renderSuccess('', '', compact('list','shoplist'));
        }
        $model = new CouponLogModel;
        if ($model->add($this->postData('data'),$this->user['user']['user_id'])) {
            return $this->renderSuccess('添加成功', url('food.market.coupon.log/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = CouponLogModel::get($id);
        if ($model->remove()) {
           return $this->renderSuccess('删除成功'); 
        }
		$error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
        
    }
}
