<?php
namespace app\store\controller\food\market;

use app\store\controller\food\Controller;
use app\store\model\food\Setting as SettingModel;
use think\facade\View;

/**
 * 用户充值控制器
 */
class Recharge extends Controller
{
    public function setting()
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem('recharge');
            return View::fetch('setting', compact('model'));
        }
        $model = new SettingModel;
        if($model->edit('recharge', $this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
