<?php
namespace app\store\controller\food\data;

use app\store\controller\food\Controller;
use app\store\model\food\Coupon as CouponModel;
use think\facade\View;

/**
 * 优惠券控制器
 */
class Coupon extends Controller
{ 
	//列表
	public function lists(){
		$model = new CouponModel;
        $list = $model->getList();
		for($n=0;$n<sizeof($list);$n++){
			$list[$n]['image'] = base_url() . 'addons/food/img/coupon.jpg';
			$list[$n]['params'] = json_encode($list[$n]);
		}
		View::layout(false);
		return View::fetch('lists', compact('list'));
	}
	
}
