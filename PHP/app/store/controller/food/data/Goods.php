<?php
namespace app\store\controller\food\data;

use app\store\controller\food\Controller;
use app\store\model\food\Goods as GoodsModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 商品管理控制器
 */
class Goods extends Controller
{ 
	//商品列表
	public function lists($shop_id = 0){
		$model = new ShopModel;
		$shoplist = $model->getList(false);
		$model = new GoodsModel;
        $list = $model->getList($shop_id);
		for($n=0;$n<sizeof($list);$n++){
			$params = [
				"goods_id" => $list[$n]['goods_id'],
				"goods_name" => $list[$n]['goods_name'],
				"goods_image" => $list[$n]['image'][0]['url'],
				"selling_point" => $list[$n]['selling_point'],
				"is_recommend" => $list[$n]['is_recommend']['value'],
				"goods_price" => $list[$n]['spec'][0]['goods_price'],
				"line_price" => $list[$n]['spec'][0]['line_price'],
				"goods_sales" => $list[$n]['goods_sales']
			];
			$list[$n]['params'] = json_encode($params);
		}
		View::layout(false);
		return View::fetch('lists', compact('list','shoplist','shop_id'));
	}
	
}
