<?php
namespace app\store\controller\food\goods;

use app\store\controller\food\Controller;
use app\store\model\food\Category as CategoryModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 商品分类
 */
class Category extends Controller
{
	/**
	 * 门店选择
	 */
	public function opt()
	{
		if($this->shop_mode == 10){
			return redirect(url('food.goods.category/index',['shop_id' => $this->shop_id]));
		}
	    $model = new ShopModel;
        $shoplist = $model->getList(false);
	    return View::fetch('opt', compact('shoplist'));
	}
    /**
     * 分类列表
     */
    public function index($shop_id)
    {
		$shop = ShopModel::detail($shop_id);
		$shop_name = $shop['shop_name'];
        $model = new CategoryModel;
        $list = $model->getCacheTree($shop_id);
        return View::fetch('index', compact('list','shop_name','shop_id'));
    }

    /**
     * 删除分类
     */
    public function delete($id)
    {
        $model = CategoryModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add($shop_id)
    {
        if ($this->request->isPost()) {
            $model = new CategoryModel;
            if ($model->add($this->postData('data'),$shop_id)) {
                return $this->renderSuccess('添加成功', url('food.goods.category/index',['shop_id' => $shop_id]));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('food.goods.category/index',['shop_id' => $shop_id]));
    }

    /**
     * 编辑
     */
    public function edit($id,$shop_id)
    {
        $model = CategoryModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'),$shop_id)) {
            return $this->renderSuccess('操作成功', url('food.goods.category/index',['shop_id' => $shop_id]));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
