<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\food\Shop as ShopModel;
use app\store\model\food\User as UserModel;
use app\store\model\food\Record as RecordModel;
use think\facade\View;

/**
 * 用户管理
 */
class User extends Controller
{
	
	/**
     * 用户列表
     */
    public function index($gender = 'all', $search = '')
    {
        $model = new ShopModel;
        $shop = $model->getList(false);
        $model = new UserModel;
        $list = $model->getList(10,$gender, $search);
        return View::fetch('index', compact('list','shop','gender','search'));
    }
	
	/**
     * 用户充值
	 *$recharge 接收表单数据（数组）
	 *$source 0为充值余额，1为充值积分
    */
    public function recharge()
    {
        if ($this->request->isPost()) {
            $model = new RecordModel;
            $data = $this->postData('data');
            if($this->shop_mode == 10){
                $data['shop_id'] = $this->shop_id;
            }
            if ($model->add($data)) {
                return $this->renderSuccess('添加成功', url('food.user/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('food.user/index'));
    }
    
    /**
     * 切换员工身份
    */
    public function staff($id)
    {
        $user = UserModel::get($id);
        $user['is_staff']['value'] == 1 ? $user->is_staff = 0 : $user->is_staff = 1;
        $user->save();
        return $this->renderSuccess('操作成功', url('food.user/index'));
    }

}
