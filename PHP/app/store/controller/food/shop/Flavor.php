<?php
namespace app\store\controller\food\shop;

use app\store\controller\food\Controller;
use app\store\model\food\Flavor as FlavorModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 口味选项控制器
 */
class Flavor extends Controller
{
     /**
     * 列表
     */
    public function index($shop_id = 0)
    {
        if($this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $category = $model->getList(false);
        $model = new FlavorModel;
        $list = $model->getList($shop_id);
        return View::fetch('index', compact('list','category','shop_id'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new FlavorModel;
            $data = $this->postData('data');
            if($this->shop_mode == 10){
                $data['shop_id'] = $this->shop_id;
            }
            if ($model->add($data)) {
                return $this->renderSuccess('添加成功', url('food.shop.flavor/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('food.shop.flavor/index'));
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = FlavorModel::get($id);
        if($model->remove()) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = FlavorModel::get($id,['shop']);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('food.shop.flavor/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
}
