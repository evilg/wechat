<?php
namespace app\store\controller\food\shop;

use app\store\controller\food\Controller;
use app\store\model\food\Shop as ShopModel;
use app\store\model\food\ShopClerk as ShopClerkModel;
use think\facade\View;

/**
 * 店员管理控制器
 */
class Clerk extends Controller
{
    /**
     * 列表
     */
    public function index($shop_id = 0, string $search='')
    {
        if($this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
		$model = new ShopModel;
        $category = $model->getList(false);
        $model = new ShopClerkModel;
        $list = $model->getList($shop_id,$search);
        return View::fetch('index', compact('list','category','shop_id','search'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new ShopClerkModel;
            $data = $this->postData('data');
            if($this->shop_mode == 10){
                $data['shop_id'] = $this->shop_id;
            }
            if ($model->add($data)) {
                return $this->renderSuccess('添加成功', url('food.shop.clerk/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('food.shop.clerk/index'));
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = ShopClerkModel::get($id);
        if (!$model->remove($id)) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = ShopClerkModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('food.shop.clerk/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
