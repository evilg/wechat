<?php
namespace app\store\controller\food\shop;

use app\store\controller\food\Controller;
use app\store\model\food\Device as DeviceModel;
use app\store\model\food\Shop as ShopModel;
use app\store\model\food\Category;
use think\facade\View;

/**
 * 云设备控制器
 */
class Device extends Controller
{
     /**
     * 列表
     */
    public function index($shop_id = 0)
    {
        if($this->shop_mode == 10){
            $shop_id = $this->shop_id;
        }
        $shop = new ShopModel;
        $shoplist = $shop->getList(false);
        $model = new DeviceModel;
        $list = $model->getList($shop_id);
        return View::fetch('index', compact('list','shoplist','shop_id'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $model = new ShopModel;
            $shoplist = $model->getList(false);
            $category = $this->getCategory($shoplist);
            return View::fetch('add', compact('shoplist','category'));
        }
        $data = $this->postData('data');
        if($this->shop_mode == 10){
            $data['shop_id'] = $this->shop_id;
        }
        $model = new DeviceModel;
        if ($model->add($data)) {
            return $this->renderSuccess('添加成功', url('food.shop.device/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        // 详情
        $model = DeviceModel::get($id);
        if (!$this->request->isAjax()) {
            $category = $this->getChecked($model);
            return View::fetch('edit', compact('model', 'category'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('food.shop.device/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = DeviceModel::get($id);
        if($model->remove()) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
    }

    /**
     * 开启/关闭
     */
    public function status($id)
    {
        $model = DeviceModel::get($id);
        // 更新记录
        if ($model->statu()) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
    
    /**
     * 编辑打印类目
     **/
    private function getChecked($data)
    {
        $category = (new Category)->where('shop_id',$data['shop_id'])->select();
        for($n=0;$n<sizeof($category);$n++){
            $category[$n]['checked'] = false;
            if(isset($data['values']['category'])){
                foreach ($data['values']['category'] as $item){
                    if($category[$n]['category_id'] == $item){
                       $category[$n]['checked'] = true; 
                    }
                }
            }
        }
        return $category;
    }
    
    /**
     * 获取所有门店和关联的商品分类
     **/
    private function getCategory($shoplist)
    {
        $category = [];
        for($n=0;$n<sizeof($shoplist);$n++){
            $category[$shoplist[$n]['shop_id']] = (new Category)->where('shop_id',$shoplist[$n]['shop_id'])->select();
        }
        return json_encode($category);
    }

}
