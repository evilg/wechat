<?php
namespace app\store\controller\food\order;

use app\store\controller\food\Controller;
use app\store\model\food\Pact as PactModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 预约控制器
 */
class Pact extends Controller
{
	
    /**
     * 排号等座
     */
    public function sorts($shop_id = 0)
    {
        if($this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $category = $model->getList(false);
        $model = new PactModel;
        $list = $model->getList(10,$shop_id);
        return View::fetch('sorts', compact('list','shop_id','category'));
    }
	
	/**
     * 预约订座
     */
    public function table($shop_id = 0)
    {
        if($this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $category = $model->getList(false);
        $model = new PactModel;
        $list = $model->getList(20,$shop_id);
        return View::fetch('table', compact('list','shop_id','category'));
    }
	
	/**
     * 状态编辑
     */
    public function status($id)
    {
        $model = PactModel::get($id);
		$model->status['value']==10 ? $model->status = 20 : $model->status = 10;
		$model->save();
        return $this->renderSuccess('更新成功');
    }
	
}
