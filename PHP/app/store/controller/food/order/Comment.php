<?php
namespace app\store\controller\food\order;

use app\store\controller\food\Controller;
use app\store\model\food\Comment as CommentModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 评论制器
 */
class Comment extends Controller
{
	
    /**
     * 列表
     */
    public function index($shop_id = 0)
    {
        if($this->shop_mode == 10){
           $shop_id = $this->shop_id;
        }
        $model = new ShopModel;
        $category = $model->getList(false);
        $model = new CommentModel;
        $list = $model->getList($shop_id);
        return View::fetch('index', compact('list','category','shop_id'));
    }
	
	/**
     * 编辑
     */
    public function edit($id)
    {
        $model = CommentModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('food.order.comment/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
	
	/**
     * 显示状态编辑
     */
    public function status($id)
    {
        $model = CommentModel::get($id);
		if ($model->status()) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
}
