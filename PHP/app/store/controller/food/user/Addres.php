<?php
namespace app\store\controller\food\user;

use app\store\controller\food\Controller;
use app\store\model\Address as AddressModel;
use think\facade\View;

/**
 * 用户收货地址控制器
 */
class Addres extends Controller
{
    /**
     * 用户充值列表
     */
    public function index(string $search='')
    {
        $model = new AddressModel;
        $list = $model->getList($search);
        return View::fetch('index', compact('list','search'));
    }
}
