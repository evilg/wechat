<?php
namespace app\store\controller\food\user;

use app\store\controller\food\Controller;
use app\store\model\food\Record as RecordModel;
use app\store\model\food\Shop as ShopModel;
use think\facade\View;

/**
 * 用户支付记录
 */
class Pay extends Controller
{
    /**
     * 用户充值列表
     */
    public function index($shop_id = 0)
    {
        $model = new ShopModel;
        $category = $model->getList(false);
        $model = new RecordModel;
        $list = $model->getList(10,0,$shop_id);
        return View::fetch('index', compact('list','category','shop_id'));
    }
}