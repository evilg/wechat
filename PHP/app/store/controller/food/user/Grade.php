<?php
namespace app\store\controller\food\user;

use app\store\controller\food\Controller;
use app\store\model\food\Setting as SettingModel;
use think\facade\View;

/**
 * 用户等级
 */
class Grade extends Controller
{
    /**
     * 用户等级列表
     */
    public function index()
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem('grade');
            return View::fetch('index', compact('model'));
        }
        $model = new SettingModel;
        if($model->edit('grade', $this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
