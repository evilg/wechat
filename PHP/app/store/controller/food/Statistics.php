<?php
namespace app\store\controller\food;

use app\store\controller\food\Controller;
use app\store\model\food\Order as OrderModel;
use app\store\model\food\Record as RecordModel;
use app\store\model\food\Shop as ShopModel;
use app\store\model\food\Pact as PactModel;
use think\facade\View;

/**
 * 统计数据
 */
class Statistics extends Controller
{
	
    public function opt()
    {
    	$model = new ShopModel;
	    $shoplist = $model->getList(false);
		return View::fetch('opt', compact('shoplist'));
    }
	
	public function index($shop_id = 0,$time_star,$time_end)
    {
        if($this->shop_mode == 10){
    		$shop_id = $this->shop_id;
    	}
		$shop['shop_id'] = $shop_id;
		$shop['star'] = strtotime($time_star);
		$shop['end'] = strtotime($time_end)+3600*24;
		$count = array();
		$count['order'] = OrderModel::getDateCount($shop);	//订单和收入统计
		$count['record'] = RecordModel::getDateCount($shop);	//充值统计
		$count['pact'] = PactModel::getDateCount($shop);	//预约统计
		return View::fetch('index', compact('count'));
    }
}
