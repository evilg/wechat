<?php
namespace app\store\controller;

use think\facade\View;
use think\facade\Session;
use think\captcha\facade\Captcha;
use app\store\model\Applet as AppletModel;


/**
 * 登录小程序管理中心
 */
class Passport extends Controller
{
    /**
     * 生成验证码
     **/
    public function captcha()
    {
        return Captcha::create();
    }

    /**
     * 用户登录
     */
    public function login()
    {
        if (!$this->request->isAjax()) {
            return View::fetch();
        }
        $model = new AppletModel;
        if ($url = $model->login($this->postData('data'))) {
            return $this->renderSuccess('登录成功', $url);
        }
        $error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

    /**
     * 小程序管理端 - 退出登录
     */
    public function applet($app_type)
    {
        // 清空登录状态
        Session::delete('hema_store_' . $app_type);
        return redirect(url('passport/login'));
    }
    
    /**
     * 其他管理端 - 退出登录
     */
    public function logout($app_type)
    {
        // 清空登录状态
        Session::delete('hema_store_' . $app_type);
        return redirect(url($app_type . '.passport/login'));
    }

}