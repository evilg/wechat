<?php
namespace app\common\model;

use hema\wechat\Driver;

/**
 * 微信群发消息模型
 */
class WechatBatchSend extends BaseModel
{
    protected $name = 'wechat_batch_send';

    // 定义主键
    protected $pk = 'wechat_batch_send_id';

    // 追加字段
    protected $append = [];
	
	/**
     * 消息类型
     */
    public function getMsgTypeAttr($value)
    {
        $status = [
			'mpnews' => '图文', 
            'news' => '图文',
			'text' => '文本', 
			'image' => '图片', 
			'video' => '视频', 
			'voice' => '语音', 
			'music' => '音乐', 
			'wxcard' => '卡券'
		];
        return ['text' => $status[$value], 'value' => $value];
    }
	
	/**
     * 群发状态
     */
    public function getStatusAttr($value)
    {
        $status = [
			0 => '未知错误',
			10 => '待群发', 
			20 => '群发中', 
			30 => '群发成功', 
			40 => '群发失败', 
			10001 => '涉嫌广告', 
			20001 => '涉嫌政治', 
			20004 => '涉嫌社会',
			20002 => '涉嫌色情',
			20006 => '涉嫌违法犯罪',
			20008 => '涉嫌欺诈',
			20013 => '涉嫌版权',
			22000 => '涉嫌互推',
			21000 => '涉嫌其他',
			30001 => '原创不能群发',
			30002 => '原创不能群发',
			30003 => '原创不能群发',
			40001 => '管理员拒绝',
			40002 => '管理员超时'
		];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        $applet_id = self::$applet_id;
        // 筛选条件
        $filter = [];
        empty($applet_id) && $filter['applet_id'] = 0;
        // 执行查询
        return $this->where($filter)->order('wechat_batch_send_id','desc')->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 获取详情
     */
    public static function getSend(array $filter)
    {
        return self::withoutGlobalScope()->where($filter)->find();
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $applet_id = self::$applet_id;
        empty($applet_id) && $applet_id = 0;
        $values = Setting::getItem('web',$applet_id);
        if(!empty($values['open_id'])){
            $wx = new Driver;
            $result= $wx->previewMsg($data,$values['open_id'],$applet_id);
            if($result['errcode'] != 0){
                $this->error = 'code：' . $result['errcode'] . '，msg：' . $result['errmsg'];
                return false;
            }
        }
        $data['applet_id'] = $applet_id;
        return $this->save($data);
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        $applet_id = self::$applet_id;
        empty($applet_id) && $applet_id = 0;
        $values = Setting::getItem('web',$applet_id);
        if(!empty($values['open_id'])){
            $wx = new Driver;
            $result= $wx->previewMsg($data,$values['open_id'],$applet_id);
            if($result['errcode'] != 0){
                $this->error = 'code：' . $result['errcode'] . '，msg：' . $result['errmsg'];
                return false;
            }
        }
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        if(!empty($this->msg_id)){
            $applet_id = self::$applet_id;
            empty($applet_id) && $applet_id = 0;
            $wx = new Driver;
            $result = $wx->deleteMass($this->msg_id,$applet_id);
            if($result['errcode'] != 0){
                $this->error = 'code：' . $result['errcode'] . '，msg：' . $result['errmsg'];
                return false;
            }
        }
        return $this->delete();
    }
    
    /**
     * 根据OpenID列表群发
     */
    public function send()
    {
        if(!empty($this->msg_id)){
            $this->error = '群发任务已存在';
            return false;
        }
        $applet_id = self::$applet_id;
        empty($applet_id) && $applet_id = 0;
        $model = User::withoutGlobalScope()->where('open_id','<>','');
        if($applet_id > 0){
            $filter['status'] = 10; // 商家会员
        }else{
            $model->where('status','>',10);//站点用户
        }
        $filter['is_subscribe'] = 1;
        $filter['applet_id'] = $applet_id;
        $open_id = $model->where($filter)->column('open_id');

        $wx = new Driver;
        $result = $wx->sendMass($this,$open_id,$applet_id);
        if($result['errcode'] != 0){
            $this->error = 'code：' . $result['errcode'] . '，msg：' . $result['errmsg'];
            return false;
        }
        $this->msg_id = $result['msg_id'];
        $this->fans_count = sizeof($open_id);
        $this->status = 20;
        return $this->save() !== false;
    }
}
    
