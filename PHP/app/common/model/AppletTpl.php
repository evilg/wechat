<?php
namespace app\common\model;

use app\common\model\Template;

/**
 * 小程序模板提审记录模型
 */
class AppletTpl extends BaseModel
{
    // 定义表名
    protected $name = 'applet_tpl';

    // 定义主键
    protected $pk = 'applet_tpl_id';

    // 追加字段
    protected $append = ['code'];

    /**
     * 审核状态
     */
    public function getStatusAttr($value)
    {
        $status = ['审核中', '被拒绝', '已上线', '待发布', '审核延期', '被撤回'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 关联模板代码表
    */
    public function getCodeAttr($value,$data)
    {
        return TemplateCode::withoutGlobalScope()->where('template_code_id',$data['template_code_id'])->find();
    }
    
    /**
     * 获取最新发布记录
     */
    public static function getNew($applet_id = null)
    {
        $self = new static();
        is_null($applet_id) && $applet_id = $self::$applet_id;
        empty($applet_id ) && $applet_id = 0;
        $filter['applet_id'] = $applet_id;
        return $self->withoutGlobalScope()->where($filter)->order('applet_tpl_id','desc')->find();
    }

	/**
     * 获取列表
     */
    public function getList()
    {
        // 执行查询
        return $this->order('applet_tpl_id','desc')->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        // 保存
        return $this->save($data) !== false;
    }

    /**
     * 编辑
     */
    public function remove()
    {
        return $this->delete();
    }

	
	











    /**
     * 删除所有记录
     */
    public static function delALL($applet_id = null)
    {
        $self = new static();
        $where = [];
        is_null($applet_id) && $applet_id = $self::$applet_id;
        $applet_id > 0 && $where['applet_id'] = $applet_id;
        return $self->withoutGlobalScope()->where($where)->delete();
    }


   

}
