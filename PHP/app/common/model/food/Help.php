<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 帮助中心
 */
class Help extends BaseModel
{
    // 定义表名
    protected $name = 'food_help';

    // 定义主键
    protected $pk = 'help_id';

    // 追加字段
    protected $append = [];

    /**
     * 获取帮助列表
     */
    public function getList()
    {
        return $this->order(['sort','help_id' => 'desc'])->select();
    }

    /**
     * 新增记录
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }

    /**
     * 更新记录
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除记录
     */
    public function remove() {
        return $this->delete();
    }
}
