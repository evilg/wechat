<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 公众号模型
 */
class OrderGoods extends BaseModel
{
    // 定义表名
    protected $name = 'food_order_goods';
    protected $updateTime = false;

    // 定义主键
    protected $pk = 'order_goods_id';

    // 追加字段
    protected $append = [];

    /**
     * 订单商品列表
     */
    public function image()
    {
        return $this->belongsTo('addons\\upload\\model\\UploadFile', 'image_id', 'file_id');
    }

    /**
     * 关联商品表
     */
    public function goods()
    {
        return $this->belongsTo('app\\common\\model\\food\\Goods','goods_id');
    }

    /**
     * 关联商品规格表
     */
    public function spec()
    {
        return $this->belongsTo('app\\common\\model\\food\\GoodsSpec', 'spec_sku_id', 'spec_sku_id');
    }

}
