<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 小程序diy页面模型
 */
class Page extends BaseModel
{
    //定义表名
    protected $name = 'page';

    // 定义主键
    protected $pk = 'page_id';

    // 追加字段
    protected $append = [];
	
	/**
     * 页面类型
     */
    public function getPageTypeAttr($value)
    {
        $status = [10 => '默认首页', 20 => '自定义页'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 格式化页面数据（读取的时候对数据转换）
     */
    public function getPageDataAttr($json)
    {
        $array = json_decode($json, true);
        return compact('array', 'json');
    }

    /**
     * 自动转换data为json格式(修改器，保存de时候操作)
     */
    public function setPageDataAttr($value)
    {
        return json_encode($value ?: ['items' => []]);
    }

    /**
     * 获取页面列表
     */
    public function getList()
    {
        // 排序规则
        $sort = [];
        $sort = ['page_id' => 'desc'];
        // 执行查询
        return $this->order($sort)->paginate(['list_rows'=>15,'query' => request()->param()]);
    }
	/**
     * diy页面详情
     */
    public static function getHome()
    {
        return self::where(['page_type' =>10])->find();
    }

    /**
     * 添加
     */
    public function add(array $page_data)
    {
        for($n=0;$n<sizeof($page_data['items']);$n++){
            if($page_data['items'][$n]['type'] == 'richText'){
                $page_data['items'][$n]['params']['content'] = htmlspecialchars_decode($page_data['items'][$n]['params']['content']);
            }
        }
		$data['page_type'] = 20;
		$data['page_data'] = $page_data;
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }

    /**
     * 更新页面数据
     */
    public function edit(array $page_data)
    {
        for($n=0;$n<sizeof($page_data['items']);$n++){
            if($page_data['items'][$n]['type'] == 'richText'){
                $page_data['items'][$n]['params']['content'] = htmlspecialchars_decode($page_data['items'][$n]['params']['content']);
            }
        }
        return $this->save(compact('page_data')) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        //判断是否批量删除门店
        if($this->count() == 1){
            $this->error = '至少保留一个页面';
            return false;
        }
        return $this->delete();
    }

    /**
     * 设置默认首页
     */
	public function status()
    {
		//取消原来的默认首页
		$this->where('page_type',10)->update(['page_type' => 20]);
		//设置新首页
		return $this->save(['page_type' => 10]) !== false;
    }

    /**
     * 新增小程序首页diy默认设置
     */
    public function insertDefault($applet_id,$data)
    {
        $page = [
            'page_type' => 10,
            'page_data' => [
				'page' => [
					'type' => 'page',
					'name' => '页面设置',
					'params' => [
						'name' => '默认首页',
						'title' => $data['shop_name'],
						'share_title' => $data['shop_name'],
						'share_image' => base_url().'addons/food/img/share_image.jpg'
					],
					'style' => [
						'titleTextColor' => 'black',
						'titleBackgroundColor' => '#ffffff'
					],
					'id' => 'page'
				],
				'tabbar' => [
					'type' => 'tabbar',
					'name' => '导航设置',
					'list' => [
						[
							'text' => '首页', //标题
							'iconPath' =>  base_url().'addons/food/img/diy/tabbar/home.png', //选中状态图标
							'selectedIconPath' => base_url().'addons/food/img/diy/tabbar/home_on.png', //激活(选中)的图标
							'pagePath' =>'/pages/index/index', //路径需要以"/"开头
						],
						[
							'text' => '订单', //标题
							'iconPath' =>  base_url().'addons/food/img/diy/tabbar/order.png', //选中状态图标
							'selectedIconPath' => base_url().'addons/food/img/diy/tabbar/order_on.png', //激活(选中)的图标
							'pagePath' =>'/pages/order/index', //路径需要以"/"开头
							'midButton' => 'false', //图标凸起
							/*
							图标建议尺寸为80px * 80px
							凸起图标建议为120px * 120px的png图片
							 */
						],
						[
							'text' => '我的', //标题
							'iconPath' =>  base_url().'addons/food/img/diy/tabbar/user.png', //选中状态图标
							'selectedIconPath' => base_url().'addons/food/img/diy/tabbar/user_on.png', //激活(选中)的图标
							'pagePath' =>'/pages/user/index', //路径需要以"/"开头
						]
					],
					'style' => [
						'borderTop' => 'true',	//是否显示顶部的边框
						'midButton' => 'false', //中间凸起
						'activeColor' => '#000000', //激活时的颜色
						'inactiveColor' => '#6e6d6b',	//未激活时的颜色
						'bgColor' => '#ffffff',	//背景颜色
					],
					'id' => 'tabbar'
				],
				'items' => [
					[
						'name' => '图片轮播',
						'type' => 'banner',
						'style' => [
							'borderRadius' => 0, //轮播图圆角值，单位rpx
							'effect3d' => 'false', //是否开启3D效果
							'title' => 'false', //是否显示标题文字
							'mode' => 'round',	//指示器类型 rect-指示器为方块状 dot-指示器为圆点 number-指示器为数字 round-激活的指示器为块状，未激活的未点状 none-不显示指示器
							'indicatorPos' => 'bottomCenter', //指示器的位置 topLeft-指示器位于左上角 topCenter-指示器位于上方中间位置 topRight-指示器位于右上角 bottomLeft-指示器位于左下角 bottomCenter-指示器位于底部中间位置 bottomRight-指示器位于右下角
							'height' => 300, //轮播图组件高度，单位rpx
							'bgColor' => '#ffffff'
						],
						'params' => [
							"autoplay" => 'true', //是否自动轮播
							"interval" => 2500, //自动轮播时间间隔，单位ms
							"duration" => 300,	//	切换一张轮播图所需的时间，单位ms
							"circular" => 'true',	//	是否衔接播放
						],
						'data' => [
							[
								"imgUrl" => base_url() . 'uploads/test/b37f5604c3026646dc8886fbefa9ff9f.png',
								"title" => '',
								"url" => ''
							],
							[
								"imgUrl" => base_url() . 'uploads/test/9a401b2be734e4ee0052291f5c435286.png',
								"title" => '',
								"url" => ''
							],
							[
								"imgUrl" => base_url() . 'uploads/test/15fa3524258124576c8cbc0765c3bc0b.png',
								"title" => '',
								"url" => ''
							]
						]
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					[
						'name' => '公告组',
						'type' => 'notice',
						'params' => [
							'mode' => 'vertical', //horizontal(水平滚动)	vertical(垂直滚动)
							'isCircular' => 'true'	//是否水平滚动衔接
						],
						'style' => [
							'paddingTop' => '0',
							'borderRadius' => '0',
							'volumeIcon' => 'true',	//是否显示小喇叭图标
							'moreIcon' => 'true',	//是否显示右边的向右箭头
							'type' => 'error' //显示的主题 primary / warning / error / success / none
						],
						'data' => [
							[
								'title' => '这里是第一条自定义公告的标题1',
								'url' => ''
							],
							[
								'title' => '这里是第一条自定义公告的标题2',
								'url' => ''
							]
						]
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					[
						'name' => '单图组',
						'type' => 'imageSingle',
						'style' => [
							'borderRadius' => '10',
							'paddingTop' => '0',
							'paddingLeft' => '0',
							'background' => '#ffffff'
						],
						'data' => [
							[
								'imgUrl' => base_url().'addons/food/img/food_but.jpg',
								'imgName' => 'image-1.jpg',
								'linkUrl' => 'pages/shop/index'
							]
						]
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					//导航组
					[
						'name' => '导航组',
						'type' => 'navBar',
						'style' => [
							'bgColor' => '#ffffff',
							'rowsNum' => '3'
						],
						'data' => [
							[
								'imgUrl' => base_url().'uploads/test/6f2fb7d9b62026dfb359a736c60eace2.png',
								'linkUrl' => 'pages/user/coupon/index',
								'text' => '优惠活动',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'uploads/test/c2f99a94c7edcf903e5335780c906000.png',
								'linkUrl' => 'pages/user/wallet/index',
								'text' => '充值有礼',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'uploads/test/55324a2141ee5aec0f8d0a723ff5f37a.png',
								'linkUrl' => 'pages/pact/sort',
								'text' => '预约排队',
								'color' => '#666666'
							],
						]
					],
					//栏目标题
					[
						'name' => '栏目标题',
						'type' => 'columnTitle',
						'params' => [
							'title' => '新品推荐',//左边主标题
							'subTitle' => '更多',//右边副标题
							'right' => 'true',//是否显示右边的内容
							'showLine' => 'true',//是否显示左边的竖条
							'arrow' => 'true',//是否显示右边箭头
							'url' => 'pages/shop/index',
						],
						'style' => [
							'fontSize' => '28',//主标题的字体大小
							'bold' => 'true',//主标题是否加粗
							'color' => '#303133',//主标题颜色
							'subColor' => '#909399',//右边副标题的颜色
							'lineColor' => '#ff9900',//左边竖线的颜色
							'background' => '#f7f7f7',
							'paddingTop' => '20'
						]
					],
					//商品组
					[
						'name' => '商品组',
						'type' => 'goods',
						'params' => [
							'source' => 'auto', //商品来源，auto=自动选择，choice=手动选择
							'auto' => [	//自动选择配置
								'category' => 0, //分类编号
								'goodsSort' => 'all',	//商品排序，all=综合，sales=销量，new=新品,recommend推荐
								'showNum' => 10	//显示数量
							]
						],
						'style' => [
							'background' => '#FFFFFF', //背景颜色
							'display' => 'slide', 	//显示类型，list=列表平铺，slide=横向滑动
							'column' => '3',	//分列数量，1=单列，2=双列，3=三列
							'show' => [	//显示内容
								'goodsName' => 'true',	//商品名称
								'goodsPrice' => 'true',	//商品价格
								'linePrice' => 'true',	//划线价格
								'sellingPoint' => 'true',//商品卖点
								'goodsSales' => 'true'	//商品销量
							]
						],
						'defaultData' => [	//默认数据
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							],
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							],
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							],
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							]
						],
						'data' => []
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					//图片橱窗
					[
						'name' => '图片橱窗',
						'type' => 'window',
						'style' => [
							'borderRadius' => '10',
							'paddingTop' => '10',
							'paddingLeft' => '10',
							'background' => '#ffffff',
							'layout' => '2'
						],
						'data' => [
							[
								'imgUrl' => base_url().'uploads/test/1d88493410ed2f3ad0d8c3ca7752adc8.jpg',
								'linkUrl' => ''
							],
							[
								'imgUrl' => base_url().'uploads/test/68fb4fb5f4d91e581623b817990af7fb.jpg',
								'linkUrl' => ''
							],
						],
						'dataNum' => 4
					],
					//栏目标题
					[
						'name' => '栏目标题',
						'type' => 'columnTitle',
						'params' => [
							'title' => '新品推荐',//左边主标题
							'subTitle' => '更多',//右边副标题
							'right' => 'false',//是否显示右边的内容
							'showLine' => 'true',//是否显示左边的竖条
							'arrow' => 'false',//是否显示右边箭头
							'url' => '',
						],
						'style' => [
							'fontSize' => '28',//主标题的字体大小
							'bold' => 'true',//主标题是否加粗
							'color' => '#303133',//主标题颜色
							'subColor' => '#909399',//右边副标题的颜色
							'lineColor' => '#ff9900',//左边竖线的颜色
							'background' => '#f7f7f7',
							'paddingTop' => '20'
						]
					],
					//导航组
					[
						'name' => '导航组',
						'type' => 'navBar',
						'style' => [
							'bgColor' => '#ffffff',
							'rowsNum' => '4'
						],
						'data' => [
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字1',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字2',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字3',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字4',
								'color' => '#666666'
							],
						]
					],
				]				
            ],
            'applet_id' => $applet_id
        ];
        return $this->save($page);
    }

	
	/**
     * 新增默认页面模板
     */
    public static function page()
    {
		$page['array'] = [
			'page' => [
				'type' => 'page',
				'name' => '页面设置',
				'params' => [
					'name' => '页面名称',
					'title' => '页面标题',
					'share_title' => '分享标题',
					'share_image' => base_url().'addons/food/img/share_image.jpg'
				],
				'style' => [
					'titleTextColor' => 'black',
					'titleBackgroundColor' => '#ffffff'
				],
				'id' => 'page'
			],
			'tabbar' => [
				'type' => 'tabbar',
				'name' => '导航设置',
				'list' => [
					[
						'text' => '首页', //标题
						'iconPath' =>  base_url().'addons/food/img/diy/tabbar/home.png', //选中状态图标
						'selectedIconPath' => base_url().'addons/food/img/diy/tabbar/home_on.png', //激活(选中)的图标
						'pagePath' =>'/pages/index/index', //路径需要以"/"开头
					],
					[
						'text' => '订单', //标题
						'iconPath' =>  base_url().'addons/food/img/diy/tabbar/order.png', //选中状态图标
						'selectedIconPath' => base_url().'addons/food/img/diy/tabbar/order_on.png', //激活(选中)的图标
						'pagePath' =>'/pages/order/index', //路径需要以"/"开头
						'midButton' => 'false', //图标凸起
						/*
						图标建议尺寸为80px * 80px
						凸起图标建议为120px * 120px的png图片
						 */
					],
					[
						'text' => '我的', //标题
						'iconPath' =>  base_url().'addons/food/img/diy/tabbar/user.png', //选中状态图标
						'selectedIconPath' => base_url().'addons/food/img/diy/tabbar/user_on.png', //激活(选中)的图标
						'pagePath' =>'/pages/user/index', //路径需要以"/"开头
					]
				],
				'style' => [
					'borderTop' => 'true',	//是否显示顶部的边框
					'midButton' => 'false', //中间凸起
					'activeColor' => '#000000', //激活时的颜色
					'inactiveColor' => '#6e6d6b',	//未激活时的颜色
					'bgColor' => '#ffffff',	//背景颜色
				],
				'id' => 'tabbar'
			],
			'items' => [
					[
						'name' => '图片轮播',
						'type' => 'banner',
						'style' => [
							'borderRadius' => 0, //轮播图圆角值，单位rpx
							'effect3d' => 'false', //是否开启3D效果
							'title' => 'false', //是否显示标题文字
							'mode' => 'round',	//指示器类型 rect-指示器为方块状 dot-指示器为圆点 number-指示器为数字 round-激活的指示器为块状，未激活的未点状 none-不显示指示器
							'indicatorPos' => 'bottomCenter', //指示器的位置 topLeft-指示器位于左上角 topCenter-指示器位于上方中间位置 topRight-指示器位于右上角 bottomLeft-指示器位于左下角 bottomCenter-指示器位于底部中间位置 bottomRight-指示器位于右下角
							'height' => 300, //轮播图组件高度，单位rpx
							'bgColor' => '#ffffff'
						],
						'params' => [
							"autoplay" => 'true', //是否自动轮播
							"interval" => 2500, //自动轮播时间间隔，单位ms
							"duration" => 300,	//	切换一张轮播图所需的时间，单位ms
							"circular" => 'true',	//	是否衔接播放
						],
						'data' => [
							[
								"imgUrl" => base_url() . 'uploads/test/b37f5604c3026646dc8886fbefa9ff9f.png',
								"title" => '',
								"url" => ''
							],
							[
								"imgUrl" => base_url() . 'uploads/test/9a401b2be734e4ee0052291f5c435286.png',
								"title" => '',
								"url" => ''
							],
							[
								"imgUrl" => base_url() . 'uploads/test/15fa3524258124576c8cbc0765c3bc0b.png',
								"title" => '',
								"url" => ''
							]
						]
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					[
						'name' => '公告组',
						'type' => 'notice',
						'params' => [
							'mode' => 'vertical', //horizontal(水平滚动)	vertical(垂直滚动)
							'isCircular' => 'true'	//是否水平滚动衔接
						],
						'style' => [
							'paddingTop' => '0',
							'borderRadius' => '0',
							'volumeIcon' => 'true',	//是否显示小喇叭图标
							'moreIcon' => 'true',	//是否显示右边的向右箭头
							'type' => 'error' //显示的主题 primary / warning / error / success / none
						],
						'data' => [
							[
								'title' => '这里是第一条自定义公告的标题1',
								'url' => ''
							],
							[
								'title' => '这里是第一条自定义公告的标题2',
								'url' => ''
							]
						]
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					[
						'name' => '单图组',
						'type' => 'imageSingle',
						'style' => [
							'borderRadius' => '10',
							'paddingTop' => '0',
							'paddingLeft' => '0',
							'background' => '#ffffff'
						],
						'data' => [
							[
								'imgUrl' => base_url().'addons/food/img/food_but.jpg',
								'imgName' => 'image-1.jpg',
								'linkUrl' => 'pages/shop/index'
							]
						]
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					//导航组
					[
						'name' => '导航组',
						'type' => 'navBar',
						'style' => [
							'bgColor' => '#ffffff',
							'rowsNum' => '3'
						],
						'data' => [
							[
								'imgUrl' => base_url().'uploads/test/6f2fb7d9b62026dfb359a736c60eace2.png',
								'linkUrl' => 'pages/user/coupon/index',
								'text' => '优惠活动',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'uploads/test/c2f99a94c7edcf903e5335780c906000.png',
								'linkUrl' => 'pages/user/wallet/index',
								'text' => '充值有礼',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'uploads/test/55324a2141ee5aec0f8d0a723ff5f37a.png',
								'linkUrl' => 'pages/pact/sort',
								'text' => '预约排队',
								'color' => '#666666'
							],
						]
					],
					//栏目标题
					[
						'name' => '栏目标题',
						'type' => 'columnTitle',
						'params' => [
							'title' => '新品推荐',//左边主标题
							'subTitle' => '更多',//右边副标题
							'right' => 'true',//是否显示右边的内容
							'showLine' => 'true',//是否显示左边的竖条
							'arrow' => 'true',//是否显示右边箭头
							'url' => 'pages/shop/index',
						],
						'style' => [
							'fontSize' => '28',//主标题的字体大小
							'bold' => 'true',//主标题是否加粗
							'color' => '#303133',//主标题颜色
							'subColor' => '#909399',//右边副标题的颜色
							'lineColor' => '#ff9900',//左边竖线的颜色
							'background' => '#f7f7f7',
							'paddingTop' => '20'
						]
					],
					//商品组
					[
						'name' => '商品组',
						'type' => 'goods',
						'params' => [
							'source' => 'auto', //商品来源，auto=自动选择，choice=手动选择
							'auto' => [	//自动选择配置
								'category' => 0, //分类编号
								'goodsSort' => 'all',	//商品排序，all=综合，sales=销量，new=新品,recommend推荐
								'showNum' => 10	//显示数量
							]
						],
						'style' => [
							'background' => '#FFFFFF', //背景颜色
							'display' => 'slide', 	//显示类型，list=列表平铺，slide=横向滑动
							'column' => '3',	//分列数量，1=单列，2=双列，3=三列
							'show' => [	//显示内容
								'goodsName' => 'true',	//商品名称
								'goodsPrice' => 'true',	//商品价格
								'linePrice' => 'true',	//划线价格
								'sellingPoint' => 'true',//商品卖点
								'goodsSales' => 'true'	//商品销量
							]
						],
						'defaultData' => [	//默认数据
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							],
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							],
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							],
							[
								'goods_name' => '此处显示商品名称',
								'image' => base_url().'addons/food/img/diy/goods_01.png',
								'goods_price' => '99.00',
								'line_price' => '139.00',
								'selling_point' => '此款商品美观大方 不容错过',
								'goods_sales' => '100'
							]
						],
						'data' => []
					],
					[
						'name' => '辅助空白',
						'type' => 'blank',
						'style' => [
							'height' => '20',
							'background' => '#f7f7f7'
						]
					],
					//图片橱窗
					[
						'name' => '图片橱窗',
						'type' => 'window',
						'style' => [
							'borderRadius' => '10',
							'paddingTop' => '10',
							'paddingLeft' => '10',
							'background' => '#ffffff',
							'layout' => '2'
						],
						'data' => [
							[
								'imgUrl' => base_url().'uploads/test/1d88493410ed2f3ad0d8c3ca7752adc8.jpg',
								'linkUrl' => ''
							],
							[
								'imgUrl' => base_url().'uploads/test/68fb4fb5f4d91e581623b817990af7fb.jpg',
								'linkUrl' => ''
							],
						],
						'dataNum' => 4
					],
					//栏目标题
					[
						'name' => '栏目标题',
						'type' => 'columnTitle',
						'params' => [
							'title' => '新品推荐',//左边主标题
							'subTitle' => '更多',//右边副标题
							'right' => 'false',//是否显示右边的内容
							'showLine' => 'true',//是否显示左边的竖条
							'arrow' => 'false',//是否显示右边箭头
							'url' => '',
						],
						'style' => [
							'fontSize' => '28',//主标题的字体大小
							'bold' => 'true',//主标题是否加粗
							'color' => '#303133',//主标题颜色
							'subColor' => '#909399',//右边副标题的颜色
							'lineColor' => '#ff9900',//左边竖线的颜色
							'background' => '#f7f7f7',
							'paddingTop' => '20'
						]
					],
					//导航组
					[
						'name' => '导航组',
						'type' => 'navBar',
						'style' => [
							'bgColor' => '#ffffff',
							'rowsNum' => '4'
						],
						'data' => [
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字1',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字2',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字3',
								'color' => '#666666'
							],
							[
								'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
								'linkUrl' => '',
								'text' => '按钮文字4',
								'color' => '#666666'
							],
						]
					],
				]
		];
		$page['json'] = json_encode($page['array']);
		return $page;
	}
	
	/**
     * 首页diy模板
     */
    public static function temp()
    {
		$temp['array'] = [
			//图片轮播
			'banner' => [
				'name' => '图片轮播',
				'type' => 'banner',
				'style' => [
					'borderRadius' => 20, //轮播图圆角值，单位rpx
					'effect3d' => 'true', //是否开启3D效果
					'title' => 'false', //是否显示标题文字
					'mode' => 'round',	//指示器类型 rect-指示器为方块状 dot-指示器为圆点 number-指示器为数字 round-激活的指示器为块状，未激活的未点状 none-不显示指示器
					'indicatorPos' => 'bottomCenter', //指示器的位置 topLeft-指示器位于左上角 topCenter-指示器位于上方中间位置 topRight-指示器位于右上角 bottomLeft-指示器位于左下角 bottomCenter-指示器位于底部中间位置 bottomRight-指示器位于右下角
					'height' => 250, //轮播图组件高度，单位rpx
					'bgColor' => '#ffffff'
				],
				'params' => [
					"autoplay" => 'true', //是否自动轮播
					"interval" => 2500, //自动轮播时间间隔，单位ms
					"duration" => 500,	//	切换一张轮播图所需的时间，单位ms
					"circular" => 'true',	//	是否衔接播放
				],
				'data' => [
					[
						"imgUrl" => base_url() . 'addons/food/img/diy/banner_01.jpg',
						"title" => '',
						"url" => ''
					],
					[
						"imgUrl" => base_url() . 'addons/food/img/diy/banner_02.jpg',
						"title" => '',
						"url" => ''
					]
				]
			],
			//单图组
			'imageSingle' => [
				'name' => '单图组',
				'type' => 'imageSingle',
				'style' => [
					'borderRadius' => '0',
					'paddingTop' => '0',
					'paddingLeft' => '0',
					'background' => '#ffffff'
				],
				'data' => [
					[
						'imgUrl' => base_url().'addons/food/img/diy/banner_01.jpg',
						'imgName' => 'image-1.jpg',
						'linkUrl' => ''
					]
				]
			],
			//图片橱窗
			'window' => [
				'name' => '图片橱窗',
				'type' => 'window',
				'style' => [
					'borderRadius' => '0',
					'paddingTop' => '0',
					'paddingLeft' => '0',
					'background' => '#ffffff',
					'layout' => '2'
				],
				'data' => [
					[
						'imgUrl' => base_url().'addons/food/img/diy/window_01.jpg',
						'linkUrl' => ''
					],
					[
						'imgUrl' => base_url().'addons/food/img/diy/window_02.jpg',
						'linkUrl' => ''
					],
					[
						'imgUrl' => base_url().'addons/food/img/diy/window_03.jpg',
						'linkUrl' => ''
					],
					[
						'imgUrl' => base_url().'addons/food/img/diy/window_04.jpg',
						'linkUrl' => ''
					]
				],
				'dataNum' => 4
			],
			//视频组
			'video' => [
				'name' => '视频组',
				'type' => 'video',
				'params' => [
					'videoUrl' => 'http://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400',
					'poster' => base_url().'addons/food/img/diy/video_poster.png',
					'autoplay' => '0'
				],
				'style' => [
					'paddingTop' => '0',
					'height' => '190',
					'background' => '#ffffff'
				]
			],
			//图文资讯
			'article' => [
				'name' => '图文资讯',
				'type' => 'article',
				'params' => [
					'source' => 'auto',
					'auto' => [
						'category' => 0,
						'showNum' => 6
					]
				],
				'style' => [],
				'defaultData' => [
					[
						'article_title' => '此处显示图文资讯标题',
						'show_type' => 10,
						'image' => base_url().'addons/food/img/diy/article_01.png',
						'views_num' => '309'
					],
					[
						'article_title' => '此处显示图文资讯标题',
						'show_type' => 10,
						'image' => base_url().'addons/food/img/diy/article_01.png',
						'views_num' => '309'
					]
				],
				'data' => []
			],
			//头条快报
			'special' => [
				'name' => '头条快报',
				'type' => 'special',
				'params' => [
					'source' => 'auto',
					'auto' => [
						'category' => 0,
						'showNum' => 6
					]
				],
				'style' => [
					'display' => '1',
					'image' => base_url().'addons/food/img/diy/special.png'
				],
				'defaultData' => [
					[
						'article_title' => '张小龙4小时演讲：你和高手之间，隔着“简单”二字'
					],
					[
						'article_title' => '张小龙4小时演讲：你和高手之间，隔着“简单”二字'
					]
				],
				'data' => []
			],
			//直播间
			'liveRoom' => [
				'name' => '直播间',
				'type' => 'liveRoom',
				'params' => [
					'image' => base_url().'addons/food/img/diy/live_room.png'
				],
				'style' => [
					'right' => '1',
					'bottom' => '10',
					'opacity' => '100'
				]
			],
			//公告组
			'notice' => [
				'name' => '公告组',
				'type' => 'notice',
				'params' => [
					'mode' => 'horizontal', //horizontal(水平滚动)	vertical(垂直滚动)
					'isCircular' => 'true'	//是否水平滚动衔接
				],
				'style' => [
					'paddingTop' => '0',
					'borderRadius' => '0',
					'volumeIcon' => 'true',	//是否显示小喇叭图标
					'moreIcon' => 'true',	//是否显示右边的向右箭头
					'type' => 'warning' //显示的主题 primary / warning / error / success / none
				],
				'data' => [
					[
						'title' => '这里是第一条自定义公告的标题',
						'url' => ''
					],
					[
						'title' => '这里是第一条自定义公告的标题',
						'url' => ''
					]
				]
			],
			//导航组
			'navBar' => [
				//导航组
				'name' => '导航组',
				'type' => 'navBar',
				'style' => [
					'bgColor' => '#ffffff',
					'rowsNum' => '4'
				],
				'data' => [
					[
						'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
						'linkUrl' => '',
						'text' => '按钮文字1',
						'color' => '#666666'
					],
					[
						'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
						'linkUrl' => '',
						'text' => '按钮文字2',
						'color' => '#666666'
					],
					[
						'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
						'linkUrl' => '',
						'text' => '按钮文字3',
						'color' => '#666666'
					],
					[
						'imgUrl' => base_url().'addons/food/img/diy/navbar_01.png',
						'linkUrl' => '',
						'text' => '按钮文字4',
						'color' => '#666666'
					],
				]
			],
			//商品组
			'goods' => [
				'name' => '商品组',
				'type' => 'goods',
				'params' => [
					'source' => 'auto', //商品来源，auto=自动选择，choice=手动选择
					'auto' => [	//自动选择配置
						'category' => 0, //分类编号
						'goodsSort' => 'all',	//商品排序，all=综合，sales=销量，new=新品,recommend推荐
						'showNum' => 6	//显示数量
					]
				],
				'style' => [
					'background' => '#FFFFFF', //背景颜色
					'display' => 'list', 	//显示类型，list=列表平铺，slide=横向滑动
					'column' => '2',	//分列数量，1=单列，2=双列，3=三列
					'show' => [	//显示内容
						'goodsName' => 'true',	//商品名称
						'goodsPrice' => 'true',	//商品价格
						'linePrice' => 'true',	//划线价格
						'sellingPoint' => 'true',//商品卖点
						'goodsSales' => 'true'	//商品销量
					]
				],
				'defaultData' => [	//默认数据
					[
						'goods_name' => '此处显示商品名称',
						'image' => base_url().'addons/food/img/diy/goods_01.png',
						'goods_price' => '99.00',
						'line_price' => '139.00',
						'selling_point' => '此款商品美观大方 不容错过',
						'goods_sales' => '100'
					],
					[
						'goods_name' => '此处显示商品名称',
						'image' => base_url().'addons/food/img/diy/goods_01.png',
						'goods_price' => '99.00',
						'line_price' => '139.00',
						'selling_point' => '此款商品美观大方 不容错过',
						'goods_sales' => '100'
					],
					[
						'goods_name' => '此处显示商品名称',
						'image' => base_url().'addons/food/img/diy/goods_01.png',
						'goods_price' => '99.00',
						'line_price' => '139.00',
						'selling_point' => '此款商品美观大方 不容错过',
						'goods_sales' => '100'
					],
					[
						'goods_name' => '此处显示商品名称',
						'image' => base_url().'addons/food/img/diy/goods_01.png',
						'goods_price' => '99.00',
						'line_price' => '139.00',
						'selling_point' => '此款商品美观大方 不容错过',
						'goods_sales' => '100'
					]
				],
				'data' => [	//选择数据
					[
						'goods_name' => '此处显示商品名称',
						'image' => base_url().'addons/food/img/diy/goods_01.png',
						'goods_price' => '99.00',
						'line_price' => '139.00',
						'selling_point' => '此款商品美观大方 不容错过',
						'goods_sales' => '100',
						'is_default' => true
					]
				]
			],
			//线下门店
			'shop' => [
				'name' => '线下门店',
				'type' => 'shop',
				'params' => [
					'source' => 'auto',
					'auto' => [
						'showNum' => 6
					]
				],
				'style' => [
					'background' => '#FFFFFF', //背景颜色
				],
				'defaultData' => [
					[
						'shop_name' => '此处显示门店名称',
						'logo_image' => base_url().'addons/food/img/diy/circular.png',
						'phone' => '010-6666666',
						'region' => [
							'province' => 'xx省',
							'city' => 'xx市',
							'region' => 'xx区'
						],
						'address' => 'xx街道'
					],
					[
						'shop_name' => '此处显示门店名称',
						'logo_image' => base_url().'addons/food/img/diy/circular.png',
						'phone' => '010-6666666',
						'region' => [
							'province' => 'xx省',
							'city' => 'xx市',
							'region' => 'xx区'
						],
						'address' => 'xx街道'
					]
				],
				'data' => [
					[
						'shop_name' => '此处显示门店名称',
						'logo_image' => base_url().'addons/food/img/diy/circular.png',
						'phone' => '010-6666666',
						'region' => [
							'province' => 'xx省',
							'city' => 'xx市',
							'region' => 'xx区'
						],
						'address' => 'xx街道'
					]
				]
			],
			//优惠券组
			'coupon' => [
				'name' => '优惠券组',
				'type' => 'coupon',
				'style' => [
					'paddingTop' => '10',
					'background' => '#ffffff'
				],
				'params' => [
					'limit' => '5'
				],
				'data' => [
					[
						'color' => 'red',
						'reduce_price' => '10',
						'min_price' => '100.00'
					],
					[
						'color' => 'violet',
						'reduce_price' => '10',
						'min_price' => '100.00'
					]
				]
			],
			//在线客服
			'service' => [
				'name' => '在线客服',
				'type' => 'service',
				'params' => [
					'type' => 'chat',//chat=客服，phone=拨打电话
					'image' => base_url().'addons/food/img/diy/service.png',
					'phone_num' => ''
				],
				'style' => [
					'right' => '1',
					'bottom' => '10',
					'opacity' => '100'
				]
			],
			//关注公众号
			'officialAccount' => [
				'name' => '关注公众号',
				'type' => 'officialAccount',
				'params' => [],
				'style' => []
			],
			//富文本
			'richText' => [
				'name' => '富文本',
				'type' => 'richText',
				'params' => [
					'content' => '这里是文本的内容'
				],
				'style' => [
					'paddingTop' => '0',
					'paddingLeft' => '0',
					'background' => '#ffffff'
				]
			],
			//辅助空白
			'blank' => [
				'name' => '辅助空白',
				'type' => 'blank',
				'style' => [
					'height' => '10',
					'background' => '#f7f7f7'
				]
			],
			//辅助线
			'guide' => [
				'name' => '辅助线',
				'type' => 'guide',
				'style' => [
					'background' => '#ffffff',
					'lineStyle' => 'solid',
					'lineHeight' => '1',
					'lineColor' => '#f7f7f7',
					'paddingTop' => '10'
				]
			],
			//栏目标题
			'columnTitle' => [
				'name' => '栏目标题',
				'type' => 'columnTitle',
				'params' => [
					'title' => '标题名称',//左边主标题
					'subTitle' => '更多',//右边副标题
					'right' => 'true',//是否显示右边的内容
					'showLine' => 'true',//是否显示左边的竖条
					'arrow' => 'true',//是否显示右边箭头
					'url' => '',
				],
				'style' => [
					'fontSize' => '28',//主标题的字体大小
					'bold' => 'true',//主标题是否加粗
					'color' => '#303133',//主标题颜色
					'subColor' => '#909399',//右边副标题的颜色
					'lineColor' => '#ff9900',//左边竖线的颜色
					'background' => '#f7f7f7',
					'paddingTop' => '20'
				]
			]
		];
		$temp['json'] = json_encode($temp['array']);
		return $temp;
        
    }
	

}
