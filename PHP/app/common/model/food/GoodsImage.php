<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 商品图片模型
 */
class GoodsImage extends BaseModel
{
    // 定义表名
    protected $name = 'food_goods_image';

    // 定义主键
    protected $pk = 'id';
    protected $updateTime = false;
    
    /**
     * 关联文件库
     */
    public function file()
    {
        return $this->belongsTo('addons\\upload\\model\\UploadFile', 'image_id', 'file_id')
            ->bind(['file_path', 'url', 'domain']);
    }
}
