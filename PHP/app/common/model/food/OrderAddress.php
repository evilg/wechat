<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 订单收货地址模型
 */
class OrderAddress extends BaseModel
{
    // 定义表名
    protected $name = 'food_order_address';
    protected $updateTime = false;

    // 定义主键
    protected $pk = 'order_address_id';

    // 追加字段
    protected $append = [];

}
