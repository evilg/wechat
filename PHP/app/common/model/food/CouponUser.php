<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 优惠券用户领取模型
 */
class CouponUser extends BaseModel
{
    // 定义表名
    protected $name = 'food_coupon_user';

    // 定义主键
    protected $pk = 'coupon_user_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->belongsTo('app\\common\\model\\User','user_id');
    }

    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\food\\Shop','shop_id');
    }

    /**
     * 关联优惠券表
     */
    public function coupon()
    {
        return $this->belongsTo('app\\common\\model\\food\\Coupon','coupon_id');
    }
    
    /**
     * 到期时间
     */
    public function getExpiretimeAttr($value)
    {
        $time = $value - time();
        if($time > 86400){ 
            $text = round($time / 86400) . '天';
        }elseif($time > 3600){
            $text = round($time / 3600) . '小时';
        }elseif($time > 60){
            $text = round($time / 60) . '分钟';
        }
        return ['text' => $text, 'value' => $value];
    }

    /**
     * 类型
     */
    public function getTypeAttr($value)
    {
        $status = [10 => '现金券', 20 => '折扣券', 30 => '赠送券', 40 => '减免券'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 规则
     */
    public function getRuleAttr($value)
    {
        $status = [10 => '通用券', 20 => '外卖券'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($user_id = 0, $type = 0, $shop_id = 0)
    {
        //删除3分钟后过期的优惠券
        $this->where('expiretime','<',time()-180)->delete();
        //筛选
        $filter = [];
        $user_id > 0 && $filter['user_id'] = $user_id;
        $type == 10 && $filter['rule'] = $type;
        // 排序规则
        $sort = [];
        $sort = ['expiretime' => 'asc','coupon_user_id' => 'desc'];//按照时间排序
        $model = $this->with(['coupon','user','shop'])->where($filter)->order($sort);
        $shop_id > 0 && $model->where('shop_id','in',[0,$shop_id]);
        // 执行查询
        return $model->select();
    }

    /**
     * 添加
     */
    public function add(array $data, $user_id)
    {
        //组成用户批量记录
        $userCoupon = array();
        foreach ($data as $item) {
            //计算优惠券到期时间
            switch ($item['valid_time']['value']) {
                case '10':
                    $expiretime = strtotime(date('Y-m-d').'23:59:59');
                    break;
                case '20':
                    $expiretime = strtotime("+1 week");
                    break;
                case '30':
                    $expiretime = strtotime("+1 month");
                    break;
                case '40':
                    $expiretime = strtotime("+1 year");
                    break;
                default:
                    $expiretime = time();
                    break;
            }
            array_push($userCoupon,[
                'coupon_id' => $item['coupon_id'],
                'type' => $item['type']['value'],
                'rule' => $item['rule']['value'],
                'user_id' => $user_id,
                'applet_id' => self::$applet_id,
                'expiretime' => $expiretime
            ]);
        }
        return $this->saveAll($userCoupon); 
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
    
    /**
     * 用户统计数量
     */
    public static function getUserCount($user_id = 0)
    {
        // 筛选条件
        $filter = [];
        $user_id > 0 && $filter['user_id'] = $user_id;
        $count = array();
        $count['all'] = self::where('expiretime','>',time())->where($filter)->count();
        $count['10'] = self::where('expiretime','>',time())->where($filter)->where('type',10)->count();
        $count['20'] = self::where('expiretime','>',time())->where($filter)->where('type',20)->count();
        $count['30'] = self::where('expiretime','>',time())->where($filter)->where('type',30)->count();
        $count['40'] = self::where('expiretime','>',time())->where($filter)->where('type',40)->count();
        return $count;
    }
}
