<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 规格/属性(组)模型
 */
class Spec extends BaseModel
{
    // 定义表名
    protected $name = 'food_spec';
    protected $updateTime = false;

    // 定义主键
    protected $pk = 'spec_id';

    // 追加字段
    protected $append = [];

    /**
     * 根据规格组名称查询规格id
     */
    public function getSpecIdByName(string $spec_name)
    {
        return $this->where(compact('spec_name'))->value('spec_id');
    }

    /**
     * 新增规格组
     */
    public function add(string $spec_name)
    {
        $applet_id = self::$applet_id;
        return $this->save(compact('spec_name', 'applet_id'));
    }

}
