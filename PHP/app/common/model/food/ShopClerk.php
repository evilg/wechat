<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 店员模型
 */
class ShopClerk extends BaseModel
{
    // 定义表名
    protected $name = 'food_shop_clerk';

    // 定义主键
    protected $pk = 'shop_clerk_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\food\\Shop','shop_id');
    }
    
    /**
     * 身份
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '店员', 20 => '店长', 30 => '配送'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($shop_id = 0, string $search = '',$is_clerk=false)
    {
        $model = $this->with(['shop']);
        if($is_clerk){
           $model->where('status','<>',20); 
        }
        //筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        if(!empty($search)){
            //判断是否是手机号
            if(is_phone($search)){
                $filter['mobile'] = $search;
            }else{
                $filter['real_name'] = $search;
            }
        }
        // 排序规则
        $sort = [];
        $sort = ['shop_clerk_id' => 'desc'];
        // 执行查询
        return $model->where($filter)
            ->order($sort)
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 获取列表
     */
    public function getAll($shop_id = 0, $status = 0)
    {
        //筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $status > 0 && $filter['status'] = $status;
        // 排序规则
        $sort = [];
        $sort = ['shop_clerk_id' => 'desc'];
        // 执行查询
        return $this->where($filter)->order($sort)->select();
    }

    /**
     * 条件获取详情
     */
    public static function getClerk(array $filter = [])
    {
        return self::where($filter)->find();
    }

    /**
     * 新增默认店长
     */
    public function insertDefault($shop_id,$data,$applet_id)
    {
        // 添加默认店长
        return $this->save([
            'shop_id' => $shop_id,
            'real_name' => $data['linkman'],
            'mobile' => $data['phone'],
            'pwd' => hema_hash($data['phone']),
            'status' => 20,
            'applet_id' => $applet_id,
        ]);
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        if(!is_phone($data['mobile'])){
            $this->error = '手机号码错误';
            return false;   
        }
        //验证手机号是否存在
        if($this->withoutGlobalScope()->where(['mobile' => $data['mobile']])->count()){
            $this->error = '手机号码被占用';
            return false;
        }
        $data['pwd'] = hema_hash($data['mobile']);
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        //如果修改了手机号码，密码重置为新手机号码
        if($this['mobile'] != $data['mobile']){
            if(!is_phone($data['mobile'])){
                $this->error = '手机号码错误';
                return false;   
            }
            if($this->withoutGlobalScope()->where(['mobile' => $data['mobile']])->count()){
                $this->error = '手机号码被占用';
                return false;
            }
            $data['pwd'] = hema_hash($data['mobile']);
        }
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }

}
