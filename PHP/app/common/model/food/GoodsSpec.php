<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 商品规格模型
 */
class GoodsSpec extends BaseModel
{
    // 定义表名
    protected $name = 'food_goods_spec';

    // 定义主键
    protected $pk = 'goods_spec_id';

    // 追加字段
    protected $append = [];

    /**
     * 规格图片
     */
    public function image()
    {
        return $this->hasOne('addons\\upload\\model\\uploadFile', 'file_id', 'image_id');
    }

    /**
     * 批量添加商品sku记录
     */
    public function addSkuList($goods_id, array $spec_list)
    {
        $data = [];
        foreach ($spec_list as $item) {
            $data[] = array_merge($item['form'], [
                'spec_sku_id' => $item['spec_sku_id'],
                'goods_id' => $goods_id,
                'applet_id' => self::$applet_id,
            ]);
        }
        return $this->saveAll($data);
    }

    /**
     * 添加商品规格关系记录
     */
    public function addGoodsSpecRel($goods_id, $spec_attr)
    {
        $data = [];
        array_map(function ($val) use (&$data, $goods_id) {
            array_map(function ($item) use (&$val, &$data, $goods_id) {
                $data[] = [
                    'goods_id' => $goods_id,
                    'spec_id' => $val['group_id'],
                    'spec_value_id' => $item['item_id'],
                    'applet_id' => self::$applet_id,
                ];
            }, $val['spec_items']);
        }, $spec_attr);
        $model = new GoodsSpecRel;
        return $model->saveAll($data);
    }

    /**
     * 移除指定商品的所有sku
     */
    public function removeAll($goods_id)
    {
        $model = new GoodsSpecRel;
        $model->where('goods_id','=', $goods_id)->delete();
        return $this->where('goods_id','=', $goods_id)->delete();
    }
}
