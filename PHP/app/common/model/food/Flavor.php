<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 口味选项模型
 */
class Flavor extends BaseModel
{
    // 定义表名
    protected $name = 'food_flavor';
    // 定义主键
    protected $pk = 'flavor_id';

    protected $append = ['checked'];
    
    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\food\\Shop','shop_id');
    }

    /**
     * 选项状态
     */
    public function getCheckedAttr($value)
    {
        return false;
    }

    /**
     * 获取列表
     */
    public function getList($shop_id = 0)
    {
        //筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        // 排序规则
        $sort = [];
        $sort = ['sort', 'flavor_id' => 'desc'];
        // 执行查询
        return $this->with(['shop'])
            ->where($filter)
            ->order($sort)
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 新增
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }

    /**
     * 更新
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove() 
    {
        return $this->delete();
    }

}
