<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 用户充值套餐模型
 */
class RechargePlan extends BaseModel
{
    // 定义表名
    protected $name = 'food_recharge_plan';

    // 定义主键
    protected $pk = 'recharge_plan_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联优惠券表
     */
    public function coupon()
    {
        return $this->belongsTo('app\\common\\model\\food\\Coupon','coupon_id');
    }

    /**
     * 赠品类型
     */
    public function getGiftTypeAttr($value)
    {
        $status = [10 =>'优惠券', 20 => '现金'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        // 执行查询
        return $this->with(['coupon'])->order(['recharge_plan_id'=> 'desc','sort' => 'asc'])->select();
    }

    /**
     * 详情
     */
    public static function detail($id)
    {
        return self::get($id,['coupon']);
    }

    /**
     * 新增记录
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);
    }

    /**
     * 更新记录
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除记录
     */
    public function remove() 
    {
        return $this->delete();
    }
}
