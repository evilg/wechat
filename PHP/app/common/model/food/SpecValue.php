<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 规格/属性(值)模型
 */
class SpecValue extends BaseModel
{
    // 定义表名
    protected $name = 'food_spec_value';
    protected $updateTime = false;

    // 定义主键
    protected $pk = 'spec_value_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联规格组表
     */
    public function spec()
    {
        return $this->belongsTo('app\\common\\model\\food\\Spec','spec_id');
    }

    /**
     * 根据规格组名称查询规格id
     */
    public function getSpecValueIdByName($spec_id, $spec_value)
    {
        return self::where(compact('spec_id', 'spec_value'))->value('spec_value_id');
    }

    /**
     * 新增规格值
     */
    public function add($spec_id, $spec_value)
    {
        $applet_id = self::$applet_id;
        return $this->save(compact('spec_value', 'spec_id', 'applet_id'));
    }
}
