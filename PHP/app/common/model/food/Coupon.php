<?php
namespace app\common\model\food;

use app\common\model\BaseModel;

/**
 * 优惠券模型
 */
class Coupon extends BaseModel
{
    // 定义表名
    protected $name = 'food_coupon';

    // 定义主键
    protected $pk = 'coupon_id';

    // 追加字段
    protected $append = [];

    /**
     * 获取器: 转义数组格式
     */
    public function getValuesAttr($value)
    {
        return json_decode($value, true);
    }

    /**
     * 修改器: 转义成json格式
     */
    public function setValuesAttr($value)
    {
        return json_encode($value);
    }

    /**
     * 类型
     */
    public function getTypeAttr($value)
    {
        $status = [10 => '现金券', 20 => '折扣券', 30 => '赠送券', 40 => '减免券'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 规则
     */
    public function getRuleAttr($value)
    {
        $status = [10 => '通用券', 20 => '外卖券'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 有效期
     */
    public function getValidTimeAttr($value)
    {
        $status = [10 => '当天', 20 => '一周', 30 => '一月', 40 => '一年'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取商品列表
     */
    public function getList($type = 0)
    {
        // 筛选条件
        $filter = [];
        $type > 0 && $filter['type'] = $type;

        // 排序规则
        $sort = ['coupon_id' => 'desc'];
        // 执行查询
        return $this->where($filter)
            ->order($sort)
            ->select();
    }

    /**
     * 添加
     */
    public function add(array $data)
    {
        $data['applet_id'] = self::$applet_id;
        return $this->save($data);    
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除
     */
    public function remove()
    {
        //验证是否有未使用
        if ($userCoupon = (new CouponUser)->where(['coupon_id' => $this->coupon_id])->count()) {
            $this->error = '用户卡包中有' . $userCoupon . '张未使用，不允许删除';
            return false;
        }
        //验证充值套餐赠品中是否存在该优惠券
        if ($planCoupon = (new RechargePlan)->where(['coupon_id' => $this->coupon_id])->count()) {
            $this->error = '充值套餐中设置为了赠品，需取消该赠品后才可以删除';
            return false;
        }
        //验证会员升级赠品中是否存在该优惠券
        if($grade = Setting::getItem('grade')){
            foreach ($grade as $vo) {
               if(isset($vo['gift'])){
                    foreach ($vo['gift'] as $item) {
                        if($item['coupon_id'] == $this->coupon_id){
                            $this->error = '会员升级中设置为了赠品，需取消该赠品后才可以删除';
                            return false;
                        }
                    }
               }
            }
        }
        return $this->delete(); 
    }
}
