<?php
namespace app\common\model\food;

use app\common\model\BaseModel;
use app\common\model\User;

/**
 * 预约模型
 */
class Pact extends BaseModel
{
    // 定义表名
    protected $name = 'food_pact';

    // 定义主键
    protected $pk = 'pact_id';

    // 追加字段
    protected $append = [
            'user'
        ];
    
    /**
     * 用户表
     */
    public function getUserAttr($value,$data)
    {
        return User::withoutGlobalScope()->find($data['user_id']);
    }
    
    /**
     * 关联门店表
     */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\food\\Shop','shop_id');
    }
    
    /**
     * 关联桌位表
     */
    public function table()
    {
        return $this->belongsTo('app\\common\\model\\food\\Table','table_id');
    }
    
    
    /**
     * 约定时间
     */
    public function getPactTimeAttr($value)
    {
        return ['date' => date("Y-m-d H:i:s",$value),'text' => date("m-d H",$value), 'value' => $value];
    }
    
    /**
     * 状态
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '预约中', 20 => '已过期', 30 => '已守约', 40 => '已取消'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取列表
     */
    public function getList($pact_type = 10, $shop_id = 0, $status = 0)
    {
        //筛选
        $filter = [];
        $filter['pact_type'] = $pact_type;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $status > 0 && $filter['status'] = $status;
        // 排序规则
        $sort = [];
        $sort = ['pact_id' => 'desc'];//按照约定时间排序
        // 执行查询
        return $this->with(['shop','table'])
            ->where($filter)
            ->order($sort)
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 获取详情
     */
    public static function detail($id)
    {
        return self::with(['shop','table'])->find($id);
    }
    
    /**
     * 状态操作
     */
    public function status($status)
    {
        return $this->save(['status' => $status]);
    }

    /**
     * 进行中的统计
     */
    public static function getNowCount($pact_type = 10, $shop_id = 0)
    {
         // 筛选条件
        $filter['status'] = 10;
        $filter['pact_type'] = $pact_type;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        return self::where($filter)->count();
    }
    
    /**
     * 根据时间段统计数量
     */
    public static function getDateCount(array $shop = [])
    {
        $self = new static;
         // 筛选条件
        $filter = [];
        $shop['shop_id'] > 0 && $filter['shop_id'] = $shop['shop_id'];
        $applet_id = $self::$applet_id;
        empty($applet_id) && $filter['applet_id'] = $applet_id;
        $count = array();
        $count['sort'] = self::where('pact_type',10)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
        $count['table'] = self::where('pact_type',20)->where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter)->count();
        return $count;
    }
    
    /**
     * 根据条件统计数量
     */
    public static function getCount($shop_id = 0)
    {
        $self = new static;
         // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $applet_id = $self::$applet_id;
        empty($applet_id) && $filter['applet_id'] = $applet_id;
        $count = array();
        //全部
        $count[0]['sort'] = self::where('pact_type',10)->where($filter)->count();
        $count[0]['table'] = self::where('pact_type',20)->where($filter)->count();
        //今天
        $star = strtotime(date("Y-m-d"),time());
        $count[1]['sort'] = self::where('pact_type',10)->where('create_time','>',$star)->where($filter)->count();
        $count[1]['table'] = self::where('pact_type',20)->where('create_time','>',$star)->where($filter)->count();
        //昨天
        $star = strtotime("-1 day");
        $end = strtotime(date("Y-m-d"),time());
        $count[2]['sort'] = self::where('pact_type',10)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        $count[2]['table'] = self::where('pact_type',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //本月起至时间 - 月度统计 
        $end = mktime(0,0,0,date('m'),1,date('y')); 
        $count[8]['sort'] = self::where('pact_type',10)->where('create_time','>',$end)->where($filter)->count();
        $count[8]['table'] = self::where('pact_type',20)->where('create_time','>',$end)->where($filter)->count();
        //上月开始  
        $star = mktime(0,0,0,date('m')-1,1,date('y'));
        $count[9]['sort'] = self::where('pact_type',10)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        $count[9]['table'] = self::where('pact_type',20)->where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();     
        return $count;
    }

}
