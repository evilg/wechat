<?php
namespace app\common\model\food;

use app\common\model\BaseModel;
use app\common\model\User;

/**
 * 评论模型类
 */
class Comment extends BaseModel
{
    // 定义表名
    protected $name = 'food_comment';

    // 定义主键
    protected $pk = 'comment_id';
    protected $append = ['total','user'];

    /**
     * 用户评分
     */
    public function getTotalAttr($value, $data)
    {
        $value = ($data['serve']+$data['speed']+$data['flavor']+$data['ambient'])/4;
        return $value;
    }
    
    /**
     * 用户表
     */
    public function getUserAttr($value,$data)
    {
        return User::withoutGlobalScope()->find($data['user_id']);
    }
    
    /**
     * 关联订单
    */
    public function order()
    {
        return $this->belongsTo('app\\common\\model\\food\\Order','order_id');
    }
    
    /**
     * 关联门店
    */
    public function shop()
    {
        return $this->belongsTo('app\\common\\model\\food\\Shop','shop_id');
    }

    
    /**
     * 显示状态
     */
    public function getIsShowAttr($value)
    {
        $status = [0 => '隐藏', 1 => '显示'];
        return ['text' => $status[$value], 'value' => $value];
    }
    
    /**
     * 获取列表
    */
    public function getList($user_id = 0, $shop_id = 0, $is_show = -1)
    {
        // 筛选条件
        $filter = [];
        $user_id > 0 && $filter['user_id'] = $user_id;
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $is_show > -1 && $filter['is_show'] = $is_show;
        $list = $this->with(['order','shop'])
            ->where($filter)
            ->order(['comment_id' => 'desc'])
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
        return $list;
    }











    
    
    /**
     * 详情
     */
    public static function detail($id)
    {
        return self::with(['order','shop'])->find($id);
    }

    /**
     * 编辑
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 显示状态编辑
     */
    public function status()
    {
        $this->is_show['value']==0 ? $this->is_show = 1 : $this->is_show = 0;
        return $this->save() !== false;
    }

    /**
     * 评分计算
     */
    public function score($shop_id = 0)
    {
        $where = [];
        $shop_id > 0 && $where['shop_id'] = $shop_id;
        $serve = $this->where($where)->avg('serve');
        $speed = $this->where($where)->avg('speed');
        $flavor = $this->where($where)->avg('flavor');
        $ambient = $this->where($where)->avg('ambient');
        $serve <=0 && $serve = 5;
        $speed <=0 && $speed = 5;
        $flavor <=0 && $flavor = 5;
        $ambient <=0 && $ambient = 5;
        $all = ($serve+$speed+$flavor+$ambient)/4;
        return [
            'all' => round($all,2),
            'serve' => round($serve,2),
            'speed' => round($speed,2),
            'flavor' => round($flavor,2),
            'ambient' => round($ambient,2)
        ];
    }
    
    /**
     * 根据条件统计数量
     */
    public static function getCount($shop_id = 0)
    {
        $self = new static;
         // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        $applet_id = $self::$applet_id;
        empty($applet_id) && $filter['applet_id'] = $applet_id;
        $count = array();
        //全部
        $count[0] = self::where($filter)->count();
        //今天
        $star = strtotime(date("Y-m-d"),time());
        $count[1] = self::where('create_time','>',$star)->where($filter)->count();
        //昨天
        $star = strtotime("-1 day");
        $end = strtotime(date("Y-m-d"),time());
        $count[2] = self::where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //前天
        $star = strtotime("-2 day");
        $end = strtotime("-1 day");
        $count[3] = self::where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-4天
        $star = strtotime("-3 day");
        $end = strtotime("-2 day");
        $count[4] = self::where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-5天
        $star = strtotime("-4 day");
        $end = strtotime("-3 day");
        $count[5] = self::where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-6天
        $star = strtotime("-5 day");
        $end = strtotime("-4 day");
        $count[6] = self::where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        //-7天
        $star = strtotime("-6 day");
        $end = strtotime("-5 day");
        $count[7] = self::where('create_time','>',$star)->where('create_time','<',$end)->where($filter)->count();
        return $count;
    }
}
