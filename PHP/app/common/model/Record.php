<?php
namespace app\common\model;

/**
 * 交易记录模型
 */
class Record extends BaseModel
{
    // 定义表名
    protected $name = 'record';

    // 定义主键
    protected $pk = 'record_id';

    // 追加字段
    protected $append = [
        'user'    
    ];
    
    /**
     * 用户信息
     */
    public function getUserAttr($value,$data)
    {
        return User::withoutGlobalScope()->where('user_id',$data['user_id'])->find();
    }

    /**
     * 交易类型
     */
    public function getTypeAttr($value)
    {
        $status = [10 => '余额', 20 => '现金', 30 => '微信', 90 => '积分'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 交易模式
     */
    public function getModeAttr($value)
    {
        $status = [10 => '自助', 20 => '后台'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 交易方式
     */
    public function getActionAttr($value)
    {
        $status = [10 => '充值', 20 => '扣减', 30 => '重置', 40 => '消费', 50 => '分红', 60 => '退款', 70 => '提现'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 交易状态
     */
    public function getStatusAttr($value)
    {
        $status = [10 => '处理中', 20 => '已完成'];
        return ['text' => $status[$value], 'value' => $value];
    }
    
    /**
     * 获取列表
     */
    public function getList($user_type = 0, $user_id = 0, $shop_id = 0, $agent_id = 0)
    {
        // 筛选条件
        $filter = [];
        $model = self::order('record_id','desc');
        if($agent_id > 0){
            //代理自己的流水
            if($user_type == 0){
                $filter['user_id'] = $agent_id;
                $filter['user_type'] = 30;
            }
            //代理所有商家的流水
            if($user_type == 20){
                $filter['user_type'] = 20;
                if($userId = (new User)->where('agent_id',$agent_id)->column('user_id')){
                    $model->where('user_id','in',$userId);
                }
            }
            //代理所有商家的所有会员流水
            if($user_type == 10){
                $filter['user_type'] = 10;
                if($userId = (new User)->where('agent_id',$agent_id)->column('user_id')){
                    if($appletId = (new Applet)->where('user_id','in',$userId)->column('applet_id')){
                        $model->where('applet_id','in',$appletId);
                    }
                }
            }
        }else{
            $user_type > 0 && $filter['user_type'] = $user_type;
            $user_id > 0 && $filter['user_id'] = $user_id;
            $shop_id > 0 && $filter['shop_id'] = $shop_id;
        }
        // 执行查询
        return $model->where($filter)->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 订单详情
     */
    public static function detail($id)
    {
        return self::get($id);
    }
    
    /**
     * 根据时间段统计数量
     */
    public static function getDateCount(array $shop)
    {
        $self = new static;
         // 筛选条件
        $filter = [];
        $shop['shop_id'] > 0 && $filter['shop_id'] = $shop['shop_id'];
        $applet_id = $self::$applet_id;
        empty($applet_id) && $filter['applet_id'] = 0;
        $model = self::where('create_time','>',$shop['star'])->where('create_time','<',$shop['end'])->where($filter);
        $count['10'] = $model->where('action',10)->sum('money');//充值
        $count['40'] = $model->where('action',40)->sum('money');//消费
        $count['50'] = $model->where('action',50)->sum('money');//分红
        $count['60'] = $model->where('action',60)->sum('money');//退款
        $count['90'] = $model->where('action',10)->sum('gift_money');//赠送
        $count['91'] = $count['10'] + $count['40'];//成交额
        return $count;
    }
    
    /**
     * 根据条件统计数量
     */
    public static function getCount($shop_id = 0, $agent_id = 0)
    {
        $self = new static;
        // 筛选条件
        $filter = [];
        $shop_id > 0 && $filter['shop_id'] = $shop_id;
        if($agent_id == 0){
            $applet_id = $self::$applet_id;
            empty($applet_id) && $filter['applet_id'] = 0;
        }
        $count = array();
        if($agent_id > 0){
            $userId = (new User)->where('agent_id',$agent_id)->column('user_id');
            //全部
            $count[0]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->sum('money');//充值
            $count[0]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->sum('money');//消费
            $count[0]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->sum('money');//分红
            $count[0]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->sum('money');//退款
            $count[0]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->sum('gift_money');//赠送
            $count[0]['91'] = $count[0]['10'] + $count[0]['40'];//成交额
            $count[0]['92'] = self::where($filter)->where(['action' => 70])->sum('money');//商户提现总额
            $count[0]['93'] = self::where($filter)->where(['action' => 70,'user_id' => $agent_id])->sum('money');//代理提现总额
            //今天
            $star = strtotime(date("Y-m-d"),time());
            $count[1]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->sum('money');
            $count[1]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->sum('money');
            $count[1]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->sum('money');
            $count[1]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->sum('money');
            $count[1]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->sum('gift_money');
            $count[1]['91'] = $count[1]['10'] + $count[1]['40'];//成交额
            //昨天
            $star = strtotime("-1 day");
            $end = strtotime(date("Y-m-d"),time());
            $count[2]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[2]['91'] = $count[2]['10'] + $count[2]['40'];//成交额
            //前天
            $star = strtotime("-2 day");
            $end = strtotime("-1 day");
            $count[3]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[3]['91'] = $count[3]['10'] + $count[3]['40'];//成交额
            //-4天
            $star = strtotime("-3 day");
            $end = strtotime("-2 day");
            $count[4]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[4]['91'] = $count[4]['10'] + $count[4]['40'];//成交额
            //-5天
            $star = strtotime("-4 day");
            $end = strtotime("-3 day");
            $count[5]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[5]['91'] = $count[5]['10'] + $count[5]['40'];//成交额
            //-6天
            $star = strtotime("-5 day");
            $end = strtotime("-4 day");
            $count[6]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[6]['91'] = $count[6]['10'] + $count[6]['40'];//成交额
            //-7天
            $star = strtotime("-6 day");
            $end = strtotime("-5 day");
            $count[7]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[7]['91'] = $count[7]['10'] + $count[7]['40'];//成交额
            
            //本月起至时间 - 月度统计 
            $end = mktime(0,0,0,date('m'),1,date('y')); 
            $count[8]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$end)->sum('money');
            $count[8]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$end)->sum('money');
            $count[8]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$end)->sum('money');
            $count[8]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$end)->sum('money');
            $count[8]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$end)->sum('gift_money');
            $count[8]['91'] = $count[8]['10'] + $count[8]['40'];//成交额
            
            //上月开始  
            $star = mktime(0,0,0,date('m')-1,1,date('y'));
            $count[9]['10'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['40'] = self::where('user_id','in',$userId)->where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['50'] = self::where('user_id','in',$userId)->where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['60'] = self::where('user_id','in',$userId)->where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['90'] = self::where('user_id','in',$userId)->where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[9]['91'] = $count[9]['10'] + $count[9]['40'];//成交额
        }else{
            //全部
            $count[0]['10'] = self::where($filter)->where('action',10)->sum('money');//充值
            $count[0]['40'] = self::where($filter)->where('action',40)->sum('money');//消费
            $count[0]['50'] = self::where($filter)->where('action',50)->sum('money');//分红
            $count[0]['60'] = self::where($filter)->where('action',60)->sum('money');//退款
            $count[0]['90'] = self::where($filter)->where('action',10)->sum('gift_money');//赠送
            $count[0]['91'] = $count[0]['10'] + $count[0]['40'];//成交额
            $count[0]['92'] = self::where($filter)->where(['action' => 70])->sum('money');//商户提现总额
            $count[0]['93'] = self::where($filter)->where(['action' => 70,'user_id' => $agent_id])->sum('money');//代理提现总额
            //今天
            $star = strtotime(date("Y-m-d"),time());
            $count[1]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->sum('money');
            $count[1]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->sum('money');
            $count[1]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->sum('money');
            $count[1]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->sum('money');
            $count[1]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->sum('gift_money');
            $count[1]['91'] = $count[1]['10'] + $count[1]['40'];//成交额
            //昨天
            $star = strtotime("-1 day");
            $end = strtotime(date("Y-m-d"),time());
            $count[2]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[2]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[2]['91'] = $count[2]['10'] + $count[2]['40'];//成交额
            //前天
            $star = strtotime("-2 day");
            $end = strtotime("-1 day");
            $count[3]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[3]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[3]['91'] = $count[3]['10'] + $count[3]['40'];//成交额
            //-4天
            $star = strtotime("-3 day");
            $end = strtotime("-2 day");
            $count[4]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[4]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[4]['91'] = $count[4]['10'] + $count[4]['40'];//成交额
            //-5天
            $star = strtotime("-4 day");
            $end = strtotime("-3 day");
            $count[5]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[5]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[5]['91'] = $count[5]['10'] + $count[5]['40'];//成交额
            //-6天
            $star = strtotime("-5 day");
            $end = strtotime("-4 day");
            $count[6]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[6]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[6]['91'] = $count[6]['10'] + $count[6]['40'];//成交额
            //-7天
            $star = strtotime("-6 day");
            $end = strtotime("-5 day");
            $count[7]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[7]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[7]['91'] = $count[7]['10'] + $count[7]['40'];//成交额
            
            //本月起至时间 - 月度统计 
            $end = mktime(0,0,0,date('m'),1,date('y')); 
            $count[8]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$end)->sum('money');
            $count[8]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$end)->sum('money');
            $count[8]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$end)->sum('money');
            $count[8]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$end)->sum('money');
            $count[8]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$end)->sum('gift_money');
            $count[8]['91'] = $count[8]['10'] + $count[8]['40'];//成交额
            
            //上月开始  
            $star = mktime(0,0,0,date('m')-1,1,date('y'));
            $count[9]['10'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['40'] = self::where($filter)->where('action',40)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['50'] = self::where($filter)->where('action',50)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['60'] = self::where($filter)->where('action',60)->where('create_time','>',$star)->where('create_time','<',$end)->sum('money');
            $count[9]['90'] = self::where($filter)->where('action',10)->where('create_time','>',$star)->where('create_time','<',$end)->sum('gift_money');
            $count[9]['91'] = $count[9]['10'] + $count[9]['40'];//成交额
        }
        return $count;
    }
}
