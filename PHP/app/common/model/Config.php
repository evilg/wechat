<?php
namespace app\common\model;

/**
 * 友情链接模型
 */
class Config extends BaseModel
{
    // 定义表名
    protected $name = 'config';

    // 定义主键
    protected $pk = 'config_id';
    
    protected $append = ['status'];
    
    /**
     * 通讯状态
     */
    public function getStatusAttr($value, $data)
    {
        $status = ['失败','正常'];
        if(!empty($data['component_verify_ticket']) AND !empty($data['component_access_token']) AND $data['expires_in'] > time()){
            $value = 1;
        }else{
            $value = 0;
        }
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取配置信息
     */
    public static function detail()
    {
        return self::withoutGlobalScope()->order('config_id','desc')->find();
    }

}
