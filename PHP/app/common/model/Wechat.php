<?php
namespace app\common\model;

use think\facade\Db;

/**
 * 公众号模型
 */
class Wechat extends BaseModel
{
    // 定义表名
    protected $name = 'wechat';

    // 定义主键
    protected $pk = 'wechat_id';

    // 追加字段
    protected $append = [];

    /**
     * 是否授权
     */
    public function getStatusAttr($value)
    {
        $status = ['否','是'];
        return ['text' => $status[$value], 'value' => $value];
    }

    /**
     * 获取详情
     */
    public static function detail()
    {
        $applet_id = self::$applet_id;
        // 筛选条件
        $filter = [];
        empty($applet_id) && $filter['applet_id'] = 0;
        return self::where($filter)->find();
    }

    /**
     * 获取详情
     */
    public static function getWechat(array $filter = [])
    {
        return self::withoutGlobalScope()->where($filter)->order('wechat_id','asc')->find();
    }

    /**
     * 切换授权 - 清除历史数据
     */
    public function clear($applet_id)
    {
        $filter['applet_id'] = $applet_id;
        // 开启事务
        Db::startTrans();
        try {
            //获取图文记录
            if($text = $this->withoutGlobalScope()->where($filter)->where('file_type',40)->select()){
                foreach ($text as $item) {
                    MaterialText::withoutGlobalScope()->where('text_no',$item['text_no'])->delete();
                }
            }
            Material::withoutGlobalScope()->where($filter)->delete();
            WechatBatchSend::withoutGlobalScope()->where($filter)->delete();
            Keyword::withoutGlobalScope()->where($filter)->delete();
            Wechat::withoutGlobalScope()->where($filter)->delete();
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

}
