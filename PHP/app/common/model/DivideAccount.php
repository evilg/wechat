<?php
namespace app\common\model;

/**
 * 分账账户关联模型
 */
class DivideAccount extends BaseModel
{
    // 定义表名
    protected $name = 'divide_account';

    // 定义主键
    protected $pk = 'divide_account_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联用户表
     */
    public function user()
    {
        return $this->hasOne('app\\common\\model\\User');
    }
    
    /**
     * 关联用户表
     */
    public function applet()
    {
        return $this->hasOne('app\\common\\model\\Applet');
    }
    
    /**
     * 获取列表
     */
    public function getList($agent_id = 0)
    {
        // 筛选条件
        $filter = [];
        $agent_id > 0 && $filter['agent_id'] = $agent_id;
        // 执行查询
        return $this->with(['user','applet'])
            ->order('divide_account_id','desc')
            ->where($filter)
            ->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    /**
     * 新增
    */ 
    public function add($data, $agent_id)
    {
        //验证该微信用户是否存在
        if(!$user = (new User)->where(['user_id' => $data['user_id'],'applet_id' => $data['applet_id']])->find()){
            $this->error = '该用户在当前微信小程序中不存在';
            return false;
        }
        $data['open_id'] = $user['open_id'];
        $data['agent_id'] = $agent_id;
        return $this->save($data);
    }

    /**
     * 删除
     */
    public function remove()
    {
        return $this->delete();
    }
}
