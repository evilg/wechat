<?php
namespace app\common\model;

/**
 * 模板代码模型
 */
class TemplateCode extends BaseModel
{
    // 定义表名
    protected $name = 'template_code';

    // 定义主键
    protected $pk = 'template_code_id';

    // 追加字段
    protected $append = [];

    /**
     * 关联模板市场表
     */
    public function template()
    {
        return $this->belongsTo('app\\common\\model\\Template','app_type','app_type');
    }

    /**
     * 获取列表
     */
    public function getList()
    {
        // 执行查询
        return $this->withoutGlobalScope()->order('template_code_id','desc')->paginate(['list_rows'=>15,'query' => request()->param()]);
    }

    
    /**
     * 获取最新模板
     */
    public static function getNew(string $app_type)
    {
        $filter = [
            'app_type' => $app_type
        ];
        return self::withoutGlobalScope()->where($filter)->order('template_code_id','desc')->find();
    }
}
