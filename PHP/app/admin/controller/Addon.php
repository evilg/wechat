<?php
namespace app\admin\controller;

use think\facade\View;
use hema\Addon as AddonModel;
use hema\device\Driver as Printer;
use think\addons\Service;
use think\facade\Cache;
use think\facade\Config;

/**
 * 插件控制器
 */
class Addon extends Controller
{
    /**
     * 获取列表
     */
    public function my()
    {   
        $addonuser = Cache::get('hemaphp',[]);
        $addon_list = get_addons_list(); //本地插件列表
        $model = new AddonModel;
        $list = [];
        foreach ($addon_list as $key=>$item){
            if($addon = $model->getAddonDetail($key)){
                $item['addon'] = $addon;
            }
            $list[] = $item;
        }
        return View::fetch('my', compact('list','addonuser'));
    }	
    
    /**
     * 获取列表
     */
    public function all($page = 1)
    {   
        $addonuser = Cache::get('hemaphp',[]);
        $model = new AddonModel;
        $result = $model->getAddonList('all',$page);
        $list = $result['list'];
        $page = str_replace("/api/addon/lists","/admin/addon/all",$result['page']);
        return View::fetch('index', compact('list','page','addonuser'));
    }
    
    /**
     * 获取列表
     */
    public function template($page = 1)
    {
        $addonuser = Cache::get('hemaphp',[]);
        $model = new AddonModel;
        $result = $model->getAddonList('template',$page);
        $list = $result['list'];
        $page = str_replace("/api/addon/lists","/admin/addon/template",$result['page']);
        return View::fetch('index', compact('list','page','addonuser'));
    }
    
    /**
     * 获取列表
     */
    public function block($page = 1)
    {
        $addonuser = Cache::get('hemaphp',[]);
        $model = new AddonModel;
        $result = $model->getAddonList('block',$page);
        $list = $result['list'];
        $page = str_replace("/api/addon/lists","/admin/addon/block",$result['page']);
        return View::fetch('index', compact('list','page','addonuser'));
    }
    
    /**
     * 登录信息
     */
    public function checkLogin()
    {
        $model = new AddonModel;
        if($model->checkLogin()){
            return $this->renderSuccess('已经登录');
        }
        return $this->renderError($model->getError());
    } 
    
    /**
     * 登录
     */
    public function login()
    {
        $model = new AddonModel;
        if($model->login($this->request->post('data'))){
            return $this->renderSuccess('登录成功');
        }
        return $this->renderError($model->getError());
    }
    
    /**
     * 退出登录
     */
    public function logout()
    {
        $model = new AddonModel;
        if($model->logout()){
            return $this->renderSuccess('退出成功');
        }
        return $this->renderError($model->getError());
    }
    
    /**
     * 安装
     */
    public function install()
    {
        if($this->user['user']['user_name'] == 'test'){
            return $this->renderError('体验用户无权操作！');
        }
        $data = $this->request->post();
        $extend = [
            'type' => 'install',
            'token' => '',
        ];
        if($addonuser = Cache::get('hemaphp')){
            $extend['token'] = $addonuser['token'];
        }
        $result = Service::install($data['name'], $extend);
        if(is_array($result)){
            $code = 0;
            $data = [];
            if(isset($result['data'])){
                $data = $result['data'];
            }
            if(isset($result['code'])){
                $result['code'] < 0 && $code = $result['code'];
            }
            return $this->renderError($result['msg'],'',$data,$code);
        }
        return $this->renderSuccess('安装成功');
    }
    
    /**
     * 升级
     */
    public function upgrade()
    {
        if($this->user['user']['user_name'] == 'test'){
            return $this->renderError('体验用户无权操作！');
        }
        $data = $this->request->post();
        $info = get_addons_info($data['name']);
        if($info['status'] == 1){
            return $this->renderError('禁用插件后才能操作！'); 
        }
        $extend = [
            'version' => $info['version'],
            'type' => 'upgrade',
            'token' => '',
        ];
        if($addonuser = Cache::get('hemaphp')){
            $extend['token'] = $addonuser['token'];
        }
        $result = Service::upgrade($data['name'], $extend);
        if(is_array($result)){
            $code = 0;
            $data = [];
            if(isset($result['data'])){
                $data = $result['data'];
            }
            if(isset($result['code'])){
                $result['code'] < 0 && $code = $result['code'];
            }
            return $this->renderError($result['msg'],'',$data,$code);
        }
        return $this->renderSuccess('升级成功');
    }
    
    /**
     * 卸载
     */
    public function uninstall()
    {
        if($this->user['user']['user_name'] == 'test'){
            return $this->renderError('体验用户无权操作！');
        }
        $data = $this->request->post();
        $addon = get_addons_info($data['name']);
        if($addon['status'] == 1){
            return $this->renderError('禁用插件后才能操作！'); 
        }
        $result = Service::uninstall($data['name']);
        if(is_array($result)){
            $code = 0;
            $data = [];
            if(isset($result['data'])){
                $data = $result['data'];
            }
            if(isset($result['code'])){
                $result['code'] < 0 && $code = $result['code'];
            }
            return $this->renderError($result['msg'],'',$data,$code);
        }
        return $this->renderSuccess('卸载成功');
    }
    
    /**
     * 状态
     */
    public function status()
    {
        if($this->user['user']['user_name'] == 'test'){
            return $this->renderError('体验用户无权操作！');
        }
        $data = $this->request->post();
        if($data['status'] == 1){
            $action = 'disable';//禁用
        }else{
            $action = 'enable';//启用
        }
        $result = Service::$action($data['name']);
        if(is_array($result)){
            $code = 0;
            $data = [];
            if(isset($result['data'])){
                $data = $result['data'];
            }
            if(isset($result['code'])){
                $result['code'] < 0 && $code = $result['code'];
            }
            return $this->renderError($result['msg'],'',$data,$code);
        }
        return $this->renderSuccess('操作成功');
    }
    
    /**
     * 配置
     */
    public function config($name)
    {
        if($this->user['user']['user_name'] == 'test'){
            return $this->renderError('体验用户无权操作！');
        }
        if ($this->request->isGet()) {
            if($config = get_addons_config($name,true)){
               return $this->renderSuccess('','',$config); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        $data = $this->request->post('data');
        if($name == 'yilianyun'){
            //易联云获取token
            if((!empty($data['app_key'])) AND (!empty($data['app_secret']))){
                $dev = new Printer('yilianyun');
                $data['access_token'] = $dev->getToken($data);
            }
        }
        if(set_addons_config($name,$data)){
            return $this->renderSuccess('配置成功');
        }
        return $this->renderError('配置失败');
    }
    
    /**
     * 购买插件
     */
    public function pay($addon_id)
    {
        $user = Cache::get('hemaphp',[]);
        $domain = $this->request->host();
        $url = Config::get('app.hemaphp.api_url') . '/user/order/pay?addon_id=' . $addon_id . '&user_id=' . $user['user_id'] . '&domain=' . $domain;
        return redirect($url);
    }
    
    /**
     * 框架升级
     */
    public function hemaphpUpgrade()
    {
        $result = Service::hemaphp();
        if(is_array($result)){
            $code = 0;
            $data = [];
            if(isset($result['data'])){
                $data = $result['data'];
            }
            if(isset($result['code'])){
                $result['code'] < 0 && $code = $result['code'];
            }
            return $this->renderError($result['msg'],'',$data,$code);
        }
        return $this->renderSuccess('升级成功');
    }
}
