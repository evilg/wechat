<?php
namespace app\admin\controller;

use app\admin\model\Config as ConfigModel;
use app\admin\model\Setting as SettingModel;
use think\facade\View;

/**
 * 开放平台对接
 */
class Open extends Controller
{
    /**
     * 微信开放平台第三方平台设置
     */
    public function wxopen()
    {
        $model = ConfigModel::detail();
        if ($this->request->isAjax()) {
            if ($model->edit($this->postData('data'))){
				return $this->renderSuccess('更新成功');
			}
            return $this->renderError('更新失败');
        }
        return View::fetch('wxopen', compact('model'));
    }
	
	/**
     * 微信开放平台网站应用设置
     */
    public function wxweb()
    {
        return $this->updateEvent('wxweb');
    }
	
	/**
     * 配送开放平台设置
     */
    public function webdelivery()
    {
        return $this->updateEvent('webdelivery');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem($key);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if ($model->edit($key,$this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
