<?php
namespace app\admin\controller\applet;

use app\admin\controller\Controller;
use app\admin\model\Template as TemplateModel;
use app\admin\model\TemplateCode as TemplateCodeModel;
use think\facade\View;

/**
 * 推送模板管理
 */
class Code extends Controller
{
	/**
     * 模板列表
     */
    public function index()
    {
        $model = new TemplateCodeModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = TemplateCodeModel::get($id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isGet()) {
            $tpl = new TemplateModel;
            $list = $tpl->getList();
            return $this->renderSuccess('', '', compact('list')); 
        }
        $model = new TemplateCodeModel;
        // 新增记录
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功', url('applet.code/index'));
        }
        return $this->renderError('添加失败');
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        //详情
        $model = TemplateCodeModel::get($id);
        if ($this->request->isGet()) {
            $tpl = new TemplateModel;
			$list = $tpl->getList();
            return $this->renderSuccess('', '', compact('model','list'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('applet.code/index'));
        }
        return $this->renderError('操作失败');
    }
}
