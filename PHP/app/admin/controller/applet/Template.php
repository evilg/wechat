<?php
namespace app\admin\controller\applet;

use app\admin\controller\Controller;
use app\admin\model\Template as TemplateModel;
use think\facade\View;

/**
 * 模板市场控制器
 */
class Template extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new TemplateModel;
		$list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($id)
    {
        $model = TemplateModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }
    
     /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new TemplateModel;
            // 新增记录
            if ($model->add($this->postData('data'))) {
                return $this->renderSuccess('添加成功', url('applet.template/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('applet.template/index'));
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = TemplateModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('applet.template/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
