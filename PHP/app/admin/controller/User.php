<?php
namespace app\admin\controller;

use app\admin\model\User as UserModel;
use app\admin\model\UserDetail as UserDetailModel;
use app\admin\model\Record as RecordModel;
use think\facade\View;
use think\facade\Session;

/**
 * 商户管理控制器
 */
class User extends Controller
{
    /**
     * 支付记录
     */
    public function pay()
    {
        $model = new RecordModel;
        $list = $model->getList(20);
        return View::fetch('pay', compact('list'));
    }

    /**
     * 会员列表
     */
    public function user()
    {
		return $this->lists(10,'商家会员');
    }

    /**
     * 商户列表
     */
    public function store()
    {
		return $this->lists(20,'商家用户');
    }
  
    /**
     * 代理列表
     */
    public function agent()
    {
        return $this->lists(30,'代理用户');
    }

    /**
     * 更新设置事件
     */
    private function lists($status, string $title)
    {
        $model = new UserModel;
		$list = $model->getList($status);
		return View::fetch('index', compact('list','title','status'));
    }

    /**
     * 用户资料
     */
    public function detail()
    {
		$model = new UserDetailModel;
		$list = $model->getList();
		return View::fetch('detail', compact('list'));
    }
	
	/**
     * 一键登录
     */
    public function oneKey($id)
    {
		$model = UserModel::get($id);
		if($url = $model->oneKey()){
			return redirect($url);
		}
		$error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }

    /**
     * 设置为代理
     */
    public function status($id)
    {
        $model = UserModel::get($id);
        if($model['status']['value'] == 10){
            return $this->renderError('商家的会员不可设置为代理');
        }
        if($model->agentAction()){
            return $this->renderSuccess('操作成功');
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }
}
