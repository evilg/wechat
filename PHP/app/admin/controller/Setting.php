<?php
namespace app\admin\controller;

use app\admin\model\Setting as SettingModel;
use think\facade\View;

/**
 * 站点配置
 */
class Setting extends Controller
{
	/**
     * 站点设置
     */
    public function web()
    {
        return $this->updateEvent('web');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem($key,0);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if ($model->edit($key,$this->postData('data'))) {
            return $this->renderSuccess('更新成功');
        }
        return $this->renderError('更新失败');
    }

}
