<?php
namespace app\admin\controller\setting;

use app\admin\controller\Controller;
use app\admin\model\Link as LinkModel;
use think\facade\View;

/**
 * 友情链接
 */
class Link extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$model = new LinkModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = LinkModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $model = new LinkModel;
            // 新增记录
            if ($model->add($this->postData('data'))) {
                return $this->renderSuccess('添加成功', url('setting.link/index'));
            }
            $error = $model->getError() ?: '添加失败';
            return $this->renderError($error);
        }
        return redirect(url('setting.link/index'));
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = LinkModel::get($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('操作成功', url('setting.link/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
