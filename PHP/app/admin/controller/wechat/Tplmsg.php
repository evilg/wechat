<?php
namespace app\admin\controller\wechat;

use app\admin\controller\Controller;
use app\admin\model\Setting as SettingModel;
use think\facade\View;

/**
 * 公众号模板消息
 */
class Tplmsg extends Controller
{
   /**
    * 更新设置事件
    */
   public function setting()
   {
  	   if (!$this->request->isAjax()) {
  		   $model = SettingModel::getItem('webtplmsg',0);
  		   return View::fetch('setting', compact('model'));
  	   }
       $model = new SettingModel;
       if ($model->edit('webtplmsg',$this->postData('data'))) {
           return $this->renderSuccess('更新成功');
       }
       return $this->renderError('更新失败');
   }
}
