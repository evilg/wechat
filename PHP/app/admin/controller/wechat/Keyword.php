<?php
namespace app\admin\controller\wechat;

use app\admin\controller\Controller;
use app\admin\model\Keyword as KeywordModel;
use think\facade\View;

/**
 * 关键字回复控制器
 */
class Keyword extends Controller
{
	/**
     * 首页
     */
    public function index()
    {
        $model = new KeywordModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }
	
	/**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            return View::fetch('add');
        }
        $model = new KeywordModel;
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功', url('wechat.keyword/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = KeywordModel::get($id);
        if (!$model->remove()) {
            return $this->renderError('删除失败');
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = KeywordModel::get($id);
        if (!$this->request->isAjax()) {
            return View::fetch('edit', compact('model'));
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('wechat.keyword/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 状态编辑
     */
    public function status($id)
    {
        $model = KeywordModel::get($id);
        // 更新记录
        if ($model->status()) {
            return $this->renderSuccess('更新成功', url('wechat.keyword/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
