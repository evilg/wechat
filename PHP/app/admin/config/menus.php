<?php
return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'addon' => [
        'name' => '插件管理',
        'icon' => 'iconyingyongzhongxin',
        'index' => 'addon/my',
        'submenu' => [
            [
                'name' => '我的插件',
                'index' => 'addon/my'
            ],
            [
                'name' => '插件市场',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '全部插件',
                        'index' => 'addon/all'
                    ],
                    [
                        'name' => '行业模板',
                        'index' => 'addon/template'
                    ],
                    [
                        'name' => '功能模块',
                        'index' => 'addon/block'
                    ],
                ]
            ],
        ]
    ],
    'apply' => [
        'name' => '用户申请',
        'icon' => 'iconrenzheng',
        'index' => 'apply/applet',
        'submenu' => [
            [
                'name' => '小程序申请',
                'index' => 'apply/applet'
            ],
            [
                'name' => '平台入驻',
                'index' => 'apply/out'
            ],
            [
                'name' => '支付申请',
                'index' => 'apply/pay'
            ],
            [
                'name' => '代理认证',
                'index' => 'apply/agent'
            ],
            [
                'name' => '实名认证',
                'index' => 'apply/auth'
            ],
        ]
    ],
    'applet' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'applet/all',
        'submenu' => [
            [
                'name' => '小程序管理',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '全部',
                        'index' => 'applet/all'
                    ],
                    [
                        'name' => '已授权',
                        'index' => 'applet/normal'
                    ],
                    [
                        'name' => '已到期',
                        'index' => 'applet/ends'
                    ],
                ]
            ],
            [
                'name' => '上线模板',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '草稿库',
                        'index' => 'applet.wxtpl/index'
                    ],
                    [
                        'name' => '模板库',
                        'index' => 'applet.wxtpl/lists'
                    ],
                    [
                        'name' => '推送模板',
                        'index' => 'applet.code/index'
                    ],
                ]
            ],
            [
                'name' => '模板市场',
                'index' => 'applet.template/index'
            ],
        ],
    ],
    'wechat' => [
        'name' => '公众号',
        'icon' => 'iconweixingongzhonghao',
        'color' => '#36b313',
        'index' => 'wechat/index',
        'submenu' => [
            [
                'name' => '基础信息',
                'index' => 'wechat/index'
            ],
            [
                'name' => '菜单设置',
                'index' => 'wechat/menus'
            ],
            [
                'name' => '模板消息',
                'index' => 'wechat.tplmsg/setting'
            ],
            [
                'name' => '群发消息',
                'index' => 'wechat.send/index',
                'uris' => [
                    'wechat.send/index',
                    'wechat.send/add',
                    'wechat.send/edit',
                    'wechat.send/delete'
                ],
            ],
            [
                'name' => '素材管理',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '图文素材',
                        'index' => 'wechat.material.text/index',
                        'urls' => [
                            'wechat.material.text/index',
                            'wechat.material.text/add',
                            'wechat.material.text/edit',
                            'wechat.material.text/delete'
                        ]
                    ],
                    [
                        'name' => '图片素材',
                        'index' => 'wechat.material.image/index',
                        'urls' => [
                            'wechat.material.image/index',
                            'wechat.material.image/add',
                            'wechat.material.image/edit',
                            'wechat.material.image/delete'
                        ]
                    ],  
                    [
                        'name' => '语音素材',
                        'index' => 'wechat.material.voice/index',
                        'urls' => [
                            'wechat.material.voice/index',
                            'wechat.material.voice/add',
                            'wechat.material.voice/edit',
                            'wechat.material.voice/delete'
                        ]
                    ],
                    [
                        'name' => '视频素材',
                        'index' => 'wechat.material.video/index',
                        'urls' => [
                            'wechat.material.video/index',
                            'wechat.material.video/add',
                            'wechat.material.video/edit',
                            'wechat.material.video/delete'
                        ]
                    ],
                ]
            ],
            [
                'name' => '智能回复',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '被关注回复',
                        'index' => 'wechat/subscribe'
                    ],
                    [
                        'name' => '关键字回复',
                        'index' => 'wechat.keyword/index',
                        'uris' => [
                            'wechat.keyword/index',
                            'wechat.keyword/add',
                            'wechat.keyword/edit',
                            'wechat.keyword/delete',
                        ],
                    ],
                    
                ]
            ],
        ],
    ],
    'pay' => [
        'name' => '支付设置',
        'icon' => 'iconcaiwuguanli',
        'color' => '#36b313',
        'index' => 'pay/webpay',
        'submenu' => [
            [
                'name' => '站点支付',
                'index' => 'pay/webpay'
            ],
            [
                'name' => '支付服务商',
                'index' => 'pay/wxpayisp'
            ],
        ]
    ],
    'user' => [
        'name' => '用户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/store',
        'submenu' => [
            [
                'name' => '商家用户',
                'index' => 'user/store'
            ],
            [
                'name' => '代理用户',
                'index' => 'user/agent'
            ],
            [
                'name' => '商家会员',
                'index' => 'user/user'
            ],
            [
                'name' => '交易记录',
                'index' => 'user/pay'
            ],
        ]
    ],
    'open' => [
        'name' => '开放平台',
        'icon' => 'iconkaifangpingtai',
        'index' => 'open/wxopen',
        'submenu' => [
            [
                'name' => '微信平台',
                'active' => true,
                'submenu' => [
                    [
                        'name' => '第三方应用',
                        'index' => 'open/wxopen'
                    ],
                    [
                        'name' => '网站应用',
                        'index' => 'open/wxweb'
                    ],
                ]
            ],
        ]
    ],  
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting/web',
        'submenu' => [
            [
                'name' => '站点设置',
                'index' => 'setting/web'
            ],
            [
                'name' => '友情链接',
                'index' => 'setting.link/index'
            ],
            [
                'name' => '其他',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '清理缓存',
                        'index' => 'setting.cache/clear'
                    ],
                    [
                        'name' => '环境监测',
                        'index' => 'setting.science/index'
                    ]
                ]
            ],
        ],
    ],
];
