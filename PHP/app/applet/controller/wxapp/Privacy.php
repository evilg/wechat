<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use think\facade\View;
use hema\wechat\Driver;

/**
 * 小程序隐私保护管理
 */
class Privacy extends Controller
{
    /**
     * 小程序隐私保护管理
     */
    public function setting()
    {
        $wx = new Driver;
        if (!$this->request->isAjax()) {
            if($model = $wx->getPrivacySetting($this->applet_id)){
                $model['user_info'] = 0;
                $model['location'] = 0;
                $model['address'] = 0;
                $model['phone'] = 0; 
                foreach ($model['setting_list'] as $vo){
                    if($vo['privacy_key'] == 'UserInfo'){
                        $model['user_info'] = 1;
                    }
                    if($vo['privacy_key'] == 'Location'){
                        $model['location'] = 1;
                    }
                    if($vo['privacy_key'] == 'Address'){
                        $model['address'] = 1;
                    }
                    if($vo['privacy_key'] == 'PhoneNumber'){
                        $model['phone'] = 1;
                    }
                }
            }else{
                $model['errmsg'] = $wx->getError();
            }
            return View::fetch('setting', compact('model'));
        }
		if($result = $wx->setPrivacySetting($this->applet_id,$this->postData('data'))){
		     return $this->renderSuccess('设置成功', url('wxapp.privacy/setting'));
        }
        $error = $wx->getError() ?: '设置失败';
        return $this->renderError($error);
    }
}
