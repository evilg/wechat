<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 小程序服务类目管理
 */
class Category extends Controller
{
    /**
     * 类目列表
     */
    public function index()
    {
		$wx = new Driver;
        $list = $wx->getCategory($this->applet_id);
		return View::fetch('index', compact('list'));	
    }

    /**
     * 添加
     */
    public function add()
    {
    	// 获取类目
        $wx = new Driver;
        $list = $wx->getAllCategories($this->applet_id);
        if (!$this->request->isAjax()) {
            return View::fetch('add', compact('list'));
        }
        $data = $this->postData('data');
        $id = explode('.',$data['id']);
		$list = $list[$id[0]]['children'][$id[1]];
		$media_id = '';
		$ca_name = '';
		if($list['sensitive_type']==1){
			if (!isset($data['certicates']) || empty($data['certicates'])) {
				return $this->renderError('请上传资质图片');
			}
			$ca_name = $list['qualify']['exter_list'][0]['inner_list'][0]['name'];
			if(!$media_id = $wx->upTempMaterial($data['certicates'],$this->applet_id)){
				return $this->renderError($wx->getError());
			}
		}

		if(!$wx->addCategory($this->applet_id,$list['father'],$list['id'],$ca_name,$media_id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('添加成功',url('wxapp.category/index'));
    }

    /**
     * 删除
     */
    public function delete(string $id)
    {
    	$data = explode(',',$id);
    	$wx = new Driver;
    	if(!$wx->deleteCategory($this->applet_id,$data[0],$data[1])){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('删除成功');
    }

}
