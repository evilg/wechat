<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use app\applet\model\AppletName as AppletNameModel;
use app\applet\model\Applet as AppletModel;
use think\facade\View;
use hema\wechat\Driver;

/**
 * 设置小程序呢称
 */
class Name extends Controller
{
    /**
     * 设置
     */
    public function setting()
    {
    	$name = AppletNameModel::where('applet_id',$this->applet_id)->order('applet_name_id','desc')->find();
        $model = AppletModel::get($this->applet_id);
        if (!$this->request->isAjax()) {
            return View::fetch('setting', compact('model','name'));
        }
		$model = new AppletNameModel;
		if ($model->action($this->postData('data'),$this->applet_id)) {
			return $this->renderSuccess('更新成功', url('wxapp/index'));
		}
		$error = $model->getError() ?: '更新失败';
		return $this->renderError($error);
    }

    /**
     * 检测名称
     */
    public function checkName($nick_name)
    {
        $wx = new Driver;
        if($result = $wx->checkWxVerifyNickName($this->applet_id,$nick_name)){
             if($result['hit_condition']){
                return $this->renderError('命中关键字：'.$result['wording']);
             }
            return $this->renderError('可以使用');
        }
        return $this->renderError($wx->getError());
    }
}
