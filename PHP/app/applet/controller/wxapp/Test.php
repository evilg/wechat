<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 体验用户管理
 */
class Test extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$wx = new Driver;
        $list = $wx->getTestUser($this->applet_id);
		return View::fetch('index', compact('list'));	
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $data = $this->postData('data');
            $wx = new Driver;
    		if(!$wx->addTestUser($this->applet_id,$data['wechatid'])){
    			return $this->renderError($wx->getError());
    		}
    		return $this->renderSuccess('添加成功',url('wxapp.test/index'));
        }
        return redirect(url('wxapp.test//index'));
    }

    /**
     * 删除
     */
    public function delete(string $id)
    {
    	$wx = new Driver;
    	if(!$wx->delTestUser($this->applet_id,$id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('删除成功');
    }

}
