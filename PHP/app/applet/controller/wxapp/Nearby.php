<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 附近的小程序管理
 */
class Nearby extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
		$wx = new Driver;
        $list = $wx->getNearbyPoi($this->applet_id);
        if(isset($list['data']['data'])){
            $list['data']['data'] = hema_json($list['data']['data'],true);
        }
		return View::fetch('index', compact('list'));	
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            $category = []; //门店列表
            return View::fetch('add', compact('category'));
        }
        $data = $this->postData('data');
        if(!isset($data['shop_id'])){
            $shop_id = $this->shop_id;
        }else{
            $shop_id = $data['shop_id'];
            unset($data['shop_id']);
        }
        $shop = ShopModel::get($shop_id);
         $data['store_name'] = $shop['shop_name'];
         $data['hour'] = $shop['shop_hours'];
         $data['contract_phone'] = $shop['phone'];
         $data['address'] = $shop['province'] . $shop['city'] . $shop['district'] . $shop['address'];
         $data['map_poi_id'] = $shop['poi_id'];
        $data['service_infos'] = [
            'service_infos' => [
                ['id' => 1,'type' => 1,'name' => '外送','appid' => $this->user['applet']['app_id'],'path' => '/pages/index/index'],
                ['id' => 6,'type' => 1,'name' => '点餐','appid' => $this->user['applet']['app_id'],'path' => '/pages/index/index'],
                ['id' => 4,'type' => 1,'name' => '预约','appid' => $this->user['applet']['app_id'],'path' => '/pages/index/index'],
                ['id' => 14,'type' => 1,'name' => '到店自提','appid' => $this->user['applet']['app_id'],'path' => '/pages/index/index']
            ]
        ];
        $data['service_infos'] = hema_json($data['service_infos']);
        $wx = new Driver;
        //上传临时素材
        if(!$result = $wx->upTempMaterial($data['qualification_list'],$this->applet_id)){
            return $this->renderError($wx->getError());
        }
        $data['qualification_list'] = $result;
        $shop_pic['list'] = [];
        foreach ($data['pic_list'] as $value) {
            //上传永久素材
            if(!$result = $wx->upMediaUrl($value,$this->applet_id)){
                return $this->renderError($wx->getError());
            }
            $shop_pic['list'][] = $result;
        }
        $data['pic_list'] = json_encode($shop_pic);
		if(!$wx->addNearbyPoi($data, $this->applet_id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('添加成功',url('wxapp.nearby/index'));
    }

    /**
     * 删除
     */
    public function delete(string $id)
    {
    	$wx = new Driver;
    	if(!$wx->delNearbyPoi($this->applet_id,$id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('删除成功');
    }
    
    /**
     * 展示状态切换
     */
    public function status($id,$status)
    {
        if($status == 1){
            $status = 0;
        }else{
            $status = 1;
        }
    	$wx = new Driver;
    	if(!$wx->setNearbyPoiShowStatus($this->applet_id,$id,$status)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('删除成功');
    }

}
