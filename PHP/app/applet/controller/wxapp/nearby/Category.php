<?php
namespace app\applet\controller\wxapp\nearby;

use app\applet\controller\Controller;
use hema\wechat\Driver;
use think\facade\View;

/**
 * 地点类目管理
 */
class Category extends Controller
{
    /**
     * 类目列表
     */
    public function index()
    {
		$wx = new Driver;
        $list = $wx->getStoreWxaAttr($this->applet_id);
		return View::fetch('index', compact('list'));	
    }

    /**
     * 添加
     */
    public function add()
    {
    	// 获取类目
        $wx = new Driver;
        $list = $wx->getMerchantCategory($this->applet_id);
        if (!$this->request->isAjax()) {
            return View::fetch('add', compact('list'));
        }
        $data = $this->postData('data');
        $id = explode('.',$data['id']);
		$new_data['category_first_id'] = $list[$id[0]]['id'];
        $list = $list[$id[0]]['children'];
        $new_data['category_second_id'] = $list[$id[1]]['id'];
		if($list[$id[1]]['sensitive_type']==1){
			if (!isset($data['certicates']) || empty($data['certicates'])) {
				return $this->renderError('请上传资质图片');
			}
			if(!$media_id = $wx->upTempMaterial($data['certicates'],$this->applet_id)){
				return $this->renderError($wx->getError());
			}
            $new_data['media_list'][0] = $media_id;
		}
		if(!$wx->nearbyApplyCategory($new_data,$this->applet_id)){
			return $this->renderError($wx->getError());
		}
		return $this->renderSuccess('添加成功',url('wxapp.nearby.category/index'));
    }

}
