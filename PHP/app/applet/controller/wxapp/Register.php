<?php
namespace app\applet\controller\wxapp;

use app\applet\controller\Controller;
use app\applet\model\Apply as ApplyModel;
use app\applet\model\UserDetail as UserDetailModel;
use think\facade\View;

/**
 * 用户注册申请
 */
class Register extends Controller
{
    /**
     * 注册小程序
     */
    public function wxapp()
    {	
    	$model = ApplyModel::getApply([
    		'apply_mode' => 10,
    		'applet_id' => $this->applet_id,
    		'user_id' => $this->user['user']['user_id']
    	]);
		if(!$this->request->isAjax()) {
			if(!$model){
				$model['details'] = UserDetailModel::getUserDetail($this->user['user']['user_id']);
			}
			return View::fetch('wxapp', compact('model'));
        }
        //提交动作
		$data = $this->postData('data');	
		if(!isset($data['details']['license_copy']) OR empty($data['details']['license_copy'])){
			return $this->renderError('请上传营业执照');  
		}
		if(!isset($data['details']['id_card_copy']) OR empty($data['details']['id_card_copy'])){
			return $this->renderError('请上传身份证（正面）');  
		}
		if(!isset($data['details']['id_card_national']) OR empty($data['details']['id_card_national'])){
			return $this->renderError('请上传身份证（反面）');  
		}
		$data['apply_status'] = 10;
		if(!$model){
			$model = new ApplyModel;
			$data['user_id'] = $this->user['user']['user_id'];
			$data['apply_mode'] = 10;
			$data['applet_id'] = $this->applet_id;
		}
        if ($model->action($data)) {
			return $this->renderSuccess('提交成功，等待审核', url('wxapp/index'));
        }
        $error = $model->getError() ?: '提交失败';
        return $this->renderError($error);      
    }

    /**
     * 注册支付商户号
     */
    public function wxpay()
    {	
    	$model = ApplyModel::getApply([
    		'apply_mode' => 30,
    		'applet_id' => $this->applet_id,
    		'user_id' => $this->user['user']['user_id']
    	]);
        if(!$this->request->isAjax()) {
			if(!$model){
				$model['details'] = UserDetailModel::getUserDetail($this->user['user']['user_id']);
			}
			return View::fetch('wxpay', compact('model'));
        }
        //提交动作
		$data = $this->postData('data');
		if(empty($this->user['applet']['app_id'])){
			return $this->renderError('还未绑定小程序');
		}	
		$data['app_id'] = $this->user['applet']['app_id'];
		if(!isset($data['details']['license_copy']) OR empty($data['details']['license_copy'])){
			return $this->renderError('请上传营业执照');  
		}
		if(!isset($data['details']['id_card_copy']) OR empty($data['details']['id_card_copy'])){
			return $this->renderError('请上传身份证（正面）');  
		}
		if(!isset($data['details']['id_card_national']) OR empty($data['details']['id_card_national'])){
			return $this->renderError('请上传身份证（反面）');  
		}
		$data['apply_status'] = 10;
		if(!$model){
			$model = new ApplyModel;
			$data['user_id'] = $this->user['user']['user_id'];
			$data['apply_mode'] = 30;
			$data['applet_id'] = $this->applet_id;
		}
		if ($model->action($data)) {
			return $this->renderSuccess('提交成功，等待审核', '/user/apply/index');
        }
        $error = $model->getError() ?: '提交失败';
        return $this->renderError($error);    
    }

}
