<?php
namespace app\applet\controller;

use app\applet\model\Setting as SettingModel;
use think\facade\View;

/**
 * 支付设置设置
 */
class Payment extends Controller
{	
	/**
	 * 微信支付设置
     */
    public function wxpay()
    {
        return $this->updateEvent('wxpay');
    }
    
    /**
     * 更新设置事件
     */
    private function updateEvent(string $key)
    {
        if (!$this->request->isAjax()) {
			$model = SettingModel::getItem($key,$this->applet_id);
            return View::fetch($key, compact('model'));
        }
        $model = new SettingModel;
        if($model->edit($key, $this->postData('data'),$this->applet_id)) {
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

}
