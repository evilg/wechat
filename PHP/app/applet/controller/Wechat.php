<?php
namespace app\applet\controller;

use app\applet\model\Wechat as WechatModel;
use app\applet\model\Setting as SettingModel;
use think\facade\View;
use hema\wechat\Driver;

/**
 * 公众号管理
 */
class Wechat extends Controller
{
    /**
     * 公众号信息
     */
    public function index()
    {
        $wx = new Driver;
        $url = $wx->authUrl($this->applet_id,1);
        $wechat = WechatModel::detail();
        return View::fetch('index', compact('wechat','url'));
    }
	
	/**
     * 被关注回复设置
     */
    public function subscribe()
    {
        return $this->updateEvent('subscribe');
    }
	
	/**
     * 公众号菜单设置
     */
    public function menus()
    {
        return $this->updateEvent('menus');
    }

    /**
     * 更新设置事件
     */
    private function updateEvent($key)
    {
        if (!$this->request->isAjax()) {
            $model = SettingModel::getItem($key,0);
        	if($key == 'menus'){
				$model = json_encode($model);
        	}
            return View::fetch($key, compact('model'));
        }
        if($this->user['user']['user_name'] == 'test'){
            return $this->renderError('体验用户无权操作！');
        }
		if(!$wechat = WechatModel::detail()){
            return $this->renderError('还未绑定公众号');
        }
        if($wechat['status']['value'] == 0){
            return $this->renderError('还未绑定公众号');
        }
        $model = new SettingModel;
        if ($model->edit($key,$this->postData('data'))){
            return $this->renderSuccess('更新成功');
        }
        $error = $model->getError() ?: '更新失败';
		return $this->renderError($error);
    }
}
