<?php
namespace app\applet\controller\wechat;

use app\applet\controller\Controller;
use app\applet\model\WechatBatchSend as WechatBatchSendModel;
use app\applet\model\Wechat as WechatModel;
use think\facade\View;

/**
 * 微信群发消息控制器
 */
class Send extends Controller
{
	/**
     * 首页
     */
    public function index()
    {
        $model = new WechatBatchSendModel;
        $list = $model->getList();
        return View::fetch('index', compact('list'));
    }
	
	/**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            return View::fetch('add');
        }
		if(!$wechat = WechatModel::detail()){
            return $this->renderError('还未绑定公众号');
        }
        if($wechat['status']['value'] == 0){
            return $this->renderError('还未绑定公众号');
        }
        $model = new WechatBatchSendModel;
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功，预览消息已发送到管理员微信', url('wechat.send/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = WechatBatchSendModel::get($id);
        if ($model->remove()) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $model = WechatBatchSendModel::get($id);
        if (!$this->request->isAjax()) {
            return View::fetch('edit', compact('model'));
        }
		if(!$wechat = WechatModel::detail()){
            return $this->renderError('还未绑定公众号');
        }
        if($wechat['status']['value'] == 0){
            return $this->renderError('还未绑定公众号');
        }
        // 更新记录
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('wechat.send/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }
	
	/**
     * 群发
     */
    public function status($id)
    {
        $model = WechatBatchSendModel::get($id);
        if ($model->send()){
            return $this->renderSuccess('群发成功');
        }
        $error = $model->getError() ?: '群发失败';
        return $this->renderError($error);
    }

}
