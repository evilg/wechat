<?php
namespace app\applet\controller\wechat;

use app\applet\controller\Controller;
use app\applet\model\Setting as SettingModel;
use think\facade\View;

/**
 * 公众号模板消息
 */
class Tplmsg extends Controller
{
   /**
    * 更新设置事件
    */
   public function setting()
   {
  	   if (!$this->request->isAjax()) {
  		   $model = SettingModel::getItem('webtplmsg');
  		   return View::fetch('setting', compact('model'));
  	   }
       $model = new SettingModel;
       if ($model->edit('webtplmsg',$this->postData('data'))) {
           return $this->renderSuccess('更新成功');
       }
       return $this->renderError('更新失败');
   }
}
