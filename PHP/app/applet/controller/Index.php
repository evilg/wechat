<?php
namespace app\applet\controller;

use think\facade\View;

/**
 * 首页
 */
class Index extends Controller
{
	
    public function index()
    {
        return View::fetch();
    }
}
