<?php
namespace app\applet\controller;

use app\applet\model\Applet as AppletModel;
use think\facade\View;

/**
 * 用户管理
 */
class Setting extends Controller
{
	/**
     * 修改管理员密码
     */
    public function renew()
    {
        $model = AppletModel::get($this->applet_id);
        if (!$this->request->isAjax()) {
            return View::fetch('renew',compact('model'));
        }
        if ($model->renew($this->postData('data'))) {
            return $this->renderSuccess('修改成功');
        }
        return $this->renderError($model->getError() ?: '修改失败');
    }
}
