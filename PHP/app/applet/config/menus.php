<?php

return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'wxapp' => [
        'name' => '微信小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'wxapp/index',
        'submenu' => [
            [
                'name' => '小程序设置',
                'index' => 'wxapp/index',
				'urls' => [
					'wxapp/index',
					'wxapp.name/setting',
					'wxapp/sethead',
				]
            ],
			[
				'name' => '隐私设置',
				'index' => 'wxapp.privacy/setting',
			],
			[
				'name' => '服务类目',
				'index' => 'wxapp.category/index',
				'uris' => [
					'wxapp.category/index',
					'wxapp.category/add',
					'wxapp.category/delete',
				],
			],
			[
				'name' => '发布/升级',
				'index' => 'wxapp.release/index',
					'urls' => [
						'wxapp.release/index',
						'wxapp.release/add',
						'wxapp.release/edit',
						'wxapp.release/delete'
					]
			],
			[
				'name' => '体验用户',
				'index' => 'wxapp.test/index',
					'urls' => [
						'wxapp.test/index',
						'wxapp.test/add',
						'wxapp.test/delete'
					]
			],
            [
                'name' => '附近小程序',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '地点类目',
                        'index' => 'wxapp.nearby.category/index',
                        'uris' => [
							'wxapp.nearby.category/index',
							'wxapp.nearby.category/add',
						],
                    ],
                    [
                        'name' => '地点列表',
                        'index' => 'wxapp.nearby/index',
                        'uris' => [
							'wxapp.nearby/index',
							'wxapp.nearby/add',
							'wxapp.nearby/delete',
						],
                    ],
                ]
            ],
        ],
    ],
    'wechat' => [
        'name' => '公众号',
        'icon' => 'iconweixingongzhonghao',
		'color' => '#36b313',
        'index' => 'wechat/index',
        'submenu' => [
			[
                'name' => '基础信息',
                'index' => 'wechat/index',
				'urls' => [
					'wechat/index',
				] 
            ],
			[
                'name' => '菜单设置',
                'index' => 'wechat/menus',
				'urls' => [
					'wechat/menu',
				] 
            ],
			[
				'name' => '群发消息',
				'index' => 'wechat.send/index',
				'uris' => [
					'wechat.send/index',
					'wechat.send/add',
                    'wechat.send/edit',
					'wechat.send/delete'
				],
			],
			[
                'name' => '素材管理',
                'active' => true,
				'submenu' => [
					[
						'name' => '图文素材',
						'index' => 'wechat.material.text/index',
						'urls' => [
							'wechat.material.text/index',
							'wechat.material.text/add',
							'wechat.material.text/edit',
							'wechat.material.text/delete'
						]
					],
					[
						'name' => '图片素材',
						'index' => 'wechat.material.image/index',
						'urls' => [
							'wechat.material.image/index',
							'wechat.material.image/add',
							'wechat.material.image/edit',
							'wechat.material.image/delete'
						]
					],	
					[
						'name' => '语音素材',
						'index' => 'wechat.material.voice/index',
						'urls' => [
							'wechat.material.voice/index',
							'wechat.material.voice/add',
							'wechat.material.voice/edit',
							'wechat.material.voice/delete'
						]
					],
					[
						'name' => '视频素材',
						'index' => 'wechat.material.video/index',
						'urls' => [
							'wechat.material.video/index',
							'wechat.material.video/add',
							'wechat.material.video/edit',
							'wechat.material.video/delete'
						]
					],
				]
            ],
			[
                'name' => '智能回复',
                'active' => false,
                'submenu' => [
					[
						'name' => '被关注回复',
						'index' => 'wechat/subscribe',
							'urls' => [
								'wechat/subscribe',
							]
					],
					[
						'name' => '关键字回复',
						'index' => 'wechat.keyword/index',
						'uris' => [
							'wechat.keyword/index',
							'wechat.keyword/add',
							'wechat.keyword/edit',
							'wechat.keyword/delete',
						],
					],
					
                ]
            ],
        ],
    ],
    'payment' => [
        'name' => '支付设置',
        'icon' => 'iconcaiwuguanli',
        'color' => '#36b313',
        'index' => 'payment/wxpay',
        'submenu' => [
            [
                'name' => '微信支付',
                'index' => 'payment/wxpay',
                'uris' => [
                    'payment/wxpay',
                ],
            ],
        ]
    ],
    'setting' => [
        'name' => '其它设置',
        'icon' => 'iconshezhi',
        'index' => 'setting/renew',
        'submenu' => [
            [
                'name' => '登录密码',
                'index' => 'setting/renew',
            ]
        ],
    ],
];
