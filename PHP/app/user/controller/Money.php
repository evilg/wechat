<?php
namespace app\user\controller;

use app\user\model\User as UserModel;
use app\user\model\Setting as SettingModel;
use app\user\model\Record as RecordModel;
use hema\wechat\Pay as WxPay;
use think\facade\View;
use think\facade\Cache;

/**
 * 钱包余额管理
 */
class Money extends Controller
{	
	/**
     * 用户钱包
     */
    public function index()
    {
		$user = UserModel::get($this->user_id);
        if (!$this->request->isAjax()) {
			View::assign('money',$user['money']);
			return View::fetch('index');
        }
		$data = $this->postData('data');
        $order_no = order_no();
        Cache::set($order_no, [
            'user_type' => 20,
            'order_no' => $order_no,
            'money' => $data['money'],
            'remark' => '扫码充值',
            'user_id' => $this->user_id
        ], 7200);
		return $this->renderSuccess('二维码生成中...', url('money/recharge',[
            'order_no'=> $order_no,
            'money'=> $data['money']
        ]));
    }

    /**
     * 添加提现申请
     */
    public function add()
    {
        $store = StoreUserModel::detail($this->user_id);
        if (!$this->request->isAjax()) {
            View::assign('wallet',$store['wallet']);
            return View::fetch('cash');
        }
        $model = new WebOrderModel;
        $pay = $this->postData('pay');
        if($store['wallet'] < $pay['money']){
           return $this->renderError('提现金额不可大于账户余额'); 
        }
        $values = WebSet::getItem('payment');
        if($values['wx']['cash_mode'] == 'fixed' AND $values['wx']['cash_fee'] > $pay['money']){
            return $this->renderError('不可提现，提现金额要大于￥'.$values['wx']['cash_fee'].'元'); 
        }
        // 创建订单
        if($model->add([
            'pay_price' => $pay['money'],
            'order_type' => 40,
            'purpose' => '用户提现',
            'affair_id' => $this->user_id,
            'agent_id' => $this->agent_id,
            'user_id' => $this->user_id
        ])) {
            return $this->renderSuccess('提交成功', url('wallet/index'));
        }
        $error = $model->getError() ?: '订单创建失败';
        return $this->renderError($error);
    }
	
	/**
     * 商户充值
     */
    public function recharge(string $order_no,$money)
    {
        $values = SettingModel::getItem('webpay',0);
        $wx = new WxPay($values['wx']);
        $code_url = $wx->unifiedorder('NATIVE',$order_no,$money,'api/notify/native','','商户充值');
		View::assign('money',$money);
		View::assign('code_url',$code_url);
		return View::fetch('recharge');
    }

	/**
     * 支付流水
     */
    public function log()
    {
        $model = new RecordModel;
        $list = $model->getList(20,$this->user_id);
        return View::fetch('log', compact('list'));
    }

    /**
     * 充值成功后跳转 - 需要在微信支付商户中心设置Native支付回调链接
     * 链接地址:你的域名/user/money/ok
     */
    public function ok()
    {
        return redirect(url('money/index'));
    }
}
