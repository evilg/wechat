<?php
namespace app\user\controller;

use app\user\model\User as UserModel;
use think\facade\View;

/**
 * 用户管理
 */
class User extends Controller
{
    /**
     * 代理登陆
     */
    public function agentLogin()
    {
        $model = UserModel::get($this->user_id);
        if ($model->agentLogin()) {
            return redirect('/agent');
        }
        $error = $model->getError() ?: '创建失败';
        return $this->renderError($error);
    }
}
