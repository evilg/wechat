<?php
namespace app\user\controller;

use app\user\model\Apply as ApplyModel;
use think\facade\View;

/**
 * 用户认证申请控制器
 */
class Apply extends Controller
{
    /**
     * 获取申请列表
     */
    public function index()
    {
        $model = new ApplyModel;
        $list = $model->getList(0,$this->user_id);
        return View::fetch('index',compact('list'));
    }

    /**
     * 编辑
     */
    public function detail($id)
    {  
        $model = ApplyModel::detail($id);
        if ($this->request->isGet()) {
            if($model){
               return $this->renderSuccess('', '', compact('model')); 
            }
            return $this->renderError('获取失败');
        }
        $data = $this->postData('data');
        $data['apply_status'] = 10;//驳回后提交
        if ($model->action($data)) {
            return $this->renderSuccess('操作成功', url('apply/index'));
        }
        $error = $model->getError() ?: '操作失败';
        return $this->renderError($error);
    }

}
