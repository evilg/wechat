<?php
namespace app\agent\controller\applet;

use app\agent\controller\Controller;
use app\agent\model\Template as TemplateModel;
use think\facade\View;

/**
 * 模板市场控制器
 */
class Template extends Controller
{
    /**
     * 列表
     */
    public function index()
    {
        $model = new TemplateModel;
		$list = $model->getList($this->user['user']['user_id']);
        return View::fetch('index', compact('list'));
    }

    /**
     * 删除分类
     */
    public function delete($id)
    {
        $model = TemplateModel::get($id);
        if (!$model->remove()) {
            $error = $model->getError() ?: '删除失败';
            return $this->renderError($error);
        }
        return $this->renderSuccess('删除成功');
    }

    /**
     * 添加
     */
    public function add()
    {
        $model = new TemplateModel;
        if (!$this->request->isAjax()) {
            $category = $model->getList();
            return View::fetch('add', compact('category'));
        }
        if ($model->add($this->postData('data'))) {
            return $this->renderSuccess('添加成功', url('applet.template/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        // 详情
        $model = TemplateModel::get($id);
        if (!$this->request->isAjax()) {
            return View::fetch('edit', compact('model'));
        }
        if ($model->edit($this->postData('data'))) {
            return $this->renderSuccess('更新成功', url('applet.template/index'));
        }
        $error = $model->getError() ?: '更新失败';
        return $this->renderError($error);
    }

}
