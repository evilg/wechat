<?php
namespace app\agent\controller;

use app\agent\model\Applet as AppletModel;
use app\agent\model\DivideAccount as DivideAccountModel;
use think\facade\Cache;
use think\facade\View;

/**
 * 小程序管理控制器
 */
class Applet extends Controller
{
    /**
     * 全部小程序列表
     */
    public function all()
    {
        return $this->lists(0,'全部');
    }
	
	/**
     * 已授权列表
     */
    public function normal()
    {
        return $this->lists(10,'已授权');
    }
	
	/**
     * 已到期列表
     */
    public function ends()
    {
        return $this->lists(20,'已到期');
    }

    /**
     * 获取列表
     */
    private function lists($type, string $title)
    {
        $model = new AppletModel;
        $list = $model->getList($type,0,$this->user['user']['user_id']);
        return View::fetch('index', compact('list','title'));
    }

    /**
     * 发布小程序新模板
     */
    public function upgrade($id)
    {
        //详情
        $model = AppletModel::getApplet(['applet_id' => $id]);
        if($model->publish()){
            return $this->renderSuccess('发布成功');
        }
        $error = $model->getError() ?: '发布失败';
        return $this->renderError($error);
    }
    
    /**
     * 关联分佣账户关系
    */
    public function relation()
    {
        $model = new DivideAccountModel;
        if ($model->add($this->postData(),$this->user['user']['user_id'])) {
            return $this->renderSuccess('关联成功');
        }
        $error = $model->getError() ?: '关联失败';
        return $this->renderError($error);
    }

}
