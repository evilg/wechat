<?php
namespace app\agent\controller;

use app\agent\model\User as UserModel;
use think\facade\View;

/**
 * 商户管理控制器
 */
class User extends Controller
{
    /**
     * 代理用户列表
     */
    public function index()
    {
        $model = new UserModel;
		$list = $model->getList(0,'all','',$this->user['user']['user_id']);
		return View::fetch('index', compact('list'));
    }

    /**
     * 添加
     */
    public function add()
    {
        if (!$this->request->isAjax()) {
            return View::fetch();
        }
        $model = new UserModel;
        if ($model->addAgent($this->postData('data'),$this->user['user']['user_id'])) {
            return $this->renderSuccess('添加成功', url('user/index'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }

    /**
     * 删除
     */
    public function delete($id)
    {
        $model = UserModel::get($id);
        if ($model->removeAgent()) {
            return $this->renderSuccess('解除成功');
        }
        return $this->renderError('解除失败');
    }

	/**
     * 一键登录
     */
    public function oneKey($id)
    {
		$model = UserModel::get($id);
		if($url = $model->oneKey()){
			return redirect($url);
		}
		$error = $model->getError() ?: '登录失败';
        return $this->renderError($error);
    }
}
