<?php
namespace app\agent\model;

use app\common\model\User as UserModel;
use think\facade\Session;
use think\facade\Db;

/**
 * 用户模型
 */
class User extends UserModel
{
	/**
     * 用户登录
     */
    public function login($data)
    {
        if(!captcha_check($data['captcha'])){
            $this->error = '验证码错误';
            return false;
        }
		$filter = [
            'user_name' => $data['user_name'],
            'password' => hema_hash($data['password']),
            'status' => 30
        ];
        // 验证用户名密码是否正确
		if($user = $this->withoutGlobalScope()->where($filter)->find()){
			// 保存登录状态
			Session::set('hema_agent', [
				'user' => $user,
				'is_login' => true
			]);
			return true;
		}else{
			$this->error = '登录失败, 用户名或密码错误';
            return false;
		}
    } 

    /**
     * 添加代理用户
     */
    public function addAgent($data,$agent_id)
    {
		$filter = [
            'user_name' => $data['user_name'],
            'password' => hema_hash($data['password'])
        ];
        // 验证用户名密码是否正确
		if (!$user = $this->where($filter)->find()){
			$this->error = '用户名或密码错误';
            return false;
		}
		if($user['agent_id']>0){
			$this->error = '该用户已绑定过代理';
            return false;
		}
		// 开启事务
        Db::startTrans();
        try {
			Applet::where('user_id',$user['user_id'])->update(['agent_id' => $agent_id]);
			$user->save(['agent_id' => $agent_id]);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

    /**
     * 删除代理用户 - 取消关系
     */
    public function removeAgent()
    {
    	// 开启事务
        Db::startTrans();
        try {
			Applet::where('user_id',$this->user_id)->update(['agent_id' => 0]);
			$this->save(['agent_id' => 0]);
			$appletId = (new Applet)->where('user_id',$this->user_id)->column('applet_id');
			foreach ($appletId as $vo){
			    DivideAccount::where('applet_id',$vo)->delete(); //清楚分账关联账户数据
			}
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
        }
        return false;
    }

}
