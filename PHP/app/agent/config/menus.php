<?php
return [
    'index' => [
        'name' => '首页',
        'icon' => 'iconshouye',
        'index' => 'index/index',
    ],
    'applet' => [
        'name' => '小程序',
        'icon' => 'iconweixinxiaochengxu',
        'color' => '#36b313',
        'index' => 'applet/all',
        'submenu' => [
            [
                'name' => '小程序管理',
                'active' => false,
                'submenu' => [
                    [
                        'name' => '全部',
                        'index' => 'applet/all',
                        'uris' => [
                            'applet/all',
                            'applet/delete'
                        ],
                    ],
                    [
                        'name' => '已授权',
                        'index' => 'applet/normal',
                        'uris' => [
                            'applet/normal',
                            'applet/delete'
                        ],
                    ],
                    [
                        'name' => '已到期',
                        'index' => 'applet/ends',
                        'uris' => [
                            'applet/ends',
                            'applet/delete'
                        ],
                    ],
                ]
            ],
            [
                'name' => '模板市场',
                'index' => 'applet.template/index',
                'uris' => [
                    'applet.template/index',
                    'applet.template/add',
                    'applet.template/edit',
                    'applet.template/delete'
                ],
            ],
        ],
    ],
    'user' => [
        'name' => '商户管理',
        'icon' => 'iconyonghuguanli',
        'index' => 'user/index',
        'submenu' => [
            [
                'name' => '商户列表',
                'index' => 'user/index',
                'uris' => [
                    'user/index',
                    'user/add',
                    'user/delete',
                ],
            ],
        ]
    ],
    'finance' => [
        'name' => '账务管理',
        'icon' => 'iconcaiwuguanli',
        'index' => 'finance/index',
        'submenu' => [
            [
                'name' => '财务统计',
                'index' => 'finance/index',
                'uris' => [
                    'finance/index',
                ],
            ],
            [
                'name' => '商户流水',
                'index' => 'finance.log/store',
                'uris' => [
                    'finance.log/store',
                ],
            ],
            [
                'name' => '会员流水',
                'index' => 'finance.log/user',
                'uris' => [
                    'finance.log/user',
                ],
            ],
            [
                'name' => '我的流水',
                'index' => 'finance.log/my',
                'uris' => [
                    'finance.log/my',
                ],
            ],
            [
                'name' => '提现申请',
                'index' => '#',
                'uris' => [
                    'finance.order/cash',
                    'finance.order/add',
                    'finance.order/edit',
                    'finance.order/delete'
                ],
            ],
        ]
    ],
    'setting' => [
        'name' => '设置',
        'icon' => 'iconshezhi',
        'index' => 'setting/renew',
        'submenu' => [
            [
                'name' => '修改密码',
                'index' => 'setting/renew',
                'urls' => [
                    'setting/renew'
                ]
            ],
        ],
    ],
];
