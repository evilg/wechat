<?php
namespace addons\upload\controller;

use think\addons\Controller;
use addons\upload\model\UploadFile as UploadFileModel;
use addons\upload\model\UploadGroup as UploadGroupModel;

class Library extends Controller
{
    /**
     * 文件库列表
     */
    public function fileList()
    {
        $applet_id = $this->request->param('applet_id',0);
        $type = $this->request->param('type','image');
        $group_id = $this->request->param('group_id',0);
        // 分组列表
        $group_list = (new UploadGroupModel)->getList($applet_id,$type);
        // 文件列表
        $file_list = (new UploadFileModel)->getlist($applet_id,intval($group_id), $type);
        return $this->renderSuccess('success', '', compact('group_list', 'file_list'));
    }

    /**
     * 新增分组
     */
    public function addGroup()
    {
        $applet_id = $this->request->param('applet_id',0);
        $group_type = $this->request->param('group_type','image');
        $group_name = $this->request->param('group_name','');
        $model = new UploadGroupModel;
        if ($model->add(compact('applet_id', 'group_name', 'group_type'))) {
            $group_id = $model->getLastInsID();
            return $this->renderSuccess('添加成功', '', compact('group_id', 'group_name'));
        }
        $error = $model->getError() ?: '添加失败';
        return $this->renderError($error);
    }


    /**
     * 编辑分组
     */
    public function editGroup()
    {
        $group_id = $this->request->param('group_id',0);
        $group_name = $this->request->param('group_name','');
        $model = UploadGroupModel::get($group_id);
        if ($model->edit(compact('group_name'))) {
            return $this->renderSuccess('修改成功');
        }
        $error = $model->getError() ?: '修改失败';
        return $this->renderError($error);
    }

    /**
     * 删除分组
     */
    public function deleteGroup()
    {
        $applet_id = $this->request->param('applet_id',0);
        $group_id = $this->request->param('group_id',0);
        $model = UploadGroupModel::get($group_id);
        if ($model->remove($applet_id)) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
    }

    /**
     * 批量删除文件
     */
    public function deleteFiles()
    {
        $fileIds = $this->request->param('fileIds',[]);
        $model = new UploadFileModel;
        if ($model->softDelete($fileIds)) {
            return $this->renderSuccess('删除成功');
        }
        $error = $model->getError() ?: '删除失败';
        return $this->renderError($error);
    }

    /**
     * 批量移动文件分组
     */
    public function moveFiles()
    {
        $group_id = $this->request->param('group_id',0);
        $fileIds = $this->request->param('fileIds',[]);
        $model = new UploadFileModel;
        if ($model->moveGroup($group_id, $fileIds) !== false) {
            return $this->renderSuccess('移动成功');
        }
        $error = $model->getError() ?: '移动失败';
        return $this->renderError($error);
    }
}
