<?php
namespace addons\food\model;
use app\common\model\Template as TemplateModel;
/**
 * 模板市场模型
 */
class Template extends TemplateModel
{
    /**
	 * 安装到模板市场
	 */
	public function install($name)
	{
	    $addon = get_addons_info($name);
		// 添加
		return $this->save([
			'name' => $addon['title'],
			'app_type' => $addon['name'],
			'is_many' => 1,//支持多门店
			'buy_single' => ["0","0","0","0","0","0"],
			'renew_single' => ["0","0","0","0","0"],
			'buy_many' => ["0","0","0","0","0","0"],
			'renew_many' => ["0","0","0","0","0"],
			'trial_day' => 30,
		]);
	}
	/**
	 * 卸载模板市场
	 */
	public function uninstall($name)
	{
	    return $this->where('app_type', $name)->delete();
	}
}
