<?php
namespace addons\food;
use think\Addons;
use addons\food\model\Applet as AppletModel;
use addons\food\model\Template as TemplateModel;
/**
 * 智慧餐厅插件
 */
class Plugin extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'food',	    // 插件标识
        'title' => '智慧餐厅',	// 插件名称
        'description' => '扫码点餐，排号点餐，外卖配送，排队订桌',	// 插件简介
        'status' => 1,	// 状态
        'author' => 'hemaPHP',
        'version' => '4.2.5'
    ];	
    /**
     * 钩子方法 - 创建小程序
     * @return mixed
     */
    public function food_applet_add($param)
    {
        $model = new AppletModel;
		return $model->add($param);
    }
    /**
     * 钩子方法 - 删除小程序
     * @return mixed
     */
    public function food_applet_delete($param)
    {
        $model = new AppletModel;
		return $model->remove($param['applet_id']);
    }
    /**
     * 钩子方法 - 清除小程序数据
     * @return mixed
     */
    public function food_applet_clear($param)
    {
        $model = new AppletModel;
		return $model->clear($param['applet_id']);
    }
     /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        //添加到模板市场
        $model = new TemplateModel;
        $model->install($this->name);
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        //清除小程序数据
        $appletId = (new AppletModel)->where('app_type', $this->name)->column('applet_id');
        foreach ($appletId as $value){
            $model = new AppletModel;
            $model->remove($value);
        }
        //卸载模板市场数据
        $model = new TemplateModel;
        $model->uninstall($this->name);
        return true;
    }
    /**
     * 插件升级方法
     * @return bool
     */
    public function upgrade()
    {
        return true;
    }
    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        return true;
    }
    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        return true;
    }
}