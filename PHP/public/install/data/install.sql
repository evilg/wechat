-- ----------------------------
-- 预约表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_pact`;
CREATE TABLE `hema_food_pact` (
  `pact_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pact_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式，10排号 20订桌',
  `row_no` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单排号',
  `table_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '桌位id',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `pact_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '约定时间',
  `people` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '约定人数',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '客户留言',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态，10预约中 20已过期 30已守约,40已取消',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`pact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='预约表';

-- ----------------------------
-- 口味选项表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_flavor`;
CREATE TABLE `hema_food_flavor` (
  `flavor_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '口味名称',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`flavor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='口味选项表';

-- ----------------------------
-- 订单评价表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_comment`;
CREATE TABLE `hema_food_comment` (
  `comment_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `serve` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '服务评分',
  `speed` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '速度评分',
  `flavor` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '服务评分',
  `ambient` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '服务评分',
  `content` varchar(255) NOT NULL DEFAULT '这家伙很懒，什么都没写' COMMENT '详情',
  `reply` varchar(255) NOT NULL DEFAULT '' COMMENT '回复详情',
  `is_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示(0隐藏，1显示)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='订单评价表';

-- ----------------------------
-- 充值套餐
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_recharge_plan`;
CREATE TABLE `hema_food_recharge_plan` (
  `recharge_plan_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值金额',
  `gift_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '赠品类型（10优惠券，20现金）',
  `coupon_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券id',
  `gift_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '赠送金额',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`recharge_plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='充值套餐表';

-- ----------------------------
-- 餐桌/包间表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_table`;
CREATE TABLE `hema_food_table` (
  `table_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '餐桌id',
  `table_name` varchar(50) NOT NULL DEFAULT '' COMMENT '餐桌名称',
  `people` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '容纳人数',
  `consume` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '最低消费',
  `ware_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '餐具调料费',
  `row_no` varchar(10) NOT NULL DEFAULT '' COMMENT '餐桌编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '状态(10空闲 20忙碌 30预定)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店ID',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='餐桌/包间表';

-- ----------------------------
-- 店员表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_shop_clerk`;
CREATE TABLE `hema_food_shop_clerk` (
  `shop_clerk_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店ID',
  `real_name` varchar(20) NOT NULL DEFAULT '' COMMENT '店员姓名',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '密码',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '身份(10店员 20店长 30 配送)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`shop_clerk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='店员表';

-- ----------------------------
-- 商品分类表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_category`;
CREATE TABLE `hema_food_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品分类id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类id',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '分类图片id',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序方式(数字越小越靠前)',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='商品分类表';

-- ----------------------------
-- 产品表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_goods`;
CREATE TABLE `hema_food_goods` (
  `goods_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `selling_point` varchar(255) NOT NULL DEFAULT '' COMMENT '商品卖点',
  `category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品分类id',
  `spec_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品规格(10单规格 20多规格)',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `content` longtext NOT NULL COMMENT '商品详情',
  `sales_initial` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '初始销量',
  `sales_actual` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '实际销量',
  `goods_sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '商品排序(数字越小越靠前)',
  `goods_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商品状态(10上架 20下架)',
  `pack_price` decimal(10,2) unsigned NOT NULL DEFAULT '1.00' COMMENT '餐盒费用',
  `is_delete` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `is_recommend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否推荐',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='产品表';

-- ----------------------------
-- 产品图片表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_goods_image`;
CREATE TABLE `hema_food_goods_image` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `image_id` int(11) NOT NULL COMMENT '图片id(关联文件记录表)',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='产品图片表';

-- ----------------------------
-- 产品规格表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_goods_spec`;
CREATE TABLE `hema_food_goods_spec` (
  `goods_spec_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品规格id',
  `spec_sku_id` varchar(255) NOT NULL DEFAULT '' COMMENT '商品spu标识',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '多规格商品图片id',
  `goods_no` varchar(100) NOT NULL DEFAULT '' COMMENT '商品编码',
  `goods_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '店内售价',
  `line_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品划线价',
  `stock_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '当前库存数量',
  `goods_weight` double unsigned NOT NULL DEFAULT '0' COMMENT '商品重量(Kg)',
  `goods_sales` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品销量',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`goods_spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='产品规格表';

-- ----------------------------
-- 产品规格属性表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_goods_spec_rel`;
CREATE TABLE `hema_food_goods_spec_rel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `spec_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '规格组id',
  `spec_value_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '规格值id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='产品规格属性表';

-- ----------------------------
-- 规格表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_spec`;
CREATE TABLE `hema_food_spec` (
  `spec_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '规格组id',
  `spec_name` varchar(255) NOT NULL DEFAULT '' COMMENT '规格组名称',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='规格表';

-- ----------------------------
-- 规格属性表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_spec_value`;
CREATE TABLE `hema_food_spec_value` (
  `spec_value_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '规格值id',
  `spec_value` varchar(255) NOT NULL COMMENT '规格值',
  `spec_id` int(11) NOT NULL COMMENT '规格组id',
  `applet_id` int(11) NOT NULL COMMENT '小程序id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`spec_value_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='规格属性表';

-- ----------------------------
-- 订单表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_order`;
CREATE TABLE `hema_food_order` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `source` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单来源(10自助点单 20代客点单 30平台点单',
  `order_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单类型(10堂食 20自取 30外卖)',
  `row_no` varchar(10) NOT NULL DEFAULT '' COMMENT '订单排号',
  `table_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '餐桌/包间id',
  `people` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '就餐人数',
  `arrive_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '自取到店时间',
  `flavor` varchar(255) NOT NULL DEFAULT '' COMMENT '口味选项',
  `message` varchar(255) NOT NULL DEFAULT '' COMMENT '买家留言',
  `total_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品总金额',
  `activity_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '优惠价格',
  `express_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '配送费用',
  `pack_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '餐盒费用',
  `ware_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '餐具调料费',
  `divide_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '分账金额',
  `pay_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单实付款金额',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '付款状态(10未付款 20已付款)',
  `transaction_id` varchar(100) NOT NULL DEFAULT '' COMMENT '微信支付交易号',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '付款时间',
  `shop_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '商家状态(10未接单 20已接单)',
  `shop_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '接单时间',
  `delivery_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '配送状态(10未配送 20配送中 30已配送)',
  `delivery_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '配送时间',
  `receipt_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '收货状态(10未收货 20已收货)',
  `receipt_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '收货时间',
  `order_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单状态(10进行中 20取消 30已完成)',
  `refund_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '退款金额',
  `refund_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '退款状态(10退款中 20已退款)',
  `refund_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '退款时间',
  `refund_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '退款理由',
  `refund_id` varchar(100) NOT NULL DEFAULT '' COMMENT '微信退款单号',
  `is_cmt` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否评价(0待评价 1已评价)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='订单表';

-- ----------------------------
-- 订单表配送表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_order_delivery`;
CREATE TABLE `hema_food_order_delivery` (
  `order_delivery_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号id',
  `company` varchar(10) NOT NULL DEFAULT 'self' COMMENT '配送公司(self=自配 sf=顺丰 dada=达达 uu=UU)',
  `order_no` varchar(50) NOT NULL DEFAULT '' COMMENT '配送单号',
  `distance` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '配送距离',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '配送费用',
  `linkman` varchar(50) NOT NULL DEFAULT '' COMMENT '骑手姓名',
  `phone` varchar(50) NOT NULL DEFAULT '' COMMENT '骑手电话',
  `delivery_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '配送状态(10待骑手接单 20骑手正赶往商家 30骑手已到店 40骑手开始配送 50骑手已送达)',
  `delivery_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '配送时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '订单状态(10进行中 20被取消 30已完成)',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单号',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`order_delivery_id`),
  UNIQUE KEY `order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='订单表配送表';

-- ----------------------------
-- 订单收货地址表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_order_address`;
CREATE TABLE `hema_food_order_address` (
  `order_address_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '地址id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`order_address_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='订单收货地址表';

-- ----------------------------
-- 用户
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_order_goods`;
CREATE TABLE `hema_food_order_goods` (
  `order_goods_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品id',
  `goods_name` varchar(255) NOT NULL DEFAULT '' COMMENT '商品名称',
  `image_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品封面图id',
  `deduct_stock_type` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '库存计算方式(10下单减库存 20付款减库存)',
  `spec_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '规格类型(10单规格 20多规格)',
  `spec_sku_id` varchar(255) NOT NULL DEFAULT '' COMMENT '商品sku标识',
  `goods_spec_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商品规格id',
  `goods_attr` varchar(500) NOT NULL DEFAULT '' COMMENT '商品规格信息',
  `content` longtext NOT NULL COMMENT '商品详情',
  `goods_no` varchar(100) NOT NULL DEFAULT '' COMMENT '商品编码',
  `goods_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品售价',
  `line_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品划线价',
  `goods_weight` double unsigned NOT NULL DEFAULT '0' COMMENT '商品重量(Kg)',
  `total_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '购买数量',
  `total_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品总价',
  `refund_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '退单数量',
  `refund_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '退单总价',
  `order_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`order_goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='订单产品表';

-- ----------------------------
-- 帮助文档
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_help`;
CREATE TABLE `hema_food_help` (
  `help_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '帮助标题',
  `content` text NOT NULL COMMENT '帮助内容',
  `sort` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序(数字越小越靠前)',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`help_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='帮助文档表';

-- ----------------------------
-- 门店表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_shop`;
CREATE TABLE `hema_food_shop` (
  `shop_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `shop_name` varchar(50) NOT NULL DEFAULT '' COMMENT '门店名称',
  `shop_category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店分类',
  `logo` varchar(255) NOT NULL DEFAULT '' COMMENT '门店logo',
  `front` varchar(255) NOT NULL DEFAULT '' COMMENT '门店照片',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `shop_hours` varchar(20) NOT NULL DEFAULT '' COMMENT '营业时间',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `address` varchar(50) NOT NULL DEFAULT '' COMMENT '详细地址',
  `coordinate` varchar(50) NOT NULL DEFAULT '' COMMENT '门店坐标',
  `poi_id` varchar(50) NOT NULL DEFAULT '' COMMENT '位置识别码',
  `summary` varchar(255) NOT NULL DEFAULT '' COMMENT '门店简介',
  `tang_mode` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '堂食模式(1选桌 2排号)',
  `is_order` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自动接单(0关闭 1开启)',
  `is_delivery` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自动配送(0关闭 1开启)',
  `delivery` varchar(255) NOT NULL DEFAULT 'self' COMMENT '配送公司(self=自配 sf=顺丰 dada=达达 uu=UU make=码科)',
  `is_grab` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '骑手抢单(0关闭 1开启)',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '门店状态(1开启 0关闭)',
  `out_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '平台状态(1开启 0关闭)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序(数字越小越靠前)',
  `sf_shop_id` varchar(20) NOT NULL DEFAULT '' COMMENT '顺丰门店ID',
  `dada_shop_id` varchar(20) NOT NULL DEFAULT '' COMMENT '达达门店ID',
  `make_shop_id` varchar(20) NOT NULL DEFAULT '' COMMENT '码科门店ID',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`shop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='门店表';

-- ----------------------------
-- 优惠券表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_coupon`;
CREATE TABLE `hema_food_coupon` (
  `coupon_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '分类（10现金券 20折扣券 30赠送券 40减免券）',
  `rule` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '类型（10通用券 20外卖券）',
  `mode` varchar(255) NOT NULL DEFAULT '' COMMENT '规则（1立减 2首单立减 3满赠 4满减 5满折 6等级折扣）',
  `valid_time` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '有效期（10当天 20一周 30一月 40一年）',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `values` longtext NOT NULL COMMENT '条件参数',
  `explain` varchar(255) NOT NULL DEFAULT '' COMMENT '规则说明',
  `color` varchar(50) NOT NULL DEFAULT 'warning' COMMENT '显示样式',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`coupon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='优惠券表';

-- ----------------------------
-- 优惠券用户领取表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_coupon_user`;
CREATE TABLE `hema_food_coupon_user` (
  `coupon_user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `coupon_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式（10现金券 20折扣券 30赠送券 40减免券）',
  `rule` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '类型（10通用券 20外卖券）',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `expiretime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '到期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`coupon_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='优惠券用户领取表';

-- ----------------------------
-- 优惠券群发记录表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_coupon_log`;
CREATE TABLE `hema_food_coupon_log` (
  `coupon_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `coupon_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '优惠券id',
  `v` tinyint(11) unsigned NOT NULL DEFAULT '0' COMMENT '等级条件',
  `count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发送数量',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发送人id',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`coupon_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='优惠券群发记录表';

-- ----------------------------
-- 云设备表
-- ----------------------------
DROP TABLE IF EXISTS `hema_food_device`;
CREATE TABLE `hema_food_device` (
  `device_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dev_type` varchar(50) NOT NULL DEFAULT '' COMMENT '设备类型',
  `dev_name` varchar(50) NOT NULL DEFAULT '' COMMENT '设备名称',
  `dev_id` varchar(50) NOT NULL DEFAULT '' COMMENT '设备编号',
  `dev_key` varchar(50) NOT NULL DEFAULT '' COMMENT '设备密钥',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '开关(0关闭 1开启)',
  `values` varchar(500) NOT NULL DEFAULT '' COMMENT '自定义设置',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店ID',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='云设备表'; 







-- ----------------------------
-- 站点配置
-- ----------------------------
DROP TABLE IF EXISTS `hema_config`;
CREATE TABLE `hema_config` (
  `config_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '站点配置ID',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppID',
  `app_secret` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppSecret',
  `encoding_aes_key` varchar(50) NOT NULL DEFAULT '' COMMENT 'encoding_aes_key',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'token',
  `api_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器域名',
  `authorize_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '授权发起页域名',
  `component_verify_ticket` varchar(255) NOT NULL DEFAULT '' COMMENT 'ticket',
  `component_access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌过期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8 COMMENT='站点配置';

-- ----------------------------
-- 站点配置表
-- ----------------------------
DROP TABLE IF EXISTS `hema_setting`;
CREATE TABLE `hema_setting` (
  `key` varchar(30) NOT NULL COMMENT '设置项标示',
  `describe` varchar(255) NOT NULL DEFAULT '' COMMENT '设置项描述',
  `values` mediumtext NOT NULL COMMENT '设置内容（json格式）',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  UNIQUE KEY `unique_key` (`key`,`applet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点配置表';

-- ----------------------------
-- 友情链接
-- ----------------------------
DROP TABLE IF EXISTS `hema_link`;
CREATE TABLE `hema_link` (
  `link_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '站点名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '站点地址',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10005 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- 微信小程序表
-- ----------------------------
DROP TABLE IF EXISTS `hema_applet`;
CREATE TABLE `hema_applet` (
  `applet_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '小程序id',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '应用类型',
  `shop_mode` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '门店模式（10单门店 20多门店）',
  `source` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '账号来源，10=自助注册，20=平台注册',
  `app_name` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序头像',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '小程序二维码',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '原始ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序AppID',
  `principal_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `api_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器域名',
  `webview_domain` varchar(255) NOT NULL DEFAULT '' COMMENT '业务域名',
  `signature` varchar(255) NOT NULL DEFAULT '' COMMENT '功能介绍',
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌凭证',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌到期时间',
  `authorizer_refresh_token` varchar(255) NOT NULL DEFAULT '' COMMENT '刷新令牌',
  `applet_tpl_id` int(11) NOT NULL DEFAULT '0' COMMENT '线上模板ID',
  `is_live` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否开启直播插件 1开启，0关闭',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否授权(0否 1是)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序到期时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`applet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='微信小程序表';

-- ----------------------------
-- 小程序页面设计表
-- ----------------------------
DROP TABLE IF EXISTS `hema_page`;
CREATE TABLE `hema_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '页面id',
  `page_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '页面类型(10首页 20自定义页)',
  `page_data` longtext NOT NULL COMMENT '页面数据',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '微信小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='小程序页面设计表';

-- ----------------------------
-- 小程序模板代码表
-- ----------------------------
DROP TABLE IF EXISTS `hema_template_code`;
CREATE TABLE `hema_template_code` (
  `template_code_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '模板ID',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '模板类型',
  `id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '服务端ID',
  `user_version` varchar(255) NOT NULL DEFAULT '' COMMENT '版本号',
  `user_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '版本描述',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`template_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='小程序模板代码表';

-- ----------------------------
-- 商户发布的模板
-- ----------------------------
DROP TABLE IF EXISTS `hema_applet_tpl`;
CREATE TABLE `hema_applet_tpl` (
  `applet_tpl_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `template_code_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '模板代码ID',
  `auditid` varchar(255) NOT NULL DEFAULT '' COMMENT '审核编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态，0审核中，1被拒绝 2已上线',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '拒绝原因',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`applet_tpl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='商户发布的模板表';

-- ----------------------------
-- 小程序名称表
-- ----------------------------
DROP TABLE IF EXISTS `hema_applet_name`;
CREATE TABLE `hema_applet_name` (
  `applet_name_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nick_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `audit_id` varchar(50) NOT NULL DEFAULT '' COMMENT '审核编号',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0审核中，1设置成功 ，2被驳回)',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`applet_name_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='小程序名称表';

-- ----------------------------
-- 公众号表
-- ----------------------------
DROP TABLE IF EXISTS `hema_wechat`;
CREATE TABLE `hema_wechat` (
  `wechat_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号id',
  `app_name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `head_img` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '二维码',
  `user_name` varchar(50) NOT NULL DEFAULT '' COMMENT '原始ID',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'AppID',
  `principal_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主体名称',
  `access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '令牌凭证',
  `expires_in` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '令牌到期时间',
  `authorizer_refresh_token` varchar(255) NOT NULL DEFAULT '' COMMENT '刷新令牌',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否授权(0否 1是)',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wechat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号表';

-- ----------------------------
-- 微信批量发送信息表
-- ----------------------------
DROP TABLE IF EXISTS `hema_wechat_batch_send`;
CREATE TABLE `hema_wechat_batch_send` (
  `wechat_batch_send_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `msg_id` varchar(50) NOT NULL DEFAULT '' COMMENT '任务ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '消息标题',
  `msg_type` varchar(20) NOT NULL DEFAULT '' COMMENT '消息类型',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '消息内容',
  `recommend` varchar(255) NOT NULL DEFAULT '' COMMENT '推荐语',
  `need_open_comment` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启评论，0关闭，1开启',
  `only_fans_can_comment` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '谁可评论，0所有人，1仅粉丝',
  `send_ignore_reprint` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '为转载时，0停止群发，1继续群发',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '内容描述',
  `status` int(11) unsigned NOT NULL DEFAULT '10' COMMENT '群发状态',
  `fans_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '群发人数',
  `send_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '成功人数',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`wechat_batch_send_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='微信批量发送信息表';

-- ----------------------------
-- 公众号素材表
-- ----------------------------
DROP TABLE IF EXISTS `hema_material`;
CREATE TABLE `hema_material` (
  `material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '素材名称',
  `file_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '文件类型 10=图片，20=音频，30=视频，40=图文',
  `file_path` varchar(255) NOT NULL DEFAULT '' COMMENT '本地文件名称',
  `media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '素材media_id',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '素材网络地址',
  `text_no` varchar(20) NOT NULL DEFAULT '' COMMENT '图文素材编号',
  `introduction` varchar(255) NOT NULL DEFAULT '' COMMENT '视频素材描述',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号素材表';

-- ----------------------------
-- 公众号图文素材详情表
-- ----------------------------
DROP TABLE IF EXISTS `hema_material_text`;
CREATE TABLE `hema_material_text` (
  `material_text_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录编号',
  `text_no` varchar(20) NOT NULL DEFAULT '' COMMENT '素材编号',
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '素材信息序号',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `author` varchar(50) NOT NULL DEFAULT '' COMMENT '作者',
  `digest` varchar(100) NOT NULL DEFAULT '' COMMENT '摘要',
  `content` longtext NOT NULL COMMENT '正文',
  `media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '素材media_id',
  `file_path` varchar(255) NOT NULL DEFAULT '' COMMENT '本地文件名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '封面网络地址',
  `content_source_url` varchar(255) NOT NULL DEFAULT '#' COMMENT '图文链接地址',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`material_text_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号图文素材详情表';

-- ----------------------------
-- 公众号关键字回复
-- ----------------------------
DROP TABLE IF EXISTS `hema_keyword`;
CREATE TABLE `hema_keyword` (
  `keyword_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `keyword` varchar(50) NOT NULL DEFAULT '' COMMENT '关键字',
  `type` varchar(10) NOT NULL DEFAULT 'text' COMMENT '消息类型 text=文字，image=图片，voice=音频，video=视频，music=音乐，news=图文',
  `is_open` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启 0=关闭，1=开启',
  `content` longtext COMMENT '消息内容',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`keyword_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='公众号关键字回复';

-- ----------------------------
-- 用户
-- ----------------------------
DROP TABLE IF EXISTS `hema_user`;
CREATE TABLE `hema_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `union_id` varchar(50) NOT NULL DEFAULT '' COMMENT '平台唯一标识',
  `open_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid(唯一标识)',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '登录密码',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `gender` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT '国家',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(50) NOT NULL DEFAULT '' COMMENT '城市',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '钱包余额',
  `linkman` varchar(20) NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `pay` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '消费金额',
  `score` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户积分',
  `converted` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '已兑换积分',
  `v` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '会员等级',
  `birthday` varchar(50) NOT NULL DEFAULT '' COMMENT '会员生日',
  `address_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '默认收货地址',
  `recommender` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '身份 10用户 20管理 30代理',
  `is_staff` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内部员工 0=否 1是',
  `platform` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '平台 10微信 20H5',
  `is_subscribe` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否关注公众号',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `login_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- 商户详情表
-- ----------------------------
DROP TABLE IF EXISTS `hema_user_detail`;
CREATE TABLE `hema_user_detail` (
  `user_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT '商户号',
  `id_card_copy` varchar(100) NOT NULL DEFAULT '' COMMENT '身份证正面',
  `id_card_national` varchar(100) NOT NULL DEFAULT '' COMMENT '身份证反面',
  `id_card_name` varchar(50) NOT NULL DEFAULT '' COMMENT '证件姓名',
  `id_card_number` varchar(50) NOT NULL DEFAULT '' COMMENT '证件号码',
  `legal_persona_wechat` varchar(50) NOT NULL DEFAULT '' COMMENT '微信号',
  `mobile_phone` varchar(50) NOT NULL DEFAULT '' COMMENT '手机号',
  `contact_email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `subject_type` varchar(50) NOT NULL DEFAULT 'SUBJECT_TYPE_INDIVIDUAL' COMMENT '主体类型',
  `license_copy` varchar(100) NOT NULL DEFAULT '' COMMENT '营业执照',
  `merchant_name` varchar(50) NOT NULL DEFAULT '' COMMENT '营业执照名称',
  `license_number` varchar(50) NOT NULL DEFAULT '' COMMENT '统一社会信用代码',
  `qualifications` varchar(255) NOT NULL DEFAULT '' COMMENT '特殊资质',
  `bank_name` varchar(100) NOT NULL DEFAULT '' COMMENT '开户银行全称',
  `account_number` varchar(50) NOT NULL DEFAULT '' COMMENT '银行账号',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`user_detail_id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='商户详情表';

-- ----------------------------
-- 用户地址表
-- ----------------------------
DROP TABLE IF EXISTS `hema_address`;
CREATE TABLE `hema_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '联系电话',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='用户地址表';

-- ----------------------------
-- 交易记录
-- ----------------------------
DROP TABLE IF EXISTS `hema_record`;
CREATE TABLE `hema_record` (
  `record_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '用户类型(10商家会员 20站点会员 30代理 40平台)',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '类型(10余额 20现金 30微信 90积分)',
  `mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '模式(10自助 20后台)',
  `action` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '方式(10充值 20扣减 30重置 40消费 50分红 60退款 70提现)',
  `order_no` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '操作数值',
  `gift_money` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '赠送数值',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '操作备注',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '20' COMMENT '状态(10处理中 20已完成)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `shop_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '门店id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='交易记录表';

-- ----------------------------
-- 认证申请表
-- ----------------------------
DROP TABLE IF EXISTS `hema_apply`;
CREATE TABLE `hema_apply` (
  `apply_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `apply_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '认证类型(10小程序申请 20商户入驻 30支付认证 40代理认证 50实名认证)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '申请人ID',
  `applet_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序ID',
  `merchant_shortname` varchar(50) NOT NULL DEFAULT '' COMMENT '商户简称',
  `app_id` varchar(50) NOT NULL DEFAULT '' COMMENT '小程序AppID',
  `auth_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '认证类型(10工商 20个人)',
  `agent_mode` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '代理级别(10 区县 20 市级 30 省级)',
  `location` varchar(50) NOT NULL DEFAULT '' COMMENT '位置坐标',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '所在省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '所在城市',
  `district` varchar(20) NOT NULL DEFAULT '' COMMENT '所在区/县',
  `pay_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '支付状态(10待支付，20已支付，30免审核费)',
  `pay_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `reject` varchar(255) NOT NULL DEFAULT '' COMMENT '驳回原因',
  `apply_status` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '审核状态(10待审核，20验证中，30已审核，40已驳回)',
  `apply_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '审查时间',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`apply_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='认证申请表';

-- ----------------------------
-- 签名记录表
-- ----------------------------
DROP TABLE IF EXISTS `hema_sign_log`;
CREATE TABLE `hema_sign_log` (
  `sign_log_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号ID',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`sign_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='签名记录表';

-- ----------------------------
-- 小程序模板市场表
-- ----------------------------
DROP TABLE IF EXISTS `hema_template`;
CREATE TABLE `hema_template` (
  `template_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '版本名称',
  `app_type` varchar(50) NOT NULL DEFAULT '' COMMENT '模板类型',
  `is_many` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '支持多门店(1支持，0不支持)',
  `buy_single` varchar(255) NOT NULL DEFAULT '' COMMENT '单门店购买价格',
  `renew_single` varchar(255) NOT NULL DEFAULT '' COMMENT '单门店续费价格',
  `buy_many` varchar(255) NOT NULL DEFAULT '' COMMENT '多门店购买价格',
  `renew_many` varchar(255) NOT NULL DEFAULT '' COMMENT '多门店续费价格',
  `trial_day` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '免费试用天数',
  `detail` varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
  `single_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '单门店代理价',
  `many_price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '多门店代理价',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序方式(数字越小越靠前)',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10002 DEFAULT CHARSET=utf8 COMMENT='小程序模板市场表';

-- ----------------------------
-- 分账账户关联表
-- ----------------------------
DROP TABLE IF EXISTS `hema_divide_account`;
CREATE TABLE `hema_divide_account` (
  `divide_account_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `open_id` varchar(50) NOT NULL DEFAULT '' COMMENT 'openid(唯一标识)',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `agent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属代理',
  `applet_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '小程序id',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`divide_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 COMMENT='分账账户关联表';

