(function() {
    document.body.ondrop = function(event) {
        event.preventDefault();
        event.stopPropagation()
    };
    var diyData = {};

    function diyPhone(temp, data) {
        diyData = temp;
        this.init(data)
    }
	
    diyPhone.prototype = {
        init: function(data) {
            new Vue({
                el: '#app',
                data: {
                    diyData: data,
					scene:'',	//场景说明
                    selectedIndex: -1
                },
                methods: {
					//添加组件
                    onAddItem: function(key) {
						if(this.diyData.values.length==5){
							$.show_error(hm_lang[52]);
							return false;
						}
						//去除重复
						for(var idx in this.diyData.values){
							if(this.diyData.values[idx].kid == diyData[key].kid){
								$.show_error(hm_lang[53]);
								return false;
							}
						}
                        var data = $.extend(true, {}, diyData[key]);
                        this.diyData.values.push(data);
                    },
					//拖拽组件
                    onDragItemEnd: function(DragItem) {
                        DragItem.newIndex
                    },
					//删除组件
                    onDeleleItem: function(index) {
                        var that = this;
						if(that.diyData.values.length==1){
							$.show_error(hm_lang[54]);
							return false;
						}
                        layer.confirm(hm_lang[1]', 
							function(temp) {
								that.diyData.values.splice(index, 1);
								that.selectedIndex = -1;
								layer.close(temp);
							})
                    },
					//提交保存
                    onSubmit: function(){
						if(this.diyData.values.length==0){
							$.show_error(hm_lang[55]);
							return false;
						}
                        if (this.diyData.sceneDesc == '') {
                            $.show_error(hm_lang[56]);
							return false;
                        };
                        $.post('', {
							data: this.diyData
                        }, function(result) {
                            result.code === 1 ? $.show_success(result.msg, result.url) : $.show_error(result.msg)
                        })
                    }
                }
            })
        }
    };
    window.diyPhone = diyPhone
})(window)