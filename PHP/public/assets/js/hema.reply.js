(function() {
    document.body.ondrop = function(event) {
        event.preventDefault();
        event.stopPropagation()
    };
    var values = {};

    function diyPhone(data) {
        values = data;
        this.init(data)
    }
	
    diyPhone.prototype = {
        init: function(data) {
            new Vue({
                el: '#app',
                data: {
					values : data //初始数据
                },
                methods: {
					//获取图片素材
					addImage: function() {
						var that = this;
						$.fileLibrary({
							type: 'image',
							done: function(img) {
								that.values.dataGroup.image.url = img[0].file_path;
								that.values.dataGroup.image.file_name = img[0].file_name;
							}
						})
					},
					/*
					addNews:function(){
						var vm = this,i;
						parent._selectNews$eb = function(data){
							var newsList = [];
							data.new.map((news)=>{
								newsList.push({
									title:news.title,
									description:news.synopsis,
									url:news.url,
									image:news.image_input,
									date:moment(news.add_time*1000).format('M月D日'),
									id:news.id
								});
							});
							vm.dataGroup.news = newsList;
							delete parent._selectNews$eb;
							$eb.closeModalFrame(i);
						};
						i = $eb.createModalFrame('选择图文消息',"/admin/wechat.wechat_news_category/select.html?callback=_selectNews%24eb",{w:975});
					}
					*/
					//提交保存
                    onSubmit: function() {
						//验证关键字
						if(typeof this.values.keyword != 'undefined'){
							if(this.values.keyword == ''){
								layer.msg(hm_lang[46], {anim: 6});
								return false
							}
						}
						//文本信息
						if(this.values.type == 'text'){
							if(this.values.dataGroup.text.content == ''){
								layer.msg(hm_lang[47], {anim: 6});
								return false
							}
						}
						//图片消息
						if(this.values.type == 'image'){
							if(this.values.dataGroup.text.content == ''){
								layer.msg(hm_lang[47], {anim: 6});
								return false
							}
						}
						//语音消息
						if(this.values.type == 'voice'){
							if(this.values.dataGroup.voice.media_id == ''){
								layer.msg(hm_lang[48], {anim: 6});
								return false
							}
						}
						//视频消息
						if(this.values.type == 'video'){
							if(this.values.dataGroup.video.title == ''){
								layer.msg(hm_lang[49], {anim: 6});
								return false
							}
							if(this.values.dataGroup.video.description == ''){
								layer.msg(hm_lang[50], {anim: 6});
								return false
							}
							if(this.values.dataGroup.video.media_id == ''){
								layer.msg(hm_lang[48], {anim: 6});
								return false
							}
						}
						//音乐消息
						if(this.values.type == 'music'){
							if(this.values.dataGroup.music.title == ''){
								layer.msg(hm_lang[49], {anim: 6});
								return false
							}
							if(this.values.dataGroup.music.description == ''){
								layer.msg(hm_lang[50], {anim: 6});
								return false
							}
							if(this.values.dataGroup.music.media_id == ''){
								layer.msg(hm_lang[48], {anim: 6});
								return false
							}
							if(this.values.dataGroup.music.url != '' || this.values.dataGroup.music.hurl!=''){
								layer.msg(hm_lang[51], {anim: 6});
								return false
							}
						}
						//图文消息
						if(this.values.type == 'news'){
							if(this.values.dataGroup.news.media_id == ''){
								layer.msg(hm_lang[48], {anim: 6});
								return false
							}
						}
						$("#my-modal-loading").html('<div class="am-modal-dialog"><div class="am-modal-hd">'+hm_lang[0]+'</div><div class="am-modal-bd"><span class="am-icon-spinner am-icon-spin"></span></div></div>');
						$("#my-modal-loading").modal('open');
                        $.post('', {
							data: this.values
                        }, function(result) {
							if(result.code === 1){
								$("#my-modal-loading").modal('close');
								$.show_success(result.msg, result.url)
							}else{
								$("#my-modal-loading").modal('close');
								$.show_error(result.msg)
							}
                        })
                    }
                }
            })
        }
    };
    window.diyPhone = diyPhone
})(window)