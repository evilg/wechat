<?php
namespace hema\delivery\engine;

/**
 * 达达配送
 * 线上域名：newopen.imdada.cn
 * 测试域名：newopen.qa.imdada.cn
 * 测试门店编号：11047059
 * 测试商户编号：73753
*/
class Dada extends Basics
{
	private $businessParams = '';//请求数据（body内容）
	
	/**
     * 预发布订单创建
	*/
	public function preOrder($data)
	{
		$this->businessParams = json_encode($data,JSON_UNESCAPED_UNICODE);
		$data = $this->bulidRequestParams();
		$result = $this->getHttpRequestWithPost($data,'/api/order/queryDeliverFee');
		if($result['status'] == 'success'){
			return [
				'delivery_time' => '30分钟', //预计送达时间
				'delivery_distance' => (int)$result['result']['distance'] . '米', //配送距离
				'delivery_price' => '￥' . $result['result']['fee'] . '元', //配送费用
				'delivery_no' => $result['result']['deliveryNo'], //配送单号
				'time' => '', //预计送达时间
				'distance' => $result['result']['distance'], //配送距离
				'price' => $result['result']['fee'], //配送费用
				'dada' => $result
			];
		}
		$this->error = $result['msg'];
		return false;
	}

	/**
     * 创建订单 - 查询运费后发单接口
	*/
	public function addOrder($data = [])
	{
		$post_data = [
			'deliveryNo' => $data['delivery_no']//是 平台订单编号
		];
		$this->businessParams = json_encode($post_data,JSON_UNESCAPED_UNICODE);
		$post_data = $this->bulidRequestParams();
		$result = $this->getHttpRequestWithPost($post_data,'/api/order/addAfterQuery');
		if($result['status'] == 'success'){
			return $result;
		}
		$this->error = $result['msg'];
		return false;
	}

	/**
     * 取消订单
	*/
	public function cancelOrder($order_no)
	{
		$post_data = [
			'order_id' => $order_no,//第三方订单编号
			'cancel_reason_id' => 4, //取消原因ID
			//'cancel_reason' => ''//否	取消原因(当取消原因ID为其他时，此字段必填)
		];
		/*
		取消原因ID
		1	没有配送员接单
		2	配送员没来取货
		3	配送员态度太差
		4	顾客取消订单
		5	订单填写错误
		34	配送员让我取消此单
		35	配送员不愿上门取货
		36	我不需要配送了
		37	配送员以各种理由表示无法完成订单
		10000	其他
		 */
		$this->businessParams = json_encode($post_data,JSON_UNESCAPED_UNICODE);
		$post_data = $this->bulidRequestParams();
		$result = $this->getHttpRequestWithPost($post_data,'/api/order/formalCancel');
		if($result['status'] == 'success'){
			return $result;
		}
		$this->error = $result['msg'];
		return false;
	}
	
	/**
     * 获取城市编码
     */
	public function getCiytCode($city)
	{
		$city = str_replace('市','',$city);
		$this->businessParams = '';
		$post_data = $this->bulidRequestParams();
		$post_data['app_secret'] = $this->config['app_secret'];
		$result = $this->getHttpRequestWithPost($post_data,'/api/cityCode/list');
		$code = '';
		for($n=0;$n<sizeof($result['result']);$n++){
			if($result['result'][$n]['cityName'] == $city){
				$code = $result['result'][$n]['cityCode'];
				break;
			}
		}
		return $code;
	}

    /**
     * 发送请求,POST
     * @param $url 指定URL完整路径地址
     * @param $data 请求的数据
     */
    private function getHttpRequestWithPost($data,$url)
    {
		$data = json_encode($data,JSON_UNESCAPED_UNICODE);
        $url = $this->config['api_url'] . $url;
        // json
        $headers = array(
            'Content-Type: application/json',
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $resp = curl_exec($curl);
        //var_dump(curl_error($curl));//如果在执行curl的过程中出现异常，可以打开此开关查看异常内容。
        $info = curl_getinfo($curl);
        curl_close($curl);
        if (isset($info['http_code']) && $info['http_code'] == 200) {
            return json_decode($resp, true);
        }
        return false;
    }
	
	/**
     * 构造请求数据
     * data:业务参数，json字符串
     */
    private function bulidRequestParams()
    {
        $requestParams['app_key'] = $this->config['app_key']; //应用Key，对应开发者账号中的app_key
        $requestParams['body'] = $this->businessParams; //业务参数，JSON字符串
        $requestParams['format'] = 'json'; //请求格式，暂时只支持json
        $requestParams['source_id'] = $this->config['source_id']; //商户编号,测试环境默认为：73753
        $requestParams['timestamp'] = time(); //时间戳,单位秒
        $requestParams['v'] = '1.0'; //API版本
        $requestParams['signature'] = $this->_sign($requestParams); //签名Hash值
        return $requestParams;
    }

    /**
     * 签名生成signature
     */
    private function _sign($data){
		
        //1.升序排序
        ksort($data);
        //2.字符串拼接
        $args = "";
        foreach ($data as $key => $value) {
            $args.=$key.$value;
        }
        $args = $this->config['app_secret'] . $args . $this->config['app_secret'];
        //3.MD5签名,转为大写
        $sign = strtoupper(md5($args));
        return $sign;
    }
	
	
}
