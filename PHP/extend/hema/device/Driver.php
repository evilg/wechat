<?php
namespace hema\device;

use hema\device\engine\Feieyun;
use hema\device\engine\Yilianyun;
use hema\device\engine\Hmcalling;
use hema\device\engine\Daqu;

/**
 * 云设备模块驱动
 */
class Driver
{
    private $engine;    // 当前设备擎类
    private $device = [
        'feieyun' => '飞鹅云打印',
        'yilianyun' => '易联云打印',
        'daqu' => '大趋云打印',
        'hmcalling' => '河马云叫号'
    ];
    /**
     * 云设备引擎类列表
     */
    const ENGINE_CLASS_LIST = [
        'feieyun' => Feieyun::class,
        'yilianyun' => Yilianyun::class,
        'daqu' => Daqu::class,
        'hmcalling' => Hmcalling::class
    ];
    
     /**
     * 构造方法
     * @param null|string $dev_type 指定设备类型
     */
    public function __construct($dev_type)
    {
        // 实例化当前引擎
        if(!$config = get_addons_config($dev_type)){
            die(json_encode(['code' => 0, 'msg' => '未安装《' . $this->device[$dev_type] .'》插件']));
        }
        $class = self::ENGINE_CLASS_LIST[$dev_type];
        $this->engine = new $class($config);
    }
    
    /**
     * 添加设备
     * $data = 设备参数
     */
    public function add($data)
    {
        return $this->engine->add($data);
    }
    
    /**
     * 获取设备状态
     * $dev_id = 设备ID
     */
    public function status($dev_id)
    {
        return $this->engine->status($dev_id);
    }

    /**
     * 删除设备
     * $dev_id = 设备ID
     */
    public function delete($dev_id)
    {
        return $this->engine->delete($dev_id);
    }
    
    /**
     * 编辑设备
     */
    public function edit($data)
    {
        return $this->engine->edit($data);
    }
    
    /**
     * 播报语音
     */
    public function push($data)
    {
        return $this->engine->push($data);
    }
    
    /**
     * 执行打印
     */
    public function print($dev,$content)
    {
        return $this->engine->print($dev,$content);
    }
    
    /**
     * 获取token
     */
    public function getToken($config)
    {
        return $this->engine->getToken($config);
    }

    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError()
    {
        return $this->engine->getError();
    }

}
