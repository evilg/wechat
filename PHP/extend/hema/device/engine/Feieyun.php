<?php
namespace hema\device\engine;

/**
 * 飞鹅打印机驱动引擎
 * 测试机型号：FP-V58W
 */
class Feieyun extends Basics
{
    /**
    * 打印订单接口
    * $dev = 设备参数,$content=打印模板
    */
    public function print($dev,$content)
    {
        $content = $this->make_templet($content);//转换打印模板
        $msgInfo = $this->msgInfo();
        $msgInfo['apiname'] = 'Open_printMsg';
        $msgInfo['sn'] = $dev['dev_id'];
        $msgInfo['content'] = $content;
        $msgInfo['times'] = $dev['prt_num'];
        $result = json_decode($this->http_post_json($msgInfo),true);
        sleep(1);//延时1秒
        if($result['ret']!=0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;
    }
    
    /**
    * [获取某台打印机状态接口 Open_queryPrinterStatus]
    */
    public function status($dev_id)
    {
        $msgInfo = $this->msgInfo();
        $msgInfo['apiname'] = 'Open_queryPrinterStatus';
        $msgInfo['sn'] = $dev_id;
        $res = json_decode($this->http_post_json($msgInfo),true);
        sleep(1);//延时1秒
        if($res['ret']==0){
            $msg = '离线';
            if($res['data']=='在线，工作状态正常。'){
                $msg = '正常';
            }
            if($res['data']=='在线，工作状态不正常。'){
                $msg = '异常';
            }
            return $msg;
        }
        return '未知';
    }
    
    /**
    * [批量添加打印机接口 Open_printerAddlist]
    */
    public function add($data)
    {
        //$printerContent,编号(必填) # 打印机识别码(必填) # 备注名称(选填) # 流量卡号码(选填)
        $printerContent = $data['dev_id'] . '#' . $data['dev_key'].'#'. $data['dev_name'];
        $msgInfo = $this->msgInfo();
        $msgInfo['apiname'] = 'Open_printerAddlist';
        $msgInfo['printerContent'] = $printerContent;
        $result = json_decode($this->http_post_json($msgInfo),true);
        if($result['ret']!=0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;
    }
    
    /**
    * [批量删除打印机 Open_printerDelList]
    * @param  [string] $snlist [打印机编号，多台打印机请用减号“-”连接起来]
    * @return [string]         [接口返回值]
    */
    public function delete($dev_id)
    {
        $msgInfo = $this->msgInfo();
        $msgInfo['apiname'] = 'Open_printerDelList';
        $msgInfo['snlist'] = $dev_id;
        $result = json_decode($this->http_post_json($msgInfo),true);
        if($result['ret']!=0){
            $this->error = $result['msg'];
            return false;
        }
        return $result;
    }

    /**
    * 公共参数
    */
    private function msgInfo(){
        $time = time();         //请求时间
        return $msgInfo = [
            'user'=>$this->config['app_key'],
            'stime'=>$time,
            'sig'=>$this->signature($time)
        ];
    }
    
    /**
    * [signature 生成签名]
    */
    private function signature($time){
        return sha1($this->config['app_key'] . $this->config['app_secret'] . $time);//公共参数，请求公钥
    }
    
    /**
     * 发送post请求
     * PHP发送Json对象数据
     */
    private function http_post_json($data)
    {
        $data = http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_URL, $this->config['api_url'] . '/Api/Open/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen($data)
            )
        );
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $response;
    }
    
    /**
     * 制作订单模板
     * $data 订单数据
     * 58mm的机器,一行打印16个汉字,32个字母;
     */
    private function make_templet($data)
    {
        /*
         <BR> ：换行符
         <CUT> ：切刀指令(主动切纸,仅限切刀打印机使用才有效果) 
         <LOGO> ：打印LOGO指令(前提是预先在机器内置LOGO图片)
         <PLUGIN> ：钱箱或者外置音响指令
         <CB></CB>：居中放大
         <B></B>：放大一倍
         <C></C>：居中
         <L></L>：字体变高一倍
         <W></W>：字体变宽一倍
         <QR></QR>：二维码（单个订单，最多只能打印一个二维码）
         <RIGHT></RIGHT>：右对齐
         <BOLD></BOLD>：字体加粗
        */
        $content = '';
        foreach ($data as $item){
            $row_arr = explode(',',$item);
            $row = $row_arr[0];
            for($m=1;$m<sizeof($row_arr);$m++){
                //放大
                if($row_arr[$m] == '<B>'){
                    $row = "<B>" . $row . "</B>";
                }
                //居中
                if($row_arr[$m] == '<C>'){
                    $row = "<C>" . $row . "</C>";
                }
            }
            $content .= $row . "<BR>";
        }
        $content .= "<BR><BR>";
        return $content;
    }
}
